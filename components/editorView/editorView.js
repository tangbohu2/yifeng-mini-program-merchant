// components/editorView/editorView.js
import * as API_RichEditor from '../../api/richEditor'
Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    initData:[]
  },

  /**
   * 组件的方法列表
   */
  methods: {
    init(id){
      API_RichEditor.get(id).then(res=>{
        const items = JSON.parse(res.attValue)
        this.setData({
          initData:items
        })
      })
    }
  }
})
