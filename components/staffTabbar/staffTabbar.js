// components/staffTabbar/staffTabbar.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  lifetimes: {
    attached: function() {
      // 在组件实例进入页面节点树时执行
      const page = getCurrentPages()
      const len = page.length - 1
      const activePage = page[len]
      // const index = this.data.tabbar.findIndex(item => {
      //   return item.pagePath == '/'+activePage.route
      // })
      const index= getApp().globalData.tabindex
      console.log(index)
      this.setData({
        activeUrl: '/'+activePage.route,
        activeIndex: index
      })
    },
  },
  data: {
    activeIndex: 0,
    activeUrl: '',
    tabbar: [
      {
        "pagePath": "/pages/sstaffIndex/staffIndex",
        "iconPath": "../../static/images/tab/index.png",
        "selectedIconPath": "../../static/images/tab/index-active.png",
        "text": "首页"
      },
      {
        "pagePath": "/pages/staff/staffVideo/staffVideo",
        "iconPath": "../../static/images/tab/video.png",
        "selectedIconPath": "../../static/images/tab/video-active.png",
        "text": "名师视频"
      },
      {
        "pagePath": "/pages/staff/staffBbs/staffBbs",
        "iconPath": "../../static/images/tab/discover.png",
        "selectedIconPath": "../../static/images/tab/discover-active.png",
        "text": "发现"
      },
      {
        "pagePath": "/pages/staff/staffMy/staffMy",
        "iconPath": "../../static/images/tab/my.png",
        "selectedIconPath": "../../static/images/tab/my-active.png",
        "text": "我的"
      }
    ]
  },

  /**
   * 组件的方法列表
   */
  methods: {
    navto(e) {
      const url = e.currentTarget.dataset.url
      if(this.data.activeUrl == url ){
        return
      }
      const index = this.data.tabbar.findIndex(item => {
        return item.pagePath == url
      })
      getApp().globalData.tabindex = index
      wx.redirectTo({
        url,
      })
    }
  }
})
