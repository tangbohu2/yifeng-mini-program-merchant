import * as API_Classify from '../../api/storeClassify'
function product(type,name,value,selectPicker,selectIndex,){
  this.type = type
  this.name = name
  this.value = value
  this.selectPicker = selectPicker
  this.selectIndex = selectIndex
}
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    attrType: {
      type: Object,
      value: {
        image: {
          name: '多选项目',
          type: 'multipleAttr',
        },
        text: {
          name: '全含项目',
          type: 'mainAttr',
        },
      },
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    productData:[],
    classifyList:[]
  },

  lifetimes:{
    attached(){
      let that = this
      API_Classify.getClassifySecond().then(res => {
        that.setData({classifyList:res})
      })
    }
  },

  /**
   * 组件的方法列表
   */
  methods: {

    loadAttr(e){
      let data = JSON.parse(e)
      this.setData({productData:data})
    },
    add(e){
      let that = this
      //根据传递类型新建对象
      let type = e.currentTarget.dataset.type
      let arr = that.data.productData
      let classify = that.data.classifyList[0]
      //只可以存在一个主营类别
      if(type == 'mainAttr'){
        for(var i = 0; i < arr.length; i++){
          if(arr[i].type == "mainAttr"){
            wx.showToast({
              title: '全含项目只可添加一个',
              duration:2000,
              icon:'none'
            })
            return
          }
        }
      }
      switch (type) {
        case "mainAttr":
          arr.push(new product(
            type,
            '包含以下所有项目',
           [ {
              productName:'',
              price:'',
              // classifyIndex:0,
              classifyIndex:that.data.classifyList[0].id,
              xcxuseindex: 0,
              classifyName:classify.classifyName
            }]
          ))
          break
        case "multipleAttr":
          arr.push(new product(
            type,
            '1选1',
            [
              {
                productName:'',
                price:'',
                // classifyIndex:0,
                classifyIndex:that.data.classifyList[0].id,
                xcxuseindex: 0,
                classifyName:classify.classifyName
              }
            ],
            ['1选1'],
            0
          ))
          break
      }
      this.setData({productData:arr})
    },
    //主类目改变产品名
    changeProductName(e){
      let that = this
      //获取是哪个元素要改变
      let index = e.currentTarget.dataset.index
      let mIndex = e.currentTarget.dataset.mindex
      //获取选中的值进行更改
      that.data.productData[index].value[mIndex].productName =  e.detail.value
      this.setData({productData: that.data.productData})
    },
    //主类目改变分类
    mainClassify(e){
      let that = this
      let index =  e.currentTarget.dataset.index
      let mIndex = e.currentTarget.dataset.mindex

      let classify = that.data.classifyList[e.detail.value]
      console.log(classify)
      that.data.productData[index].value[mIndex].xcxuseindex = e.detail.value
      that.data.productData[index].value[mIndex].classifyIndex =  classify.id
      that.data.productData[index].value[mIndex].classifyName =  classify.classifyName
      this.setData({productData: that.data.productData})
    },
    //全含项目删除
    maindelProduct() {
      this.setData({
        productData: []
      })
    },
    //更改多选项内名称
    changeMultipleProductName(e){
      let that = this
      let index = e.currentTarget.dataset.index
      let mIndex = e.currentTarget.dataset.mindex
      let multArr = that.data.productData[index].value
      multArr[mIndex].productName = e.detail.value
      that.data.productData[index].value[mIndex] =  multArr[mIndex]
      this.setData({productData: that.data.productData})
    },
    //更改多选项内价格
    changeMultiplePrice(e){
      let that = this
      let index = e.currentTarget.dataset.index
      let mIndex = e.currentTarget.dataset.mindex
      let multArr = that.data.productData[index].value
      multArr[mIndex].price = e.detail.value
      that.data.productData[index].value[mIndex] =  multArr[mIndex]
      this.setData({productData: that.data.productData})
    },
    //次数
    changeTimes(e) {
      let that = this
      let index = e.currentTarget.dataset.index
      let mIndex = e.currentTarget.dataset.mindex
      let multArr = that.data.productData[index].value
      multArr[mIndex].times = e.detail.value
      that.data.productData[index].value[mIndex] =  multArr[mIndex]
      this.setData({productData: that.data.productData})
    },
    //更改多选项内分类
    multClassify(e){
      let that = this
      let index = e.currentTarget.dataset.index
      let mIndex = e.currentTarget.dataset.mindex
      let multArr = that.data.productData[index].value
      let classify = that.data.classifyList[e.detail.value]
      // multArr[mIndex].classifyIndex = e.detail.value
      multArr[mIndex].classifyIndex = classify.id
      multArr[mIndex].xcxuseindex = e.detail.value
      // that.data.productData[index].value.classifyIndex =  e.detail.value
      that.data.productData[index].value.classifyIndex =  classify.id
      that.data.productData[index].value.classifyName =  classify.classifyName
      that.data.productData[index].value[mIndex] =  multArr[mIndex]
      this.setData({productData: that.data.productData})
      console.log(this.data.productData)
    },
    //增加一个多选元素
    addMultItem(e){
      let that = this
      let index = e.currentTarget.dataset.index
      let multArr = that.data.productData[index].value
      let item = that.data.productData[index]
      let classify = that.data.classifyList[0]
      multArr.push({
            productName:'',
            price:'',
            classifyId:'',
            // classifyIndex:0,
            classifyIndex:that.data.classifyList[0].id,
            xcxuseindex: 0,
            classifyName:classify.classifyName
          })
      item.name = multArr.length + '选' + '1'
      item.selectPicker = []
      item.selectIndex = 0
      for(var i = 1; i < multArr.length; i++){
        item.selectPicker.push(multArr.length + '选' + i)
      }
      that.data.productData[index] = item
      that.data.productData[index].value =  multArr
      this.setData({productData:that.data.productData})
    },
    //增加一个多选元素
    addMainItem(e){
      let that = this
      let index =  e.currentTarget.dataset.index
      let multArr = that.data.productData[index].value
      let item = that.data.productData[index]
      let classify = that.data.classifyList[0]
      console.log(classify)
      multArr.push({
            productName:'',
            price:'',
            classifyId:'',
            // classifyIndex:0,
            classifyIndex: that.data.classifyList[0].id,
            xcxuseindex: 0,
            classifyName:classify.classifyName
          })
      // item.name = multArr.length + '选' + '1'
      // item.selectPicker = []
      // item.selectIndex = 0
      // for(var i = 1; i < multArr.length; i++){
      //   item.selectPicker.push(multArr.length + '选' + i)
      // }
      that.data.productData[index] = item
      that.data.productData[index].value =  multArr
      this.setData({productData:that.data.productData})
    },
    changeMultSelect(e){
      let that = this
      let index = e.currentTarget.dataset.index
      let productData=that.data.productData;
      productData[index].selectIndex=e.detail.value;
      productData[index].name= productData[index].selectPicker[e.detail.value]
      this.setData({
        productData:productData
      })
      /*let item = that.data.productData[index]
      console.log(item)
       // item.selectPicker.push(multArr.length + '选' + i)
        let a = (e.detail.value*1)+1
        this.setData({
          selectIndex:a
        })*/
    },
    delMultItem(e){
      let that = this
      let index = e.currentTarget.dataset.index
      let mIndex = e.currentTarget.dataset.mindex
      let multArr = that.data.productData[index].value
      let item = that.data.productData[index]
      multArr.splice(mIndex,1)
      if(multArr.length == 0){
        that.data.productData.splice(index,1)
        this.setData({productData:that.data.productData})
        return
      }
      item.name = multArr.length + '选' + '1'
      item.selectPicker = []
      item.selectIndex = 0
      if(multArr.length == 1){
        item.selectPicker.push(1 + '选' + 1)

      }else{
        for(var i = 1; i < multArr.length; i++){
          item.selectPicker.push(multArr.length + '选' + 1)
        }

      }
      
      that.data.productData[index] = item
      that.data.productData[index].value =  multArr
      this.setData({productData:that.data.productData})

    },
    delMainItem(e){
      let that = this
      let index =  e.currentTarget.dataset.index
      let mIndex = e.currentTarget.dataset.mindex
      let multArr = that.data.productData[index].value
      let item = that.data.productData[index]
      multArr.splice(mIndex,1)
      if(multArr.length == 0){
        that.data.productData.splice(index,1)
        this.setData({productData:that.data.productData})
        return
      }
      // item.name = multArr.length + '选' + '1'
      // item.selectPicker = []
      // item.selectIndex = 0
      // for(var i = 1; i < multArr.length; i++){
      //   item.selectPicker.push(multArr.length + '选' + i)
      // }
      that.data.productData[index] = item
      that.data.productData[index].value =  multArr
      this.setData({productData:that.data.productData})
    },
    changePrice(e){
      let that = this
      //获取是哪个元素要改变
      let index = e.currentTarget.dataset.index
      let mIndex = e.currentTarget.dataset.mindex

      //获取选中的值进行更改
      that.data.productData[index].value[mIndex].price =  e.detail.value
      this.setData({productData: that.data.productData})
    },
    changeAllTimes(e) {
      let that = this
      //获取是哪个元素要改变
      let index = e.currentTarget.dataset.index
      let mIndex = e.currentTarget.dataset.mindex

      //获取选中的值进行更改
      that.data.productData[index].value[mIndex].times =  e.detail.value
      this.setData({productData: that.data.productData})
    },
    d(){
      console.log(this.data.productData)
    },
    callback(){
      //是否有数据
      var exist = true
      if(this.data.productData.length<1){
        exist = false
      }
      console.log("tan")
      console.log(this.data.productData);
      let json = JSON.stringify(this.data.productData)
      var data = {
        data: json,
        exist:exist
      }
      this.triggerEvent('ProductAttr', data, {})
    }
  }
})
