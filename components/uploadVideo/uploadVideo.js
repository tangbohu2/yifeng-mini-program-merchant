import * as API_Upload from '../../utils/fileUpload'
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    showDel:{
      type: Boolean,
      value: false
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    videos: [], //上传的结果集
    tempFiles:[] //展示用
  },

  /**
   * 组件的方法列表
   */
  methods: {
    load(data){
      this.setData({
        videos:data
      })
    },
    bindlongpressimg:function(e){
      this.data.videos.splice(e.currentTarget.dataset.id,1)
        this.setData({
          videos: this.data.videos
        })
        var myEventDetail = {
          videos: this.data.videos
        } // detail对象，提供给事件监听函数
        var myEventOption = {} // 触发事件的选项
        this.triggerEvent('video', myEventDetail, myEventOption)
  },
   //选择视频上传
   chooseVideo(e){
    let that = this
    let arr = that.data.videos
    // let tempArr = this.data.tempFiles
      wx.chooseMedia({
        count: 1,
        mediaType: ['video'],
        sourceType: ['album', 'camera'],
        success: function (res) {
          API_Upload.upload(res.tempFiles[0].tempFilePath,res=>{
            const {url} = JSON.parse(res.data)
            arr.push(url)
            that.setData({
              videos:arr
            })
            var myEventDetail = {
              videos: that.data.videos
            } // detail对象，提供给事件监听函数
            var myEventOption = {} // 触发事件的选项

            that.triggerEvent('video', myEventDetail, myEventOption)
          },res=>{
              wx.showToast({
                  title:'上传失败',
                  icon:'none'
              })
          })
        }
    })
  },
  }
})
