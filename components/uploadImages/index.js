// component/uploadImages/index.js
import * as API_File from '../../utils/fileUpload'
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    count: { //最多选择图片的张数，默认9张
      type: Number,
      value: 9
    },
    unShowAdd:{
      type: Boolean,
      value: false
    },
    showDel:{
      type: Boolean,
      value: false
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    detailPics: [], //上传的结果图片集合
  },

  ready: function() {
  },

  /**
   * 组件的方法列表
   */
  methods: {
    bindlongpressimg:function(e){
        this.data.detailPics.splice(e.currentTarget.dataset.id,1)
          this.setData({
            detailPics: this.data.detailPics
          })
          var myEventDetail = {
            picsList: this.data.detailPics
          } // detail对象，提供给事件监听函数
          var myEventOption = {} // 触发事件的选项
          this.triggerEvent('myevent', myEventDetail, myEventOption)
    },
    load(obj){
      this.setData({
        detailPics:obj
      })
    },
    /**
     * 上传详细图片
     */
    uploadDetailImage: function(e) { //这里是选取图片的方法
      var that = this;
      var detailPics = that.data.detailPics;
      var canUse = that.data.count - detailPics.length

      if (detailPics.length >= that.data.count) {
        wx.showToast({
          title: '最多选择' + that.data.count + '张！',
        })
        return;
      }
      wx.showActionSheet({
        itemList: ['相册', '相机'],
        itemColor: "#40ba67",
        success: function (res) {
         if (!res.cancel) {
          if (res.tapIndex == 0) {
           that.camera(canUse,'album')
          } else if (res.tapIndex == 1) {
           that.camera(canUse,'camera')
          }
         }
        }
       })
    },
  camera(canUse,type){
    let that = this
    var pics = [];
    wx.chooseImage({
      count: canUse, // 最多可以选择的图片张数a
      sizeType: ['compressed'], // original 原图，compressed 压缩图，默认二者都有
      // sourceType: ['album', 'camera'], // album 从相册选图，camera 使用相机，默认二者都有\
      sourceType:[type],
      success: function(res) {
        var imgs = res.tempFilePaths;
        for (var i = 0; i < imgs.length; i++) {
          pics.push(imgs[i])
        }
        that.uploadimg({
          path: pics //这里是选取的图片的地址数组
        });
      },
    })
  },
    //多张图片上传
    uploadimg: function(data) {
      wx.showLoading({
        title: '上传中...',
        mask: true,
      })
      var that = this,
        i = data.i ? data.i : 0,
        success = data.success ? data.success : 0,
        fail = data.fail ? data.fail : 0;
        API_File.upload(data.path[i],res=>{
          wx.hideLoading();
          success++;
          const {url} = JSON.parse(res.data)
          var pic_name = url
          var detailPics = that.data.detailPics;
          detailPics.push(pic_name)
          that.setData({
            detailPics: detailPics
          })
          i++;
          if (i == data.path.length) { //当图片传完时，停止调用     
            var myEventDetail = {
              picsList: that.data.detailPics
            } // detail对象，提供给事件监听函数
            var myEventOption = {} // 触发事件的选项

            that.triggerEvent('myevent', myEventDetail, myEventOption)
          } else { //若图片还没有传完，则继续调用函数
            data.i = i;
            data.success = success;
            data.fail = fail;
            that.uploadimg(data);
          }
      },res=>{
          wx.showToast({
              title:'上传失败',
              icon:'none'
          })
      })
      // wx.uploadFile({
      //   url: data.url,
      //   filePath: data.path[i],
      //   name: 'fileData',
      //   formData: null,
      //   success: (resp) => {
          
      //   },
      //   fail: (res) => {
      //     fail++;
      //     console.log('fail:' + i + "fail:" + fail);
      //   },
      //   complete: () => {
         
      //   }
      // });
    },

  }
})
