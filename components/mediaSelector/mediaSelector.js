import * as API_Upload from '../../utils/fileUpload'
// components/mediaSelector/mediaSelector.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    
  },

  /**
   * 组件的初始数据
   */
  data: {
    mediaType:['image','video'],
    sourceType:['album', 'camera'],
    count:1,//一次可以选几个
    maxCount:0,//最多可以选几个
    imgs:[],
    videos:[],
    source: [],
    showAdd:true,
    showDel:true
  },
 
  /**
   * 组件的方法列表
   */
  methods: {

    priview(e){
      console.log('priview')
      let that = this
      let imgs = []
      that.data.imgs.forEach(item => {
        imgs.push(item.src)
      })
      wx.previewImage({
        current: that.data.imgs[e.currentTarget.dataset.id].src, // 当前显示图片的 http 链接
        urls: imgs // 需要预览的图片 http 链接列表
      })
    },
     del:function(e){
      let that = this
      that.data.imgs.splice(e.currentTarget.dataset.id,1)
          this.setData({
            imgs: that.data.imgs
          })
          var files = {
            type:'image',
            count:9,
            media: that.data.imgs
          } 
          that.canAddNewNum();
          that.triggerEvent('media', files)
    },
    delVideo(e){
      
      let that = this
      that.data.videos.splice(e.currentTarget.dataset.id,1)
          this.setData({
            videos: that.data.videos
          })
          var files = {
            type:'video',
            count:1,
            media: that.data.videos
          } 
          that.canAddNewNum();
          that.triggerEvent('media', files)
    },
    init(e){
      let that = this
      if(e.mediaType){
        let arr = [e.mediaType]
        that.setData({
          mediaType:arr
        }) 
        if(e.arrList&&e.arrList.length>0){
          if(e.mediaType=='image'){
            that.setData({
              imgs:e.arrList
            }) 
          }else{
            that.setData({
              videos:e.arrList
            }) 
          }
        }
      }
      
      if(e.count){
        that.setData({count:e.count})
      }
      if(e.maxCount){
        that.setData({maxCount:e.maxCount})
      }
      that.canAddNewNum();

    },
    initImgSource(source) {
      console.log(source)
      this.setData({
        source: this.data.source.push(source['tempFilePath'])
      })
    },
    /**
     * 返回是否可以继续选择
     */
    canAddNewNum:function(){
      let maxCount=this.data.maxCount;
      this.setData({showAdd:true});
      if(maxCount>0){
         //限制总数
        let nowLength=this.data.imgs.length+this.data.videos.length;
        let canSelect=maxCount-nowLength;
        if(canSelect<=0){
          this.setData({showAdd:false});
          return 0;
        }
        return canSelect;
      }
      return 9;
    },
    select(e){
      let that = this
        let canSelectnum=that.canAddNewNum();
        if(canSelectnum<=0){
          wx.showToast({
            title: '最多可选'+that.data.maxCount+'条',
            icon:'none',
            duration:1500
          })
          return 
        }
        let count=this.data.count;
        if(count>canSelectnum){
          count=canSelectnum;
        }
      wx.chooseMedia({
        count: count,
        mediaType: that.data.mediaType,
        sourceType: that.data.sourceType,
        maxDuration: 30,
        camera: 'back',
        success(res) {
          if(res.type == 'video'){
            that.uploadVideo(res)
          }else if(res.type == 'image'){
            var pics = [];
            var imgs = res.tempFiles;
            for (var i = 0; i < imgs.length; i++) {
              pics.push(imgs[i])
            }
            that.uploadimg(pics)
          }
        },
        fail(e){
          wx.showToast({
            title: '上传失败',
            icon:'none'
          })
        }
      })
    },
      uploadimg(data) {
        wx.showLoading({
          title: '上传中...',
          mask: true,
        })
        var that = this,
          i = data.i ? data.i : 0,
          success = data.success ? data.success : 0,
          fail = data.fail ? data.fail : 0;
          API_Upload.upload(data[i].tempFilePath,res=>{
            success++;
            const {url} = JSON.parse(res.data)
           
            var item = {}
            item.src = url
            var detailPics = that.data.imgs;
            wx.getImageInfo({
              src: url,
              success(res){
                item.height = res.height
                item.width = res.width
                item.type = res.type
                detailPics.push(item)
                that.setData({
                  imgs: detailPics
                })
                that.canAddNewNum();
              }
            })
            i++;
            if (i == data.length) {
              const sourceList = that.data.source
              sourceList.push(data[0]['tempFilePath'])
              that.setData({
                source: sourceList
              })
              var files = {
                type:'image',
                count:9,
                media: that.data.imgs,
                source: that.data.source
              } 
              that.triggerEvent('media', files)
            } else { 
              data.i = i;
              data.success = success;
              data.fail = fail;
              that.uploadimg(data);
            }
        },res=>{
            wx.showToast({
                title:'上传失败',
                icon:'none'
            })
        })
    },
    uploadVideo(res){
      let that = this
      let arr = that.data.videos
      API_Upload.upload(res.tempFiles[0].tempFilePath,res=>{
        console.log(res)
        const {url} = JSON.parse(res.data)
        arr.push(url)
        that.setData({
          videos:arr
        })
        var files = {
          type:'video',
          count:1,
          media: that.data.videos
        } 
        that.canAddNewNum();
        that.triggerEvent('media', files)
      },res=>{
          wx.showToast({
              title:'上传失败',
              icon:'none'
          })
      })
    }
  }
})
