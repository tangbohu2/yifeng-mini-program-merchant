// components/memberInfo/memberInfo.js
//用于检测用户是否授权头像信息
//未授权的情况下弹出授权
import * as API_Member from '../../api/member'
import * as API_Config from '../../api/sysConfig'
Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    //用户信息
    userInfo: {},
    scrollHeight: '',
    scrollTop: 0,//滚动高度
    //授权弹窗
    showDialog:true,
    timmer:''
  },
  lifetimes:{
    attached(){
      let that = this
      //获取设备宽高
      this.setData({scrollHeight: wx.getSystemInfoSync().windowHeight});
      API_Config.getValueByKey("member_avatar").then(res =>{
        if('Y' == res){
          that.startCheck()
        }
      })
    }
  },
  /**
   * 组件的方法列表
   */
  methods: {
    startCheck(){
      let that = this
      //默认必须有token
      let token = wx.getStorageSync('token')
      //当没有的时候设置定时器等待token返回
      if(!token){
        clearInterval(this.data.timmer)
        this.data.timmer= setInterval(
          function () {
            wx.showLoading({
              title: '加载中...',
              mask:true
            })
            let d = wx.getStorageSync('token')
            if(d){
              wx.hideLoading()
              that.check()
            }
          }, 500);
      }else{
        that.check()
      }
    },
    check(){
       clearInterval(this.data.timmer)

       const member = wx.getStorageSync('member')

       if(!member.headImg || !member.nickName){
          wx.hideTabBar()
          this.setData({
            showDialog:false
          })
        // API_Member.reload().then(res=>{
        //   if(!res.headImg || !res.nickName){
        //     wx.hideTabBar()
        //      this.setData({
        //        showDialog:false
        //      })
        //    }
        // })
       }
      
    },
    userProfile:function(){
      const that = this
      wx.getUserProfile({
        desc:'获取信息',
        success(res){
          const userInfo = res.userInfo
          let uInfo = that.data.userInfo;
          uInfo = { headImgUrl:userInfo.avatarUrl,nickName:userInfo.nickName}
           //更新用户信息
           API_Member.updateBasicInfo(uInfo).then(res => {
            wx.setStorageSync('member', res.data);
            that.setData({
              userInfo:res.data,
              showDialog:true
            })
            var close =  getApp().globalData.phone
            if(!close){
              wx.showTabBar({
                animation: true,
              })
            }
            var detail = {} 
            var option = {} 
            that.triggerEvent('MemberInfo', detail, option)
          })
          
        },
        fail(res){
          wx.switchTab({
            url: '/pages/index/index',
          })
        }
      })
  
    },
  }
})
