//分类
import request from '../utils/request'

export function getClassify() {
    return request.ajax({
        url: `/classify`,
        method: 'get'
    })
} 
export function getClassifySecond() {
    return request.ajax({
        url: `/classify/second`,
        method: 'get'
    })
} 
export function getClassifyFirst() {
    return request.ajax({
        url: `/classify/first`,
        method: 'get'
    })
} 
export function getName(id) {
    return request.ajax({
        url: `/classify/getName/${id}`,
        method: 'post'
    })
} 