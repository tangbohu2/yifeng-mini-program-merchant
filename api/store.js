//门店申请部分
import request from '../utils/request'

//创建申请
export function create(params) {
  return request.ajax({
      url: `/storeApply`,
      method: 'put',
      isJson:true,
      loading:true,
      params
  })
}
//创建申请
export function update(params) {
  return request.ajax({
      url: `/storeApply`,
      method: 'post',
      isJson:true,
      params
  })
}
//查询是否存在申请记录
export function getApply(){
  return request.ajax({
    url: `/storeApply`,
    method: 'get'
  })
}

//门店部分
//查询是否存在
export function getStroe(params){
  return request.ajax({
    url: `/storeB/getStore`,
    method: 'post',
    params
  })
}
/**
 * 查询当前所有的权限
 */
export function getRoleTypes(){
  return request.ajax({
    url: `/storeB/getRoleTypes`,
    method: 'post'
  })
}
//查询昨日数据
export function getData(){
  return request.ajax({
    url: `/storeB/getData`,
    method: 'post'
  })
}
//有权限的门店列表
export function getMyStoreList(){
  return request.ajax({
    url: `/storeB/getMyStoreList`,
    method: 'post'
  })
}
/**
 * 检测是否需要提示
 */
export function checkTips(){
  return request.ajax({
    url: `/storeB/checkTips`,
    method: 'post'
  })
}