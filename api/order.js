//意见反馈
import request from '../utils/request'

//创建新的
export function create(params) {
    return request.ajax({
        url: `/order/getList`,
        method: 'post',
        params
    })
}
//获取已售出的订单分类
export function getClassifyList() {
    return request.ajax({
        url: `/order/getClassifyList`,
        method: 'post',
    })
}
//获取已售出的订单分类
export function getList(params) {
    return request.ajax({
        url: `/order/getList`,
        method: 'post',
        params
    })
}
//员工端，商户端的 核销（包括扫码和输码）
export function scanVerify(params) {
    return request.ajax({
        url: `/order/verify`,
        method: 'post',
        loading:true,
        params
    })
}
//员工端，商户端的 售后（包括扫码和输码）
export function scanAfter(params) {
  return request.ajax({
      url: `/order/aftersale`,
      method: 'post',
      loading:true,
      params
  })
}

//获取验券记录
export const getListNew = () => {
  return request.ajax({
    url: `/order/getListNew`,
    method: 'get',
    loading:true
  })
}
//员工端获取验券记录
export function getVerifyList (params) {
  return request.ajax({
    url: `/order/getVerifyList`,
    method: 'post',
    params,
    loading:true
  })
}