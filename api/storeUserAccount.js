import request from '../utils/request'

export function getList(params) {
    return request.ajax({
        url: `/storeUserAccount/list`,
        method: 'post',
        loading:true,
        params
    })
}
export function editAcount(params) {
    return request.ajax({
        url: `/storeUserAccount/editAcount`,
        method: 'post',
        loading:true,
        params
    })
}
export function getAcount(params) {
    return request.ajax({
        url: `/storeUserAccount/getAcount`,
        method: 'post',
        loading:true,
        params
    })
}
export function delAcount(params) {
    return request.ajax({
        url: `/storeUserAccount/delAcount`,
        method: 'post',
        loading:true,
        params
    })
}