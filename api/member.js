import request from '../utils/request'
export function getOpenId(code,openId) {
  return request.ajax({
      url: `/storeUser/getOpenId`,
      method: 'get',
      params:{code:code,openId:openId},
      noNeedToken:true
  })
}
//更新手机号
export function getPhone(params) {
  return request.ajax({
    url: '/storeUser/getPhone',
    method: 'post',
    loading: true,
    params
  })
}
//自动登录
export function autoLogin(openId) {
  return request.ajax({
      url: `/storeUser/auto-login`,
      method: 'post',
      params:{openId:openId}
  })
}

//通过手机号注册
export function register(params) {
  return request.ajax({
    url: '/storeUser/register',
    method: 'post',
    loading: true,
    params
  })
}

//更新手机号
export function flushPhone(params) {
  return request.ajax({
    url: '/storeUser/flushPhone',
    method: 'post',
    loading: true,
    params
  })
}


//自动登录
export function updateBasicInfo(params) {
  return request.ajax({
      url: `/storeUser/update/basic/info`,
      method: 'post',
      loading: true,
      isJson: true,
      params
  })
}
//刷新
export function reload() {
  return request.ajax({
      url: `/storeUser/reload`,
      method: 'post',
      loading: true
  })
}
//刷新
export function loginOut() {
  return request.ajax({
      url: `/storeUser/loginout`,
      method: 'post',
      loading: true
  })
}
//获取最新数据
export function getMember() {
  return request.ajax({
      url: `/storeUser/getMember`,
      method: 'post',
      loading: true
  })
}
export function showFollowSer() {
  return request.ajax({
      url: `/storeUser/showFollowSer`,
      method: 'post'
  })
}

// 员工端修改头像
export function updateMember(params) {
  return request.ajax({
      url: `/storeUser/updateSotreMember`,
      method: 'post',
      isJson: true,
      params
  })
}
