
import request from '../utils/request'



export function listPaymentConfig(){
  return request.ajax({
    url: `/vipPayment/listPaymentConfig`,
    method: 'post'
  })
}

export function listAdapayRecordB(){
  return request.ajax({
    url: `/vipPayment/listAdapayRecordB`,
    method: 'post'
    
  })
}
export function pay(params){
  return request.ajax({
      url:`/vipPayment/pay`,
      method:'post',
      loading:true,
      params
  })
}