import request from '../utils/request'

export function edit(params) {
    return request.ajax({
        url: `/fitment/edit`,
        method: 'post',
        loading:true,
        isJson:true,
        params
    })
}
export function addImage(params) {
  return request.ajax({
      url: `/fitment/addImage`,
      method: 'post',
      loading:true,
      isJson:true,
      params
  })
}
export function addVideo(params) {
  return request.ajax({
      url: `/fitment/addVideo`,
      method: 'post',
      loading:true,
      isJson:true,
      params
  })
}
export function getInfo(){
  return request.ajax({
    url: `/fitment/getInfo`,
    method: 'post',
    loading:true,
  })
}
export function getList(params){
  return request.ajax({
    url: `/fitment/getList/${params.type}`,
    method: 'post',
    loading:true,
  })
}
export function del(id){
  return request.ajax({
    url: `/fitment/del/${id}`,
    method: 'post',
    loading:true,
  })
}