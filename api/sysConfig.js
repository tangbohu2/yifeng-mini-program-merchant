//参数设置js
import request from "../utils/request";

// 获取参数值
export function getValueByKey(key) {
    return request.ajax({
        url: `/config/getConfigByKey/${key}`,
        method: 'post'
    })
}

// 获取参数值
export function getDictData(key) {
    return request.ajax({
        url: `/dict/list/${key}`,
        method: 'post'
    })
}