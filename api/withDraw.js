import request from '../utils/request'

export function getCustomerList(params){
  return request.ajax({
    url: `/withDraw/getCustomerList`,
    method: 'post',
    params
  })
}
export function getData(){
  return request.ajax({
    url: `/withDraw/getData`,
    method:'post'
  })
}
export function getCommissionList(params){
  return request.ajax({
    url: `/withDraw/getCommissionList`,
    method: 'post',
    params
  })
}

export function refreshStoreCommission(params){
  return request.ajax({
    url: `/storeB/refreshStoreBelow`,
    method: 'post',
    params
  })
}