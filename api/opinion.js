//意见反馈
import request from '../utils/request'

//创建新的
export function create(params) {
    return request.ajax({
        url: `/opinion`,
        method: 'put',
        isJson:true,
        params
    })
}