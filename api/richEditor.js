//富文本编辑器
import request from '../utils/request'

//新增
export function insert(params) {
    return request.ajax({
        url: `/richEditor`,
        method: 'put',
        loading: true,
        params
    })
}
//新增
export function edit(params) {
  return request.ajax({
      url: `/richEditor/edit`,
      method: 'put',
      isJson:true,
      loading: true,
      params
  })
}
//获取
export function get(id) {
  return request.ajax({
      url: `/richEditor/${id}`,
      loading: true,
      method: 'get'
      
  })
}
//获取
export function getByType(type) {
  return request.ajax({
      url: `/richEditor/byType/${type}`,
      loading: true,
      method: 'get'
  })
}
export function del(id) {
  return request.ajax({
      url: `/richEditor/del/${id}`,
      loading: true,
      method: 'get'
  })
}