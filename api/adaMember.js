
import request from '../utils/request'


export function createMember(params){
  return request.ajax({
    url: `/adaMember/createMember`,
    method: 'post',
    loading:true,
    params
  })
}
export function createSettleAccount(params){
  return request.ajax({
    url: `/adaMember/createSettleAccount`,
    method: 'post',
    loading:true,
    params
  })
}
export function getMember(){
  return request.ajax({
    url: `/adaMember/getMember`,
    method: 'post',
    loading:true,
  })
}
export function getSettleAccount(){
  return request.ajax({
    url: `/adaMember/getSettleAccount`,
    method: 'post',
    loading:true,
  })
}
//
export function getStaffSettleAccount(){
  return request.ajax({
    url: `/adaMember/getStaffSettleAccount`,
    method: 'post',
    loading:true,
  })
}
export function delSettleAccount(){
  return request.ajax({
    url: `/adaMember/delSettleAccount`,
    method: 'post',
    loading:true,
  })
}
export function delMember(){
  return request.ajax({
    url: `/adaMember/delMember`,
    method: 'post',
    loading:true,
  })
}

// 获取当前用户实名信息(员工端)
export function getClerkMember(){
  return request.ajax({
    url: `/adaMember/getClerkMember`,
    method: 'post',
    loading: true,
    handleError: true
  })
}

// 获取当前商户结算账户信息(员工端)
export function getClerkSettleAccount(){
  return request.ajax({
    url: `/adaMember/getClerkSettleAccount`,
    method: 'post',
    loading: true,
    handleError: true
  })
}

// 员工的实名信息添加
export function createClerkMember(params){
  return request.ajax({
    url: `/adaMember/createClerkMember`,
    method: 'post',
    loading: true,
    params,
    message: true,
    handleError: true
  })
}

// 员工的实名信息添加
export function createClerkSettleAccount(params){
  return request.ajax({
    url: `/adaMember/createClerkSettleAccount`,
    method: 'post',
    loading: true,
    params,
    message: true,
    handleError: true
  })
}

// 删除当前实名信息
export function delClerkMember(){
  return request.ajax({
    url: `/adaMember/delClerkMember`,
    method: 'post',
    loading: true,
    message: true,
    handleError: true
  })
}

// 删除结算账号
export function delClerkSettleAccount(){
  return request.ajax({
    url: `/adaMember/delClerkSettleAccount`,
    method: 'post',
    loading: true,
    message: true,
    handleError: true
  })
}

// 员工端-申请提现
export function applyStaffDrawcash(){
  return request.ajax({
    url: `/finance/applyStaffDrawcash`,
    method: 'post',
    loading: true,
    message: true,
    handleError: true
  })
}

//员工端提现记录/store/finance/listClerkDrawcashRecord
export function listClerkDrawcashRecordWithStuff(){
  return request.ajax({
    url: `/finance/listClerkDrawcashRecord`,
    method: 'post',
    loading: true,
    handleError: true
  })
}

// 员工端-刷新推广二维码
export function refreshStoreBelow(){
  return request.ajax({
    url: `/user/refreshStoreBelow`,
    method: 'post',
    loading: true,
    handleError: true
  })
}