
import request from '../utils/request'


export function getBalance(){
  return request.ajax({
    url: `/finance/getBalance`,
    method: 'post'
  })
}
export function listBalanceRecord(params){
  return request.ajax({
    url: `/finance/listBalanceRecord`,
    method: 'post',
    params
  })
}
export function listDrawcashRecord(params){
  return request.ajax({
    url: `/finance/listDrawcashRecord`,
    method: 'post',
    params
  })
}

export function applyDrawcash(){
  return request.ajax({
    url: `/finance/applyDrawcash`,
    method: 'post'
  })
}

export function getList(params) {
  return request.ajax({
    url: `/order/getStoreFinance`,
    method: 'post',
    params
  })
}

export function getDetailList(params) {
  return request.ajax({
    url: `/order/getStoreFinanceDetail`,
    method: 'post',
    params
  })
}