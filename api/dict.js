//字典控制
import request from '../utils/request'

//根据类型取值
export function getDictData(type) {
    return request.ajax({
        url: `/dict/list/${type}`,
        method: 'get'
    })
}