//分类
import request from '../utils/request'

//根据类型取值
export function getList(id,type) {
    return request.ajax({
        url: `/technicianOpus/getList/${id}/${type}`,
        method: 'get'
    })
} 
export function getTechnicianOpusList(type) {
    return request.ajax({
        url: `/technicianOpus/getTechnicianOpusList/${type}`,
        method: 'get'
    })
} 
//创建新的
export function create(params) {
    return request.ajax({
        url: `/technicianOpus/create`,
        method: 'put',
        isJson:true,
        params
    })
}
//创建新的
export function update(params) {
    return request.ajax({
        url: `/technicianOpus/edit`,
        method: 'put',
        isJson:true,
        params
    })
}
//创建新的
export function getOne(id) {
    return request.ajax({
        url: `/technicianOpus/getOne/${id}`,
        method: 'get'
    })
}
//创建新的
export function del(id) {
    return request.ajax({
        url: `/technicianOpus/del/${id}`,
        method: 'get'
    })
}