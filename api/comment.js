import request from '../utils/request'

export function getList(params) {
    return request.ajax({
        url: `/comment`,
        method: 'post',
        params
    })
}
export function commit(params) {
    return request.ajax({
        url: `/comment/reply`,
        method: 'post',
        isJson:true,
        loading:true,
        params
    })
}