//技师相关
import request from '../utils/request'

//增加技师
export function create(params) {
  return request.ajax({
      url: `/technician`,
      method: 'put',
      isJson:true,
      params
  })
}
//修改技师
export function update(params) {
  return request.ajax({
      url: `/technician`,
      method: 'post',
      isJson:true,
      params
  })
}
//技师列表
export function list() {
  return request.ajax({
      url: `/technician`,
      method: 'get',
  })
}
//删除单个
export function del(id) {
  return request.ajax({
      url: `/technician/del/${id}`,
      method: 'post',
  })
}
//停用
export function close(id) {
  return request.ajax({
      url: `/technician/close/${id}`,
      method: 'post',
  })
}
//启用
export function open(id) {
  return request.ajax({
      url: `/technician/open/${id}`,
      method: 'post',
  })
}
//启用
export function getOne(id) {
  return request.ajax({
      url: `/technician/getOne/${id}`,
      method: 'post',
  })
}
export function getOneInfo() {
  return request.ajax({
      url: `/technician/getOneInfo`,
      method: 'post',
  })
}