import request from '../utils/request'

export function create(params) {
    return request.ajax({
        url: `/coupons/add`,
        method: 'post',
        loading:true,
        isJson:true,
        params
    })
}

export function getList(params) {
    return request.ajax({
        url: `/coupons/list`,
        method: 'get',
        loading:true,
        params
    })
}

export function up(id){
    return request.ajax({
        url: `/coupons/shelves/up/${id}`,
        method: 'get',
        loading:true,
    })
}
export function down(id){
    return request.ajax({
        url: `/coupons/shelves/down/${id}`,
        method: 'get',
        loading:true,
    })
}

export function getQuanDetail(id) {
  return request.ajax({
    url: `/coupons/${id}`,
    method: 'get',
    loading:true,
  })
}

//启用优惠券修改
export function editCoupons(params) {
  return request.ajax({
    url: `/coupons/edit`,
    method: 'post',
    loading:true,
    isJson:true,
    params
})
}

