//字典控制
import request from '../utils/request'
//创建团单
export function create(params) {
  return request.ajax({
      url: `/storeProduct`,
      method: 'put',
      isJson:true,
      params
  })
}
export function update(params) {
  return request.ajax({
      url: `/storeProduct/edit`,
      method: 'put',
      isJson:true,
      params
  })
}
export function get(id) {
  return request.ajax({
      url: `/storeProduct/${id}`,
      method: 'get',
      loading:true
  })
}
export function getList(params) {
  return request.ajax({
      url: `/storeProduct/list`,
      method: 'get',
      loading:true,
      params
  })
}
export function shelvesUp(id) {
  return request.ajax({
      url: `/storeProduct/shelves/up/${id}`,
      method: 'post',
      loading:true,
  })
}
export function shelvesDown(id) {
  return request.ajax({
      url: `/storeProduct/shelves/down/${id}`,
      method: 'post',
      loading:true,
  })
}
export function tongbuSub(params) {
  return request.ajax({
      url: `/storeProduct/tongbuSub`,
      method: 'post',
      loading:true,
      params
  })
}