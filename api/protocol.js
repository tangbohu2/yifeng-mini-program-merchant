import request from '../utils/request'

export function getTemplate(code) {
    return request.ajax({
        url: `/protocol/getTemplate/${code}`,
        method: 'post',
    })
}