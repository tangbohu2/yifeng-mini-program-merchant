import request from '../utils/request'
/**
 * 获取设置信息
 */
export function appointmentSetInfo(){
  return request.ajax({
    url: `/appointment/appointmentSetInfo`,
    method: 'post',
    loading:true
  })
}

export function appointmentEdit(params){
  return request.ajax({
    url: `/appointment/appointmentEdit`,
    method: 'post',
    isJson:true,
    loading:true,
    params
  })
}

export function listRecordInfo(){
  return request.ajax({
    url: `/appointment/listRecordInfo`,
    method: 'post',
    loading:true
  })
}

export function listRecord(params){
  return request.ajax({
    url: `/appointment/listRecord`,
    method: 'post',
    loading:true,
    params
  })
}


export function handleAppointment(params){
  return request.ajax({
    url: `/appointment/handleAppointment`,
    method: 'post',
    loading:true,
    params
  })
}