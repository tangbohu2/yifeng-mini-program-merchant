import request from '../utils/request'

export const getList = (params) => {
  return request.ajax({
    url: `/staff/getList`,
    method: 'get',
    loading:true,
    isJson:true,
    params
  })
}

export const addStaff = (params) => {
  return request.ajax({
    url: `/staff/add`,
    method: 'post',
    loading:true,
    isJson:true,
    params
  })
}
// /staff/edit
export const editStaff = (params) => {
  return request.ajax({
    url: `/staff/edit`,
    method: 'post',
    loading:true,
    isJson:true,
    params
  })
}
// /storeProduct/shelvesnew/up/{id}
export const upnew = (id) => {
  return request.ajax({
    url: `/storeProduct/shelvesnew/up/${id}`,
    method: 'post',
    loading:true,
    isJson:true
  })
}
// /storeProduct/shelvesnew/down/{id}
export const downnew = (id) => {
  return request.ajax({
    url: `/storeProduct/shelvesnew/down/${id}`,
    method: 'post',
    loading:true,
    isJson:true
  })
}

///storeProduct/productConfig
/**
 * 
 * @param {staff_money,is_after_sales,as_num,as_start_time,as_end_time,as_money} param0 
 */
export const configSet = (params) => {
  return request.ajax({
    url: `/storeProduct/productConfig`,
    method: 'post',
    loading:true,
    isJson:true,
    params
  })
}

// /storeProduct/showProductConfig/{id}
export const getConfig = (id) => {
  return request.ajax({
    url: `/storeProduct/showProductConfig/${id}`,
    method: 'get',
    loading:true,
    isJson:true
  })
}

//员工登录
export const staffLoginByUserPwd = (params) => {
  return request.ajax({
    url: `/storeUser/loginByUserPwd`,
    method: 'post',
    loading:true,
    params
  })
}

//员工首页
export const staffIndex = () => {
  return request.ajax({
    url: `/user/getIndexData`,
    method: 'post',
    loading:true,
  })
}

//我的分销
export const staffDistribution = () => {
  return request.ajax({
    url: `/user/getData`,
    method: 'post',
    loading:true,
  })
}

//项目佣金明细
export const projectCommissionInfo = (params) => {
  return request.ajax({
    url: `/user/getcCommissionData`,
    method: 'post',
    loading:true,
    params
  })
}

//分销佣金明细
export const distributionCommissionInfo = (params) => {
  return request.ajax({
    url: `/user/getcDivCommissionData`,
    method: 'post',
    loading:true,
    params
  })
}

//员工端分销佣金明细
export const distributionCommissionInfoWithStuff = (params) => {
  return request.ajax({
    url: `/user/getcCommissionData`,
    method: 'post',
    loading:true,
    params
  })
}

//我的客户明细
export const customInfo = (params) => {
  return request.ajax({
    url: `/user/getMyMember`,
    method: 'post',
    loading:true,
    params
  })
}

//删除员工store
export const deleteStaff = (staffId) => {
  return request.ajax({
    url: `/staff/del/${staffId}`,
    method: 'get',
    loading:true
  })
}

//获取员工详情/staff/getOne/{staffid}
export const getStaffDetail = (staffid) => {
  return request.ajax({
    url: `/staff/getOne/${staffid}`,
    method: 'get',
    loading:true
  })
}

//获取商品详情store/storeProduct/getInfo/{id}
export const getProductDetail = (id) => {
  return request.ajax({
    url: `/storeProduct/getInfo/${id}`,
    method: 'get',
    loading:true
  })
}

//获取商品详情（商家端）
export const getStoreDetail = (data) => {
  return request.ajax({
    url: `/storeProduct/getInfo`,
    method: 'post',
    loading:true,
    data
  })
}

//获取短信模板
export const getSmsTmp = () => {
  return request.ajax({
    url: `/order/smsTemplateList`,
    method: 'post',
    loading:true
  })
}

//发送短信/store/order/sendSms
export const sendSms = (data) => {
  return request.ajax({
    url: `/order/sendSms`,
    method: 'post',
    loading:true,
    data
  })
}

//获取员工详情/store
export const getStaffInfo = (data) => {
  return request.ajax({
    url: `/staff/getStaffInfo`,
    method: 'post',
    loading:true,
    data
  })
}

//获取验券记录
export const getListNew = (data) => {
  return request.ajax({
    url: `/order/getListNew`,
    method: 'post',
    loading:true,
    data
  })
}

//获取验券记录（改）
export const getListNewChange = (data) => {
  return request.ajax({
    url: `/order/getAftersaleList`,
    method: 'post',
    loading:true,
    data
  })
}

//商家端获取分类（订单使用）
export const getOrderSortList = () => {
  return request.ajax({
    url: `/order/getStoreClassifyList`,
    method: 'post',
    loading:true
  })
}

//获取access token /store/mall/bbs/getAccessToken
export function getAccess() {
  return request.ajax({
    url: '/mall/bbs/getAccessToken',
    method: 'post'
  })
}

//提现/store/order/applyDrawcash
export function applyDrawcash() {
  return request.ajax({
    url: '/order/applyDrawcash',
    method: 'post'
  })
}

//提现记录/store/order/drawcashStoreList
export function drawcashStoreList() {
  return request.ajax({
    url: '/order/drawcashStoreList',
    method: 'post'
  })
}

//上门预约订单列表（门店）/store/doorserver/doorserverStoreOrderList
export function getServiceForStore(params) {
  return request.ajax({
    url: '/doorserver/doorserverStoreOrderList',
    method: 'get',
    params
  })
}

//上门服务详情/store/doorserver/doorserverOrderDetail
export function getServeDetail(id) {
  return request.ajax({
    url: '/doorserver/doorserverOrderDetail?id='+id,
    method: 'post'
  })
}

//上门预约订单列表（员工）/store/doorserver/doorserverStaffOrderList
export function getStaffOrderList(params) {
  return request.ajax({
    url: `/doorserver/doorserverStaffOrderList`,
    method: 'get',
    loading:true,
    params
  })
}

//指派员工列表/store/doorserver/getStaffList
export function getStaffList() {
  return request.ajax({
    url: '/doorserver/getStaffList',
    method: 'post'
  })
}

//指派员工/store/doorserver/addDoorStoreUser
export function assignStaff(params) {
  return request.ajax({
    url: '/doorserver/addDoorStoreUser',
    method: 'post',
    params
  })
}