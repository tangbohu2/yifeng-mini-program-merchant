import request from '../../utils/request'

//点赞收藏
export function like(params) {
  return request.ajax({
      url: `/mall/bbs/options/add`,
      method: 'post',
      params
  })
}
export function del(params) {
  return request.ajax({
      url: `/mall/bbs/options/del`,
      method: 'post',
      params
  })
}
export function getMineData(id) {
  return request.ajax({
      url: `/mall/bbs/options/getMineData/${id}`,
      method: 'post'
  })
}
export function getOptionsCount(id) {
  return request.ajax({
      url: `/mall/bbs/options/getOptionsCount/${id}`,
      method: 'post'
  })
}