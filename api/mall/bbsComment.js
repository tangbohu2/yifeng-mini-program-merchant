import request from '../../utils/request'

export function getList(params) {
  return request.ajax({
      url: `/mall/bbs/comments/getList`,
      method: 'post',
      params
  })
} 
export function getCount(id) {
  return request.ajax({
      url: `/mall/bbs/comments/getCount/${id}`,
      method: 'post'
  })
}
export function add(params){
  return request.ajax({
    url: `/mall/bbs/comments/add`,
    method: 'post',
    loading:true,
    params
  })
}
export function loadMore(id){
  let params = {maxId:id}
  return request.ajax({
    url: `/mall/bbs/comments/loadMore`,
    method: 'post',
    params
  })
}