import request from '../../utils/request'

export function publish(params) {
  return request.ajax({
      url: `/mall/bbs/publish`,
      method: 'post',
      params
  })
}
export function getList(params) {
  return request.ajax({
      url: `/mall/bbs/getList`,
      method: 'post',
      loadding:true,
      params
  })
}
export function getDetail(id) {
  return request.ajax({
      url: `/mall/bbs/getDetail/${id}`,
      method: 'post',
      loadding:true
  })
}
export function del(id) {
  return request.ajax({
      url: `/mall/bbs/del/${id}`,
      method: 'post',
      loadding:true
  })
}
export function getSearch(params) {
  return request.ajax({
      url: `/mall/bbs/getSearch`,
      method: 'post',
      loadding:true,
      params
  })
}