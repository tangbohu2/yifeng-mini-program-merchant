import request from '../utils/request'

export function getList(params) {
    return request.ajax({
        url: `/teaching`,
        method: 'post',
        params
    })
}
export function getOne(id) {
    return request.ajax({
        url: `/teaching/getOne/${id}`,
        method: 'post',
    })
}