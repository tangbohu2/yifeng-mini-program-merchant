const config = require('./config.js')
import check from './checkToken'
// const host = config.model === "dev"? config.devServer : config.httpServer
const host = config.httpServer; // 服务器baseUrl
// const TenantId = config.TenantId
class Ajax {
  header = {
    timeOffset: 8000,
    'content-type': 'application/x-www-form-urlencoded'
  }
  // 响应是否成功 1成功 0 失败
  RESPONSE_STATUS = 1
  constructor(header, NODE_ENV, RESPONSE_STATUS) {
    this.header = header
    this.RESPONSE_STATUS = RESPONSE_STATUS
  }

  // 请求拦截器
  request(config) {
    /** 配置全屏加载 */
    if (config.loading) {
    //   wx.showLoading({title: '加载中...',mask:true})
    }
    const token = wx.getStorageSync('token')
    const storeId = wx.getStorageSync('storeId')
    /** query参数传递 只适用于get*/
    if (config.params) {
      config.data = config.params
    }
    config = {
      ...config,
      header: {
        "Authorization":token,
        "StoreId":storeId,
        timeOffset: 8000,
        'content-type': 'application/x-www-form-urlencoded',
      }
    }
    // json 提交
    if (config.isJson) {
      config = {
        ...config,
        header: {
          "Authorization":token,
          "StoreId":storeId,
          timeOffset: 8000,
          "content-type": 'application/json'
        }
      }
    }
    return config
  }

  // 响应拦截器
  response (response, config = null) {
    if(response.data.code === 500) {
        // console.log(config)
      wx.hideLoading()
      if(!['/adaMember/getClerkMember'].includes(config.url)) {
        wx.showToast({ title: response.data.msg, icon: 'none'})
      }
      return response.data
    }
    if (config && config.handleError && response.data.code !== 200) {
      wx.hideLoading()
      wx.showToast({ title: response.data.msg, icon: 'none' })
      return response.data
    }
    if(this.RESPONSE_STATUS) { // 成功
      if (config && config.message) {
        wx.showToast({ title: response.data.msg, icon: 'none' })
      }
      wx.hideLoading()
      return response.data
    } else { // 失败
      wx.hideLoading()
      const error_response = response.data || {}
      if (error_response.msg !== '') {
        wx.showToast({ title: error_response.msg, icon: 'none' })
        return response.data
      }
      if (error_response.message !== false) {
        let _message = response.code === 'ECONNABORTED' ? '连接超时，请稍候再试！' : '网络错误，请稍后再试！'
        // 错误提示
        wx.showToast({ title: error_response.message || _message, icon: 'none' })
      }
      return response.data
    }
  }

  ajax(options = {}) {
    return new Promise((resolve, reject) => {
      check(options).then(()=>{
        // 拦截 请求
        const config = this.request(options)
        // 处理url
        wx.request({
          url: config.isComplete ? host+config.url : host+"/store"+config.url,
          data: config.data,
          header: config.header || this.header,
          method: config.method || 'get',
          success: res => { // 1 为成功 0 为失败

            if (res.statusCode === 200) {
              // 拦截 响应
              this.RESPONSE_STATUS = 1
              const _res = this.response(res, config)
              if (config.handleError && res.data.code !== 200) {
                reject(_res)
              }
              resolve(_res)
            } else {
              this.RESPONSE_STATUS = 0
              const _res = this.response(res)
              reject(_res)
            }
          },
          fail: res => {// 失败
            this.RESPONSE_STATUS = 0
            const _res = this.response(res)
            reject(_res)
          }
        })
      })
    })
  }

  get({
    url = 'localhost',
    data = {},
    success = null,
    fail = null,
    complete = null,
    callback = null,
    that = null,
  } = {}) {
    return this.ajax({
      url,
      data,
      method: 'GET',
      success,
      fail,
      complete,
      callback,
      that
    });
  }

  post({
    url = 'localhost',
    data = {},
    success = null,
    fail = null,
    that = null,
    complete = null,
    callback = null
  } = {}) {
    return this.ajax({
      url,
      data,
      method: 'POST',
      success,
      fail,
      that,
      complete,
      callback
    })
  }

  put({
    url = 'localhost',
    data = {},
    success = null,
    fail = null,
    that = null,
    complete = null,
    callback = null
  } = {}) {
    return this.ajax({
      url,
      data,
      method: 'POST',
      success,
      fail,
      that,
      complete,
      callback
    })
  }

}

export default new Ajax();
