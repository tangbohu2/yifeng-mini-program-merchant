const formatTime = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()
  return [year, month, day].map(formatNumber).join('/') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}
function dateAddDays(dataStr,dayCount){
  var strdate = dataStr; // 
  strdate=strdate.replace("-","/");
  strdate=strdate.replace("-","/");
  strdate=strdate.replace("-","/");
  var isdate = new Date(strdate);
  isdate = new Date((isdate/1000+(86400*dayCount))*1000); // dayCount=1
  var year = isdate.getFullYear(); // yyyy
  var month = isdate.getMonth()+1; // M
  var day = isdate.getDate(); // d

  if (month >= 1 && month <= 9) { // MM
      month = "0" + month;
  }
  if (day >= 0 && day <= 9) { // dd
      day = "0" + day;
  }

  var pdate = year+"-"+month+"-"+day; 
  return pdate;
}
const getDate= date =>{
  if(!date){
    date=new Date();
  }
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  return [year, month, day].map(formatNumber).join('-');
}
const getDateNY= date =>{
  if(!date){
    date=new Date();
  }
  const year = date.getFullYear()
  const month = date.getMonth() + 1

  return [year, month].map(formatNumber).join('-');
}
const getTimeSF= date =>{
  if(!date){
    date=new Date();
  }
  const hour = date.getHours()
  const minute = date.getMinutes()
 
  return [hour, minute].map(formatNumber).join(':')
}
const getTimeSFM= date =>{
  if(!date){
    date=new Date();
  }
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()
  return [hour, minute, second].map(formatNumber).join(':')
}
const getHours= date=>{
  const hour = date.getHours();
  return hour;
}
const getMinutes= date=>{
  const minute = date.getMinutes();
  return minute;
}
const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : '0' + n
}
/*var formatDate = function (date, type) {
  var str = type ? type : "yyyy-MM-dd hh:mm:ss"  //默认格式
  var date = getDate(datetime);
  var year = date.getFullYear(); //年
  var month = date.getMonth() + 1 > 10 ? date.getMonth() + 1 : ('0' + (date.getMonth() + 1)); //月
  var day = date.getDate() > 10 ? date.getDate() : '0' + date.getDate(); //日
  var hours = date.getHours() > 10 ? date.getHours() : '0' + date.getHours() //时
  var minutes = date.getMinutes() > 10 ? date.getMinutes() : '0' + date.getHours() //分
  var seconds = date.getSeconds() > 10 ? date.getSeconds() : '0' + date.getSeconds() //秒
  // console.log(year,month,day,hours,minutes,seconds)
  str = str.replace(getRegExp('yyyy', 'g'), year)
  str = str.replace(getRegExp('MM', 'g'),month)
  str = str.replace(getRegExp('dd', 'g'),day)
  str = str.replace(getRegExp('hh', 'g'),hours)
  str = str.replace(getRegExp('mm', 'g'),minutes)
  str = str.replace(getRegExp('ss', 'g'),seconds)
  // console.log(str, 484848)
  // return [year, month, day].map(formatNumber).join('-');
  return str
}*/
//禁止按重复点击
function btnDis(fn, gapTime) {
  if (gapTime == null || gapTime == undefined) {
    gapTime = 1500
  }
  let _lastTime = null
  // 返回新的函数
  return function () {
    let _nowTime = + new Date()
    if (_nowTime - _lastTime > gapTime || !_lastTime) {
      fn.apply(this, arguments)   //将this和参数传给原函数
      _lastTime = _nowTime
    }
  }
}
module.exports = {
  formatTime: formatTime,
  getDate:getDate,
  getDateNY:getDateNY,
  getHours: getHours,
  getMinutes:getMinutes,
  //formatDate:formatDate,
  getTimeSF:getTimeSF,
  getTimeSFM:getTimeSFM,
  btnDis:btnDis,
  dateAddDays:dateAddDays
}
