/*
req.js
请求外部封装类
*/
const config = require('./config.js')
var app = getApp();
// const host = config.model === "dev"? config.devServer : config.httpServer
const host = config.httpServer; // 服务器baseUrl
/**
 * POST请求，
 * URL：接口
 * postData：参数，json类型
 * doSuccess：成功的回调函数
 * doFail：失败的回调函数
 */
function postData(url, postData, doSuccess, doFail) {
  var cookieid=wx.getStorageSync('cookieid');
  wx.request({
    //项目的真正接口，通过字符串拼接方式实现
    url: host + url,
    // 这个header根据你的实际改！
    header: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'cookie': cookieid,
      'Request-Origin': 'app'
    },
    data: postData,
    method: 'POST',
    success: function (res) {
      //参数值为res.data,直接将返回的数据传入

      wx.setStorageSync('cookieid',res.header["Set-Cookie"]);
      doSuccess(res.data);
    },
    fail: function (res) {
      doFail(res);
    },
  })
}


/**
 * GET请求，
 * URL：接口
 * getData：参数，json类型
 * doSuccess：成功的回调函数
 * doFail：失败的回调函数
 */
function getData(url, getData, doSuccess, doFail) {
  var cookieid=wx.getStorageSync('cookieid');
  const token=wx.getStorageSync('token');
  wx.request({
    url: host + url,
    header: {
      'Authorization':token,
      'Content-Type': 'application/json',
      'cookie': cookieid,
      'Request-Origin': 'app'
    },
    method: 'GET',
    data: getData,
    success: function (res) {
      var cookieset=res.header["Set-Cookie"];
      if(cookieset){
        wx.setStorageSync('cookieid',cookieset);
      }
      doSuccess(res.data);
    },
    fail: function (res) {
      doFail(res);
    },
  })
}

module.exports.postData = postData;
module.exports.getData = getData;
