const config = require('./config.js')
// const host = config.model === "dev"? config.devServer : config.httpServer
const host = config.httpServer; // 服务器baseUrl

export function upload(path,success,fail){
    wx.showLoading({
      title: '上传中...',
      mask:true
    })
    wx.uploadFile({
        url:host+'/store/common/upload',
        filePath:path,
        name:'file',
        success(res) {
            wx.hideLoading()
           success(res)
        },
        fail(res) {
            wx.hideLoading()
            fail(res)
        }
    })
}
