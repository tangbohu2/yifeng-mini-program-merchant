/*
 所有调用服务器操作在这里定义module.exports.***
 同一请求地址只可以存在一个入口
*/
import request from "./request";
var http = require('./http.js');


module.exports={
  autoBind:autoBind,
  autoLogin:autoLogin,
  registBind:registBind,
  doGetData:doGetData,
  checkOnline:checkOnline,
  reloadMemberInfo:reloadMemberInfo,
  removeToken:removeToken
};
function doGetData(url, getData, doSuccess, doFail){
    http.getData("/store"+url,getData,doSuccess,doFail);
}
function doPost(url, getData, doSuccess, doFail){
    http.postData("/store"+url,getData,doSuccess,doFail);
}
function removeToken( doSuccess, doFail){
  wx.showLoading({
    title: '加载中',
    mask:true
  })
  doGetData('/storeUser/remove-token',{},(rep) => {
    wx.hideLoading();


    doSuccess(rep);
  }, (error) => {

    doFail();
  })
}
//绑定注册
function autoBind(code, doSuccess, doFail){
  doGetData('/member/getOpenId', {code:code}, (rep) => {

    doSuccess(rep);
  }, (error) => {
    doFail();
  })
}
//绑定登录
function autoLogin(code, doSuccess, doFail){
  wx.showLoading({
    title: '加载中...',
  })
  doGetData('/member/auto-login', {openId:code}, (rep) => {
    wx.hideLoading()
    doSuccess(rep);
  }, (error) => {
    wx.hideLoading()
    doFail();
  })
}
//是否在线
function checkOnline(openId,code,doSuccess, doFail){
  doGetData('/member/check-login', {openId:openId,code:code}, (rep) => {

    doSuccess(rep);
  }, (error) => {

    doFail(error);
  })
}
function registBind(member){
  return request.ajax({
    url: `/member/regist-bind`,
    method: 'post',
    params:member,
    isJson:true
  })
}
function reloadMemberInfo(member){
  return request.ajax({
    url: `/member/reload-info`,
    method: 'post',
    params:member,
    isJson:true,
    loading:true
  })
}
