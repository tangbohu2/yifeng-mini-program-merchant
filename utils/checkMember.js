export default function check() {
  return new Promise((resolve, reject) => {
    const member = wx.getStorageSync('member');
    if(!member){
      wx.navigateTo({
        url: '/pages/UCenter/register/register',
      })
    }else{
      resolve()
    }
  })
}