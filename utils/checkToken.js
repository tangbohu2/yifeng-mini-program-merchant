
export default function checkToken(options) {
    return new Promise((resolve, reject) => {
        if(options.noNeedToken){
            resolve()
            return
        }
        //用户
        const member = wx.getStorageSync('member')
        //token
        const token = wx.getStorageSync('token')
        var openId = getApp().globalData.openId || wx.getStorageSync('openid')
        // let arr = ['/storeUser/loginByUserPwd']
        if(member && token && openId){
            resolve()
            return
        }
        if(!member || !token){
          let pages = getCurrentPages()
          let prevPages = pages[pages.length - 1]
          if (prevPages.route !== "pages/middlePage/middlePage" && prevPages.route !== "pages/storeApply/storeApply") {
            wx.reLaunch({
              url: '/pages/middlePage/middlePage',
            })
          }
          getApp().login().then((res)=>{
            resolve()
            return
          })
        }
    })
}
