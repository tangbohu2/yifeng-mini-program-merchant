/*
config.js
配置文件 常用配置参数
*/
/**测试环境 */
// let domain = 'https://wxtest.yiqisa.cn/prod-api'
// let socketDomain = 'wss://wxtest.yiqisa.cn/prod-api/ws/connect/talk'
// let domain = 'https://wxmaster.yiqisa.cn/prod-api'
// let socketDomain = 'wss://wxmaster.yiqisa.cn/prod-api/ws/connect/talk'
// let domain = 'http://localhost:8080'
// let socketDomain = 'ws://localhost:8080/ws/connect/talk'
// let domain = 'http://1e54905h23.vicp.fun'
// let socketDomain = 'ws://1e54905h23.vicp.fun/ws/connect/talk'
// let domain = 'https://wx.yiqisa.cn/prod-api'
// let socketDomain = 'wss://wx.yiqisa.cn/prod-api/ws/connect/talk'
// let domain = 'https://wxmaster.yiqisa.cn/prod-api'
// let socketDomain = 'wss://wxmaster.yiqisa.cn/prod-api/ws/connect/talk'


let domain = 'https://yifeng.doing365.com'
let socketDomain = 'wss://yifeng.doing365.com/ws/connect/talk'

// let domain = 'https://wx.yiqisa.cn/prod-api'
// let socketDomain = 'wss://wx.yiqisa.cn/prod-api/ws/connect/talk'
module.exports = {
  httpServer: domain,  // 业务服务器地址 每个人的不一样，按照需要改！
  socketServer: socketDomain,//正式环境必须使用wss，只要配置https就可以使用wss
  QQMapKey:'LLUBZ-NIAWP-LAODJ-LCCIR-HSEQJ-B6BF5'
}