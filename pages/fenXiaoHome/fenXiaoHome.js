// pages/fenXiaoHome/fenXiaoHome.js
import * as API_Mall_With_Draw from '../../api/withDraw'
import * as API_Adamember from '../../api/adaMember'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    img:'',
    yesterday:0,
    count:0,
    hasSettleAccount:false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      img: wx.getStorageSync('store').belowImageUrl
    })
    this.loadData()
  },
  loadData(){
    API_Mall_With_Draw.getData().then(res=>{
      console.log(res)
      this.setData({
        yesterday:res.data.yesterday,
        count:res.data.count
      })
    })
  },
  goCustomer(){
    wx.navigateTo({
      url: '../fenXiaoCustomer/fenXiaoCustomer',
    })
  },
  goRecord(){
    wx.navigateTo({
    //   url: '../fenXiaoCommission/fenXiaoCommission?count='+count,
    url: '../fenXiaoCommission/fenXiaoCommission',
    })
  },
  showImg(e){
      console.log(this.data.hasSettleAccount)
    if(!this.data.hasSettleAccount) {
      wx.showToast({
        title: '请先绑定结算卡',
        icon: 'none'
      })
      return
    }
   let img = e.currentTarget.dataset.img
   if (!img) {
    let data = {
      storeId: wx.getStorageSync('storeId')
    }
    API_Mall_With_Draw.refreshStoreCommission(data).then(({ data })=>{
      let store = wx.getStorageSync('store')
      store.belowImageUrl = data.belowImageUrl
      store.belowImageLocal = data.belowImageLocal
      store.belowPage = data.belowPage
      wx.setStorageSync('store', store)
      img = data.belowImageUrl
      this.setData({
        img: data.belowImageUrl
      })
      wx.previewImage({
        urls: [img],
        current:img
      })
    })
   } else {
    wx.previewImage({
      urls: [img],
      current:img
    })
   }
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
      const that = this
    API_Adamember.getSettleAccount().then(res=>{
      if(res.code==200){
        if(!res.data){
            console.log(1)
          that.setData({hasSettleAccount:false});
        }else{
            
          that.setData({hasSettleAccount:true});
          console.log(that.data.hasSettleAccount)
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})