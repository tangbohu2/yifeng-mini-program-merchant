// pages/technicianOpusEdit/technicianOpusEdit.js
import * as API_Dict from '../../api/dict'
import * as API_Upload from '../../utils/fileUpload'
import * as API_Opus from '../../api/technicianOpus'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    opusTypeArray:[],
    opusTypeIndex:0,
    showContent:'',
    opus:{
      techId:null,
      opusName:'',
      opusType:'',
      opusUrl:'',
      opusVideoUrl:''
    },
    submitState:true
  },

  //修改作品类型
  changeType(e){
    let index =e.detail.value;
    this.setData({
      opusTypeIndex:index,
      'opus.opusType':this.data.opusTypeArray[index].dictValue,
      showContent:this.data.opusTypeArray[index].dictLabel
    })
  },
  changeName(e){
    this.setData({'opus.opusName':e.detail.value})
  },

  //上传
  uploadImg: function (e) {
    var that = this
    wx.showActionSheet({
      itemList: ['相册', '相机'],
      itemColor: "#40ba67",
      success: function (res) {
      if (!res.cancel) {
        if (res.tapIndex == 0) {
          that.chooseWxImageShop('album')
        } else if (res.tapIndex == 1) {
          that.chooseWxImageShop('camera')
        }
      }
      }
    })
  },
  chooseWxImageShop: function (type) {
    var that = this;
    if(this.data.showContent == '图片'){
      wx.chooseImage({
        count: 1,
        sizeType: ['compressed'],
        sourceType: [type],
        success: function (res) {
          //门店形象照
          API_Upload.upload(res.tempFilePaths[0],res=>{
            const {url} = JSON.parse(res.data)
              that.setData({'opus.opusUrl':url})
        },res=>{
            wx.showToast({
                title:'上传失败',
                icon:'none'
            })
        })
        }
      })
    }else{
      wx.chooseMedia({
        count: 1,
        mediaType: ['video'],
        sizeType: ['compressed'],
        sourceType: type,
        success: function (res) {
          debugger
          API_Upload.upload(res.tempFiles[0].tempFilePath,res=>{
            const {url} = JSON.parse(res.data)
            that.setData({'opus.opusVideoUrl': url})
            console.info("url="+url)
          },res=>{
              wx.showToast({
                  title:'上传失败',
                  icon:'none'
              })
          })
          API_Upload.upload(res.tempFiles[0].thumbTempFilePath,res=>{
            const {url} = JSON.parse(res.data)
            that.setData({'opus.opusUrl': url})
          },res=>{
              wx.showToast({
                  title:'上传失败',
                  icon:'none'
              })
          })
        }

    })
    }
  },
  save(){
    let that = this
    that.setData({submitState:!that.data.submitState})
    let opus = that.data.opus
    if(!opus.opusName.trim()){
      wx.showToast({title:'请填写作品名',icon:'none',duration:1500})
      that.setData({submitState:!that.data.submitState})
      return
    }
    if(!opus.opusUrl.trim()){
      wx.showToast({title:'请上传作品',icon:'none',duration:1500})
      that.setData({submitState:!that.data.submitState})
      return
    }
    if(opus.id){
      API_Opus.update(opus).then(res=>{
          var pages = getCurrentPages();
          var beforePage = pages[pages.length - 2];
          wx.navigateBack({
            delta: 0,
            success(e){
              beforePage.getList()
            }
          })
      })
    }else{
      API_Opus.create(opus).then(res=>{
        if(res.id){
          var pages = getCurrentPages();
          var beforePage = pages[pages.length - 2];
          wx.navigateBack({
            delta: 0,
            success(e){
              beforePage.getList()
            }
          })
        }
      })
    }

  },
  /**
   * 生命周期函数--监听页面加载
   */
  // https://yifeng-1308901251.cos.ap-beijing.myqcloud.com/upload/2022/08/25/dee89ebb-b31c-4f2e-878c-29818f95e014.mp4
  onLoad: function (options) {
    console.log(options)
    if(options.id){
      API_Opus.getOne(options.id).then(res=>{
        this.setData({
          opus:res
        })
        API_Dict.getDictData("file_type").then(ref =>{
          for(var i = 0; i<ref.length; i++){
            console.log(res)
            if(res.opusType == ref[i].dictValue){
              this.setData({
                opusTypeArray:ref,
                opusTypeIndex:i,
                showContent:ref[i].dictLabel,
              })
            }
          }
        })
      })
    }else{
      API_Dict.getDictData("file_type").then(res =>{
        this.setData({
          opusTypeArray:res,
        })
        const type = options.type
    if(type == 1) {
      this.setData({
        opusTypeIndex: 1,
        showContent: '视频',
        'opus.opusType': 'video'
      })
    } else {
      this.setData({
        // opusTypeArray:res,
        'opus.opusType':res[0].dictValue,
        showContent:res[0].dictLabel
      })
    }

        if(options.techId){

          this.setData({
            'opus.techId':options.techId
          })
        }
       
      })
    }

    // const type = options.type
    // if(type == 1) {
    //   this.setData({
    //     opusTypeIndex: 1,
    //     showContent: '视频',
    //     'opus.opusType': 'video'
    //   })
    // } else {
    //   this.setData({
    //     opusTypeIndex: 0
    //   })
    // }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

   
})