// pages/messageList/messageList.js
import * as socketUtil from'../../api/socketUtil'
import * as API_Member from '../../api/member'
import * as API_Stroe from '../../api/store'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    pageNum:1,
    pageSize:1000,
    msgList:[],
    showFollowSer:'N',
    showMessageList:false
  },
  showBigImg(e){
    if(this.data.timestamp){
      this.data.timestamp=Date.parse(new Date()); 
    }
    let imagesrc="https://wx.yiqisa.cn/mp/qrcode.jpg?v="+ this.data.timestamp;
    wx.previewImage({
      current: imagesrc, // 当前显示图片的http链接
      urls: [imagesrc] // 需要预览的图片http链接列表
    })
   },
  RECEIVE_MSG_INFORM:function(json){
    //收到新消息
    // let reqParam={page:this.data.page,rows:this.data.rows} ;
    // socketUtil.sendData("MY_MSG_LIST",reqParam);
   this.loadMsgList();
   let reqParam={storeId:this.data.store.id} ;
   socketUtil.sendData("MY_MSG_UNREAD_COUNT",reqParam);
  },
  MY_MSG_LIST:function(json){
    //获取历史消息列表
    this.data.msgList=json.info.data;
    this.setData({
      msgList:this.data.msgList
    });
  },
  loadMsgList:function(){
    let reqParam={storeId:this.data.store.id,pageNum:this.data.pageNum,pageSize:this.data.pageSize} ;
    socketUtil.sendData("MY_MSG_LIST",reqParam);
  },
  MY_MSG_UNREAD_COUNT:function(json){
    let unreadMsgCount= json.info.unreadMsgCount;
    if(unreadMsgCount==0){
     wx.removeTabBarBadge({
       index: 1
      })
    }else{
     wx.setTabBarBadge({
       index: 1,
       text: unreadMsgCount+''
      })
    }
  },
  goMessageDetail:function(e){
    
    var memberId=e.currentTarget.dataset.memberid;//与我对话用户的id
    wx.navigateTo({
      url:'../messageDetail/messageDetail?memberId='+memberId
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
   //有切换门店功能，所有刷新都放到onshow
  },
  initShowFollowSer:function(){
    let that=this;
    API_Member.showFollowSer().then(res =>{
      if(res.code==200){
        that.setData({showFollowSer:res.data.showFlag});
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    let store=wx.getStorageSync('store');
    this.setData({store:store})
    getApp().watch(this.MY_MSG_LIST,'MY_MSG_LIST')//注册监听
    getApp().watch(this.RECEIVE_MSG_INFORM,'RECEIVE_MSG_INFORM')//注册监听
    this.loadMsgList();
    let reqParam={storeId:this.data.store.id} ;
    socketUtil.sendData("MY_MSG_UNREAD_COUNT",reqParam);
    this.initShowFollowSer();
    this.initShowView();
  },
/**
   * 初始化哪些需要展示
   */
  initShowView(){
    let that=this;
    API_Stroe.getRoleTypes().then(res=>{
      let showMessageList=false;
      if(res.code==200){
        let list=res.data;
        if(list.indexOf('store_manager')>-1||list.indexOf('store_yunYing')>-1||list.indexOf('store_keFu')>-1){
          showMessageList=true;
        }
      }
      that.setData({
        showMessageList:showMessageList
      })
    })
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

})