// pages/technicianList/technicianList.js
import * as API_Technician from '../../api/technician'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    techList:[],
    params:{
      pageNum:1,
      pageSize:20
    },
    isFinished:false,
  },

  toAdd(){
    wx.navigateTo({
      url: '../technicianAdd/technicianAdd',
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getList()
  },
  flush(){
    this.setData({
      'params.pageNum':1,
      techList:[]
    })
    this.getList()
  },
  getList(){
    API_Technician.list().then(res =>{
     
      let list = this.data.techList
      list.push(...res.data.rows)
      this.setData({
        techList:list,
        isFinished:res.data.lastPage
      })
    })
  },
  //删除
  del(e){
    let that = this
    wx.showModal({
      title:'是否删除该技师？',
      success(res){
        if(res.confirm){
          let id = e.currentTarget.dataset.id
          API_Technician.del(id).then(()=>{
            that.flush()
          })
        }
      }
    })
  
  },
  // 停用
  close(e){
    let id = e.currentTarget.dataset.id
    API_Technician.close(id).then(res=>{
      if(res){
        this.flush()
      }
    })
  },
  // 启用
  open(e){
    let id = e.currentTarget.dataset.id
    API_Technician.open(id).then(res=>{
      if(res){
        this.flush()
      }
    })
  },
  //编辑
  edit(e){
    let id = e.currentTarget.dataset.id
    wx.navigateTo({
      url: '../technicianAdd/technicianAdd?id='+id,
    })
  },
   //编辑
   manageOpus(e){
    let id = e.currentTarget.dataset.id
    wx.navigateTo({
      url: '../technicianOpusList/technicianOpusList?techId='+id,
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    let that = this
    if(that.data.isFinished){
      return
    }
    that.setData({
      'params.pageNum':that.data.params.pageNum + 1
    })
    that.getList()
  },

   
})