// pages/finance/finance.js
import * as API_Finance from '../../api/finance'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    AdapayBalance:{},
    finished:false,
    params:{
      pageSize:20,
      pageNum:1,
    },
    list:[],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.listDrawcashRecord();
    let store = wx.getStorageSync('store')
    this.setData({store:store})
  },
  toBalanceRecord:function(){
    wx.navigateTo({
      url: '/pages/balanceRecord/balanceRecord',
    })
  },
  applyDrawcash:function(){
    let that = this
    wx.showModal({
      cancelColor: '#666',
      title:'提现',
      content:'确定提现吗？',
      success (res) {
        if (res.confirm) {
          API_Finance.applyDrawcash().then(res=>{
            if(res.code==200){
              wx.showToast({title:res.msg,duration:2500,icon:'none',success(e){
                setTimeout(function () {
                  //要延时执行的代码
                  that.getBalance();
                  that.doNext()
                }, 2000) //延迟时间
              }});
            }else{
              wx.showToast({title:res.msg,duration:2500,icon:'none',success(e){
                setTimeout(function () {
                  //要延时执行的代码
                  that.doNext()
                }, 2000) //延迟时间
              }});
            }
        })
        } else if (res.cancel) {
        }
      }
    })
  },
  doNext(){
    let that = this
    that.setData({
      'params.pageNum':1,
      list:[]
    })
    that.listDrawcashRecord();
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
   this.getBalance();
  },
  getBalance:function(){
    let that=this;
    API_Finance.getBalance().then(res=>{
      if(res.code==200){
        if(res.data){
          //获取到余额
          that.setData({AdapayBalance:res.data});
        }else{
          wx.showToast({title:"余额获取失败请联系管理员",duration:1500,icon:'none'});
        }
      }else{
        wx.showToast({title:res.msg,duration:1500,icon:'none'});
      }
  })
  },
  listDrawcashRecord:function(){
    API_Finance.listDrawcashRecord(this.data.params).then(res=>{
      let arr = this.data.list
      arr.push(...res.rows)
      this.setData({
        list:arr,
        finished:res.lastPage
      })
    })
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
 

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  onReachBottom: function () {
    if(this.data.finished){
      return
    }
    var page = this.data.params.pageNum + 1
    this.setData({
      'params.pageNum':page
    })
    this.listDrawcashRecord()
  },
})