// pages/appointmentRecord/appointmentRecord.js
const util = require('../../utils/util')
import * as API_Appointment from '../../api/appointment'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    selectIndex:'',
    platformRemark:'',
    handleAppointmentId:'',
    states:[{name:'已预约',checked:'N',key:'init'},{name:'未到店',checked:'N',key:'before_shop'},{name:'已到店',checked:'N',key:'reach_store'},{name:'已取消',checked:'N',key:'cancel'}],
    showBackMask:false,
    isTechnician:'N',//是否技师进入该页面
    paramArrayIndex:0,
    technicianQueryList:[],//技师信息
    paramArraydIndex:0,
    dateQueryList:[],//可查询的日期信息
    tabindex:0,
    params:{
      appointmentStatus:'',
      appointmentDate:'',
      technicianId:'',
      pageSize:20,
      pageNum:1,
    },
    recordList:[],
    finished:false,
  },
  changeRemarktValue:function(e){
    let platformRemark= e.detail.value;
    this.setData({platformRemark:platformRemark});
  },
  selectState:function(e){
    let key=e.currentTarget.dataset.key;
   this.choiceByKey(key);
  },
  choiceByKey(key){
  let states=this.data.states;
  states.forEach(item=>{
      if(item.key==key){
        item.checked='Y';
      }else{
        item.checked='N';
      }
    })
    this.setData({states:states})
  },
  handleAppointment:function(){
    let appointmentRecordId=this.data.handleAppointmentId;
    let states=this.data.states;
    let appointmentStatus="";
    let index=this.data.selectIndex;
    let recordList=this.data.recordList;
    let selectRecord=recordList[index];
    states.forEach(item=>{
      if(item.checked=='Y'){
        appointmentStatus=item.key;
      }
    })
    let platformRemark=this.data.platformRemark;
    let params={appointmentRecordId:appointmentRecordId,platformRemark:platformRemark,appointmentStatus:appointmentStatus};
    API_Appointment.handleAppointment(params).then(res=>{
      let jso=res.data;
      let appointmentStatusStr=jso.appointmentStatusStr;
      let msg=jso.msg;
      selectRecord.appointmentStatusStr=appointmentStatusStr;
      selectRecord.appointmentStatus=appointmentStatus;
      selectRecord.platformRemark=platformRemark;
      recordList[index]=selectRecord;
      this.setData({recordList:recordList});
      wx.showToast({
          title:msg,
          icon:'none'
      })
    })
   this.colseHandleAppointment();
  },
  colseHandleAppointment:function(){
    this.setData({showBackMask:false});
  },
  showHandleAppointment:function(e){
    // debugger
    let index=e.currentTarget.dataset.index;
    let selectRecord=this.data.recordList[index];
   let appointmentStatus= selectRecord.appointmentStatus;
   this.choiceByKey(appointmentStatus);
   let platformRemark=selectRecord.platformRemark;
    let id=e.currentTarget.dataset.id;
    this.setData({showBackMask:true,handleAppointmentId:id,platformRemark:platformRemark,selectIndex:index});
  },
  toAppointmentEdit:function(){
    wx.navigateTo({
      url: '../appointmentEdit/appointmentEdit?id='+this.data.params.technicianId,
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    API_Appointment.listRecordInfo().then(res=>{
      let jso=res.data;
      let isTechnician=jso.isTechnician;
      let technicianQueryList=jso.technicianQueryList;
      let dateQueryList=jso.dateQueryList;
      this.setData({isTechnician:isTechnician,technicianQueryList:technicianQueryList,dateQueryList:dateQueryList})
      this.listRecord();
    });
  },
reListRecord:function(){

  this.setData({'params.pageNum':1,recordList:[]});
  this.listRecord();
},

  listRecord(){
    API_Appointment.listRecord(this.data.params).then(res=>{
      let data=res.data;
     let recordList=this.data.recordList;
     recordList.push(...data.rows);
      this.setData({recordList:recordList, finished:data.lastPage})
    });
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
   
  },
  paramArrayChange:function(e){
    let index=e.detail.value;
    let technicianId=this.data.technicianQueryList[index].id;
    this.setData({paramArrayIndex:index,'params.technicianId':technicianId});
    this.reListRecord();
  },
  paramArrayChange1:function(e){
    let index=e.detail.value;
   let appointmentDate= this.data.dateQueryList[index].id;
  this.setData({paramArraydIndex:index,'params.appointmentDate':appointmentDate});
  this.reListRecord();
  },
  tabchange:function(e){
    let appointmentStatus=e.currentTarget.dataset.state;
    let tabindex=e.currentTarget.dataset.index;
    this.setData({tabindex:tabindex,'params.appointmentStatus':appointmentStatus});
    this.reListRecord();
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {
    if(this.data.finished){
      return
    }
    var page = this.data.params.pageNum + 1
    this.setData({
      'params.pageNum':page
    })
    this.listRecord();
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})