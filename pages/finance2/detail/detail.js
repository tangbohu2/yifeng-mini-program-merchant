// pages/finance2/detail/detail.js
import * as API_Finance from '../../../api/finance'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    listParams: {
      pageNum: 1,
      pageSize: 20
    },
    list: [],
    total: 0,
    id: '',
    fenyongOne: {},
    productid: '',
    techremark: ''
  },
  getList() {
    const data = {productId: this.data.productid,techRemark:this.data.techremark,...this.data.listParams}
    API_Finance.getDetailList(data).then(res => {
      let arr = this.data.list
      const list = res.data.commissionList.rows
      list.forEach(item => {
        if(!String(item.money).includes('-')) {
          item.money = '+'+item.money
        }
      })
      arr.push(...list)
      let page = this.data.listParams.pageNum

      let fenyongOne = res.data.fenyongOne
      if(fenyongOne) {
        if(!String(fenyongOne.money).includes('-')) {
          fenyongOne.money = '+'+fenyongOne.money
        }
      }
      this.setData({
        list:arr,
        total: res.data.commissionList.total,
        "listParams.pageNum": ++page,
        fenyongOne: fenyongOne
      })
      console.log(this.data.list)
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.setData({
      id: options.id,
      productid: options.productid,
      techremark: options.techremark
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    this.setData({
      list: [],
      fenyongOne: {}
    })
    this.getList()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {
    if(this.data.list.length == this.data.total) {
      return
    } else {
      this.getList()
    }
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})