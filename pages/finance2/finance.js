// pages/finance2/finance.js
import * as API_Finance from '../../api/finance'
import {applyDrawcash} from '../../api/fix'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    listParams: {
      pageNum: 1,
      pageSize: 20
    },
    list: [],
    total: 0,
    financePrice: '',
    dongCashPrice: 0,
    store: {
      storeName: ''
    },
    date: '',
    dateText: '全部'
  },
  getCash() {
    const that = this
    wx.showModal({
      title: '提示',
      content: '确认提现？',
      success (res) {
        if (res.confirm) {
          applyDrawcash().then(res => {
            if(res.code == 200) {
              wx.showToast({
                title: '提现成功',
              })
              that.setData({
                "listParams.pageNum": 1
              })
              that.getList()
            }
          })
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  },
  toCashList() {
    wx.navigateTo({
      url: './record/record',
    })
  },
  getList() {
    let data = {...this.data.listParams}
    if(this.data.dateText != '全部') {
      data = {dateTime: this.data.date,...data}
    }
    API_Finance.getList(data).then(res => {
      let arr = this.data.list
      const list = res.data.list.rows
      list.forEach(item => {
        if(!String(item.money).includes('-')) {
          item.money = '+'+item.money
        }
      })
      arr.push(...list)
      let page = this.data.listParams.pageNum
      this.setData({
        list:arr,
        total: res.data.list.total,
        financePrice: res.data.financePrice,
        dongCashPrice: res.data.dongCashPrice,
        "listParams.pageNum": ++page
      })
    })
  },
  toDetail(e) {
    const id = e.currentTarget.dataset.id
    const productid = e.currentTarget.dataset.productid
    const techremark = e.currentTarget.dataset.techremark
    wx.navigateTo({
      url: '/pages/finance2/detail/detail?id='+id+'&productid='+productid+'&techremark='+techremark,
    })
  },
  getDate() {
    const date = new Date()
    const year = date.getFullYear()
    const month = (date.getMonth() + 1) > 9 ? (date.getMonth() + 1) : '0' + (date.getMonth() + 1)
    const day = date.getDate() > 9 ? date.getDate() : '0' + date.getDate()
    return year + '-' + month + '-' + day
  },
  bindDateChange: function(e) {
    this.setData({
      date: e.detail.value,
      dateText: e.detail.value
    })
    const listParams =  {
      pageNum: 1,
      pageSize: 20
    }
    this.setData({
      list: [],
      listParams,
      total: 0
    })
    this.getList()
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    let store = wx.getStorageSync('store')
    this.setData({store:store})

    // const date = this.getDate()
    // this.setData({
    //   date
    // })

    this.getList()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    // const listParams = {
    //   pageNum: 1,
    //   pageSize: 20
    // }
    // this.setData({
    //   list: [],
    //   listParams,
    //   total: 0
    // })

    // const date = this.getDate()
    // this.setData({
    //   date
    // })

    // this.getList()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {
    if(this.data.list.length == this.data.total) {
      return
    } else {
      this.getList()
    }
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})