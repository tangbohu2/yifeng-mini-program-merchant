// pages/storeUser/storeUser.js
import * as API_StoreUserAccount from '../../api/storeUserAccount'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userList:[]
  },
  /**
   * 添加用户
   */
  addUser:function(){
    wx.navigateTo({
      url: '../storeUserEdit/storeUserEdit',
    })
  },
  /**
   * 编辑用户
   */
  editUser:function(e){
    
    wx.navigateTo({
      url: '../storeUserEdit/storeUserEdit?uuid='+e.currentTarget.dataset.uuid,
    })
  },
 delUser:function(e){
   let uuid=e.currentTarget.dataset.uuid;
   let that=this;
  wx.showModal({
    title:'删除子账号',
    content:'删除子账号后，该账号将不能操作，确定继续？',
    success (res) {
      if (res.confirm) {
        let param={uuid:uuid}
        API_StoreUserAccount.delAcount(param).then(res=>{
          if(res.code==200){
          that.getList();
          }else{
            wx.showToast({
              title:res.msg,
              icon:'none'
          })
          }
        })
      }
    }
  })
  },
  getList:function(){
    let that=this;
    API_StoreUserAccount.getList().then(res=>{
      if(res.code==200){
       that.setData({userList:res.data.rows})
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getList();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})