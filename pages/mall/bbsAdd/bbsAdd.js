// pages/mall/bbsAdd/bbsAdd.js
import * as API_Dict from '../../../api/dict'
import * as API_Mall_BBs from '../../../api/mall/bbs'
import { getAccess } from '../../../api/fix'
import FormData from '../../../utils/formdata'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    media:[],
    bbs:{
      messageClassify:'',
      headline:'',
      mainBody:'',
      videoList:'',
      imageList:'',
     id:''
    },
    messageClassifyList:[],
    commitState:true,
    mediaType:'',
    accessToken: '',
    sourceList: []
  },
  initMedia:function(){
    this.choiceGuide = this.selectComponent("#media")
    var option = {
      count:9
    } 
    this.choiceGuide.init(option)
  },
  uploadMedia(e){
    console.log(e)
    this.choiceGuide = this.selectComponent("#media")
    let detail = e.detail
    let mediaType=detail.type;
    let count=detail.count;
    let maxCount=detail.maxCount;
    if(mediaType=='image'){
      count=9;
      // this.choiceGuide.initImgSource(detail.source[0])
      this.setData({
        sourceList: e.detail.source
      })
    }
    else if(mediaType == 'video') {
      this.setData({
        sourceList: e.detail.media
      })
    }
    else{
      maxCount=1;
    }
    var option = {
      mediaType:mediaType,
      count:count,
      maxCount:maxCount
    } 
    this.choiceGuide.init(option)
    this.setData({
      media:detail.media,
      mediaType:mediaType
    })
  },
  loadDetail(id){
    API_Mall_BBs.getDetail(id).then(res=>{
      let {data} = res
      
      let arrList=[];
      let mediaType;
      let count=1;
      let maxCount;
      // let media=[];
      if(data.imageList.trim()){
        let imageList=JSON.parse(data.imageList)
        mediaType="image"
        count=9;
        arrList=  imageList;
        
      }
      if(data.videoList.trim()){
        let videoList=data.videoList.split(',');
        
        mediaType="video"
        count=1;
        maxCount=1;
        arrList=videoList;
      }
      
      this.choiceGuide = this.selectComponent("#media")
      var option = {
        count:count,
        maxCount:maxCount,
        arrList:arrList,
        mediaType:mediaType
      } 
      this.choiceGuide.init(option)

     let messageClassify=data.messageClassify;
     let headline=data.headline
     let mainBody=data.mainBody
     let id=data.id;
     let messageClassifyList = this.data.messageClassifyList;
     
     messageClassifyList.forEach(item=>{
      if(messageClassify==item.dictValue){
        item.select="Y"
      }
     })
      this.setData({
        messageClassifyList:messageClassifyList,
        // 'bbs.messageClassify':messageClassify,
        'bbs.headline':headline,
        'bbs.mainBody':mainBody,
        'bbs.id':id,
        mediaType:mediaType,
        media:arrList
      })
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if(options.id){
      //修改
     this.setData({'bbs.id':options.id})
    }

    this.loadClassify()
    this.initMedia();
    getAccess().then(res => {
      this.setData({
        accessToken: res.msg
      })
    })
  },
  loadClassify(){
    API_Dict.getDictData('bbs_classify').then(res=>{
      for(let item of res){
        item.select = 'N'
      }
      this.setData({
        messageClassifyList:res
      })
      let bbsid=this.data.bbs.id;
      if(bbsid){
        this.loadDetail(bbsid);
      }
     
    })
  },
  selectClassify(e){
    let that = this
    let item = this.data.messageClassifyList[e.currentTarget.dataset.index]
    for(let classify of this.data.messageClassifyList){
      classify.select = 'N'
    }
    item.select = 'Y'
    this.setData({
      messageClassifyList:that.data.messageClassifyList
    })
  },

  setCommitState(){
    let that = this
    that.setData({
      commitState:!that.data.commitState
    })
  },
  commit(){
    
    let that = this
    that.setCommitState()
    let classify = ''
    for(let item of this.data.messageClassifyList){
      if(item.select == 'Y'){
        classify = item.dictValue
        break
      }
    }
    if(!classify.trim()){
      wx.showToast({
        title: '请选择分类',
        icon:'none',
        duration:1500
      })
    that.setCommitState()
      return
    }
    let mediaArr = this.data.media
    let bbs = this.data.bbs
    bbs.messageClassify=classify;
    if(!bbs.headline.trim()){
      wx.showToast({
        title: '请填写标题',
        icon:'none',
        duration:1500
      })
    that.setCommitState()
      return
    }
    if(!mediaArr.length > 0 && !bbs.mainBody.trim()){
      wx.showToast({
        title: '请填写正文或上传图片/视频',
        icon:'none',
        duration:1500
      })
      that.setCommitState()
      return
    }
    if(that.data.mediaType == 'image'){
      bbs.imageList = JSON.stringify(mediaArr)
    }else{
      bbs.videoList = mediaArr.join(',')
    }

    //先做验证
    //正文
    API_Mall_BBs.publish(bbs).then(res=>{
      console.log(res)
      if(res.code == 200){
        wx.showToast({
          title: '发布成功',
          duration:1500,
          success(res){
            setTimeout(() => {
              var pages = getCurrentPages();
              
              var prevPage = pages[pages.length - 2];  //上一个页面

              let paramsArr = prevPage.data.paramsArr;
              if(paramsArr){
                let param=paramsArr[0];
                param.load = true
                param.finished = false
                param.pageNum = 1
                prevPage.setData({
                  tab_index:0,
                  paramsArr:prevPage.data.paramsArr,
                })//设置数据
                prevPage.onPullDownRefresh()
              }
              wx.navigateBack({
                delta: 0,
              })
            }, 1500);
          }
        })
      }else{
        wx.showToast({
          title: res.msg || '发布失败',
          duration: 1500,
          icon: 'none'
        })
       that.setCommitState()
      }
    })
    // wx.request({
    //   url: 'https://api.weixin.qq.com/wxa/msg_sec_check?access_token='+that.data.accessToken,
    //   method: 'post',
    //   data: {
    //     content: bbs.mainBody
    //   },
    //   success(res) {
    //     console.log(res)
    //     if(res.data.errcode == 87014) {
    //       wx.showToast({
    //         title: '正文存在不合规内容',
    //         icon: 'none'
    //       })
    //       return
    //     }
    //     //标题
    //     wx.request({
    //       url: 'https://api.weixin.qq.com/wxa/msg_sec_check?access_token='+that.data.accessToken,
    //       method: 'post',
    //       data: {
    //         content: bbs.headline
    //       },
    //       success(res) {
    //         console.log(res)
    //         if(res.data.errcode == 87014) {
    //           wx.showToast({
    //             title: '标题存在不合规内容',
    //           })
    //           return
    //         }

    //         API_Mall_BBs.publish(bbs).then(res=>{
    //           console.log(res)
    //           if(res.code == 200){
    //             wx.showToast({
    //               title: '发布成功',
    //               duration:1500,
    //               success(res){
    //                 setTimeout(() => {
    //                   var pages = getCurrentPages();
                      
    //                   var prevPage = pages[pages.length - 2];  //上一个页面
        
    //                   let paramsArr = prevPage.data.paramsArr;
    //                   if(paramsArr){
    //                     let param=paramsArr[0];
    //                     param.load = true
    //                     param.finished = false
    //                     param.pageNum = 1
    //                     prevPage.setData({
    //                       tab_index:0,
    //                       paramsArr:prevPage.data.paramsArr,
    //                     })//设置数据
    //                     prevPage.onPullDownRefresh()
    //                   }
    //                   wx.navigateBack({
    //                     delta: 0,
    //                   })
    //                 }, 1500);
    //               }
    //             })
    //           }else{
    //             wx.showToast({
    //               title: res.msg || '发布失败',
    //               duration: 1500,
    //               icon: 'none'
    //             })
    //            that.setCommitState()
    //           }
    //         })
    //             //图片
    //         // const imgList = JSON.parse(bbs.imageList)
            
    //         // const sourceList = that.data.sourceList
    //         // console.log(sourceList)
    //         // const promiseItem = sourceList.map(item => {
    //         //   const formdata = new FormData()
    //         //   formdata.appendFile('media',item)
    //         //   const data = formdata.getData()
    //         //   // return this.checkSource('https://api.weixin.qq.com/wxa/img_sec_check?access_token='+that.data.accessToken,data)
    //         //   return 123
    //         // })
    //         // console.log(promiseItem)
    //         // console.log(sourceList)
    //         // sourceList.forEach(item => {
    //         //   // console.log(item)
    //         //   const formdata = new FormData()
    //         //   formdata.appendFile('media',item)
    //         //   const data = formdata.getData()
    //         //   wx.request({
    //         //     url: 'https://api.weixin.qq.com/wxa/img_sec_check?access_token='+that.data.accessToken,
    //         //     method: 'post',
    //         //     data: data.buffer,
    //         //     header: {
    //         //       'content-type': data.contentType
    //         //     },
    //         //     success(res) {
    //         //       if(res.data.errcode == 87014) {
    //         //         wx.showToast({
    //         //           title: '图片存在不合规内容',
    //         //         })
    //         //         return
    //         //       }
    //         //       console.log(bbs)
    //         //       API_Mall_BBs.publish(bbs).then(res=>{
    //         //         if(res.code == 200){
    //         //           wx.showToast({
    //         //             title: '发布成功',
    //         //             duration:1500,
    //         //             success(res){
    //         //               setTimeout(() => {
    //         //                 var pages = getCurrentPages();
                            
    //         //                 var prevPage = pages[pages.length - 2];  //上一个页面
              
    //         //                 let paramsArr = prevPage.data.paramsArr;
    //         //                 if(paramsArr){
    //         //                   let param=paramsArr[0];
    //         //                   param.load = true
    //         //                   param.finished = false
    //         //                   param.pageNum = 1
    //         //                   prevPage.setData({
    //         //                     tab_index:0,
    //         //                     paramsArr:prevPage.data.paramsArr,
    //         //                   })//设置数据
    //         //                   prevPage.onPullDownRefresh()
    //         //                 }
    //         //                 wx.navigateBack({
    //         //                   delta: 0,
    //         //                 })
    //         //               }, 1500);
    //         //             }
    //         //           })
    //         //         }else{
    //         //           wx.showToast({
    //         //             title: res.msg || '发布失败',
    //         //             duration: 1500,
    //         //             icon: 'none'
    //         //           })
    //         //          that.setCommitState()
    //         //         }
    //         //       })
    //         //     },
    //         //     fail(err) {
    //         //       console.log(err)
    //         //     }
    //         //   })
    //         // })
    //       }
    //     })
    //   }
    // })
    
  },
  checkSource(url,data) {
    return new Promise((resolve,reject) => {
      wx.request({
        url: url,
        method: 'post',
        data: data.buffer,
        header: {
          'content-type': data.contentType
        },
        success(res) {
          resolve(res)
        },
        fail(err) {
          reject(err)
        }
      })
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  changeTitle(e){
    this.setData({
      'bbs.headline':e.detail.value
    })
  },
  changeText(e){
    this.setData({
      'bbs.mainBody':e.detail.value
    })
  }
})