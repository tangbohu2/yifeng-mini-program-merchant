// pages/mall/bbsMy/bbsMy.js

import * as API_Mall_BBs from '../../../api/mall/bbs'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    classifyList:[{name:'作品',myFlag:'my'},{name:'收藏',myFlag:'collect'},{name:'赞过',myFlag:'like'}],
    tab_index:0,
    opus:[],
    bbsArr:[],
    params:{
      pageNum:1,
      pageSize:10,
      finished:false,
      myFlag:'my'
    }
  },


  
  goDetail(e){
    wx.navigateTo({
      url: '../bbsDetail/bbsDetail?id='+e.currentTarget.dataset.id,
    })
  },
  
  videoloadedmetadata(e){
    let index=e.currentTarget.dataset.index;
    let height=e.detail.height;
    let width=e.detail.width;
    let ob="opus["+index+"].videoheight";
    this.setData({
      [ob]:height/width*46.6106+'vw'
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.switchTabIndex(0);
    this.initGlobalData();
  },
  initGlobalData:function(){
    let globalData=getApp().globalData;
    let statusBarHeightpx=globalData.statusBarHeightpx;
    let navHeadHeightpx=globalData.navHeadHeightpx;
    let navHeightpx=globalData.navHeightpx;
    this.setData({navHeadHeightpx:navHeadHeightpx,navHeightpx:navHeightpx,statusBarHeightpx:statusBarHeightpx});
  },
  loadData(){
    let that = this
    let param = this.data.params;
    API_Mall_BBs.getList(param).then(res=>{
      let arr = that.data.bbsArr;
      for(let item of res.data.rows){
        
        if(item.imageList){
          item.imageList = JSON.parse(item.imageList)
        }
        if(item.videoList){
          item.videoList= item.videoList.split(',');
         }
      }
      
      arr.push(...res.data.rows)
      that.setData({
        bbsArr:arr,
        'params.finished':res.data.lastPage,
      })
    })
  },

  switchTab(e){
    let index = e.currentTarget.dataset.tab_index
    this.switchTabIndex(index);
  },
  switchTabIndex:function(index){
    let param = this.data.params;
    let classifyList=this.data.classifyList;
    let info=classifyList[index];
    param.finished=false;
    param.myFlag=info.myFlag;
    param.pageNum=1;
    this.setData({
      tab_index:index,
      param:param,
      bbsArr:[]
    })
      this.loadData()
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // debugger
    let bbsdelId=wx.getStorageSync('bbsdelId');
    if(bbsdelId){
      let bbsArr=this.data.bbsArr;
    
      let index=0;
      let delIndex=0;
      bbsArr.forEach(item1=>{
        if(item1.id==bbsdelId){
          delIndex=index;
        }
        index++;
      })
      bbsArr.splice(delIndex,1)
     
      this.setData({bbsArr:bbsArr})
      wx.removeStorageSync('bbsdelId')
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {


    let tab_index=this.data.tab_index;
    this.switchTabIndex(tab_index)
    wx.stopPullDownRefresh();

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    let params=this.data.params;
    if(params.finished){
      return;
    }
    params.pageNum = params.pageNum + 1
    this.setData({
      params:params
    })
    this.loadData()
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})