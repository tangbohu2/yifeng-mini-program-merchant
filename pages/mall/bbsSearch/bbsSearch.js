// pages/mall/bbsHome/bbsHome.js
import * as API_Mall_BBs from '../../../api/mall/bbs'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    params:{
      pageNum:1,
      pageSize:10,
      searchValue:''
    },
    finished:false,
    bbsList:[],
    headshow:0,
    searchValue:''
  },
  scroll :function(e){
    let deltaX=e.detail.deltaX;
    let scrollWidth=e.detail.scrollWidth;
    let scrollLeft=e.detail.scrollLeft+80;
    let clientWidth=getApp().globalData.clientWidth;
    let rightX=scrollWidth-clientWidth;
    let opacity = 1
    if(rightX<scrollLeft){
      opacity = (100-(scrollLeft- rightX)*3)/100
    }
    this.setData({headshow:opacity})
  },
  goDetail(e){
    wx.navigateTo({
      url: '../bbsDetail/bbsDetail?id='+e.currentTarget.dataset.id,
    })
  },
  videoloadedmetadata(e){
    let index=e.currentTarget.dataset.index;
    let height=e.detail.height;
    let width=e.detail.width;
  //  opus[index].videoheight=height/width*46.6106+'vw';
    let ob="opus["+index+"].videoheight";
    this.setData({
      [ob]:height/width*46.6106+'vw'
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      'params.searchValue':options.searchValue,
    })
    this.loadData()
  },
  loadData(){
    let that = this
    API_Mall_BBs.getSearch(this.data.params).then(res=>{
      let arr = this.data.bbsList
      let rows=res.data.rows
      rows.forEach(item=>{
        if(item.imageList){
          item.imageList = JSON.parse(item.imageList)
        }
        if(item.videoList){
         item.videoList= item.videoList.split(',');
        }
      })

      arr.push(...rows)
      this.setData({
        bbsList:arr,
        finished:res.data.lastPage
      })
    })
  },
  initGlobalData:function(){
    let globalData=getApp().globalData;
    let statusBarHeightpx=globalData.statusBarHeightpx;
    let navHeadHeightpx=globalData.navHeadHeightpx;
    let navHeightpx=globalData.navHeightpx;
    this.setData({navHeadHeightpx:navHeadHeightpx,navHeightpx:navHeightpx,statusBarHeightpx:statusBarHeightpx});
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    let bbsdelId=wx.getStorageSync('bbsdelId');
    if(bbsdelId){
      let bbsArr=this.data.bbsList;
    
      let index=0;
      let delIndex=0;
      bbsArr.forEach(item1=>{
        if(item1.id==bbsdelId){
          delIndex=index;
        }
        index++;
      })
      bbsArr.splice(delIndex,1)
     
      this.setData({bbsList:bbsArr})
      // wx.removeStorageSync('bbsdelId')
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    if(this.data.finished){
      return
    }
    let page = this.data.params.pageNum+1
    this.setData({
      'params.pageNum':page
    })
    this.loadData()
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})