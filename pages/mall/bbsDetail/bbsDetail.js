// pages/mall/bbsDetail/bbsDetail.js
import * as API_Mall_BBs from '../../../api/mall/bbs'
import * as API_Mall_BBsComment from '../../../api/mall/bbsComment'
import * as API_Mall_BBsOptions from '../../../api/mall/bbsOptions'

Page({

  /**
   * 页面的初始数据
   */
  data: {
    bbsId:'',
    comment:{mainBody:'',parentId:0,bbsId:'',parentLevelId:0},
    bbs:{},
    commentShow:false,//是否显示评价弹窗
    inputPlaceholder:"喜欢就给个评论支持一下",//评价弹窗提示输入语
    commentList:[],
    commentId:0,
    params:{
      bbsId:'',
      pageNum:1,
      pageSize:10,
      parentId:0,
      finished:false
    },
    likeFlag:false,
    likeCount:0,
    collectFlag:false,
    collectCount:0,
    vwNunm:0,
    sending:false,
    showEditBtn:false,
  },
  getMineData(){
    API_Mall_BBsOptions.getMineData(this.data.bbsId).then(res=>{
      this.setData({
        likeFlag:res.data.like,
        collectFlag:res.data.collect
      })
    })
  },
  getOptionsCount(){
    API_Mall_BBsOptions.getOptionsCount(this.data.bbsId).then(res=>{
      this.setData({
        likeCount:res.data.likeCount,
        collectCount:res.data.collectCount
      })
    })
  },
  goBBsEdit(){

    wx.navigateTo({
      url: '../bbsAdd/bbsAdd?id='+this.data.bbs.id,
    })
  },
  del:function(){
    let that = this
    let id=this.data.bbsId;
    wx.showModal({
      title:'删除',
      content:'是否删除该内容?',
      success (res) {
        if (res.confirm) {
          API_Mall_BBs.del(id).then(res=>{
            wx.setStorageSync('bbsdelId',id)
            wx.showToast({
              title: '删除成功',
              icon:'none',
              duration:1500
            })
            setTimeout(() => {
              wx.navigateBack();
            }, 1500);
          
          })
        }
      }
    })
  },
  bbsOption(e){
    let type = e.currentTarget.dataset.type
    let like = {
      foreignId:this.data.bbsId,
      belongType:type
    }
    let that = this
    API_Mall_BBsOptions.like(like).then(res=>{
      if(res.code = 200){
        if(type == 'like'){
          this.setData({
            likeFlag:true,
            likeCount:that.data.likeCount + 1
          })
        }else{
          this.setData({
            collectFlag:true,
            collectCount:that.data.collectCount + 1
          })
        }
      }
    })
  },
  disOption(e){
    let type = e.currentTarget.dataset.type
    let like = {
      foreignId:this.data.bbsId,
      belongType:type
    }
    let that = this
    API_Mall_BBsOptions.del(like).then(res=>{
      if(res.code = 200){
        if(type == 'like'){
          this.setData({
            likeFlag:false,
            likeCount:that.data.likeCount - 1
          })
        }else{
          this.setData({
            collectFlag:false,
            collectCount:that.data.collectCount - 1
          })
        }
      }
    })
  },
  changeComment(e){
    this.setData({
      'comment.mainBody':e.detail.value
    })
  },
  showToComment:function(){
    let id = this.data.bbsId
    this.setData({commentShow:true,'comment.bbsId':id});
  },
  closeToComment:function(){
    this.setData({commentShow:false});
  },
  showSecondComment(e){
    let parentId = e.currentTarget.dataset.parentId
    let secondId = e.currentTarget.dataset.secondId || e.currentTarget.dataset.parentId
    let id = this.data.bbs.id
    this.setData({
      commentShow:true,
      'comment.bbsId':id,
      'comment.parentLevelId':secondId,
      'comment.parentId':parentId,
      commentId:parentId
    });
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const member = wx.getStorageSync('member')
    this.setData({member:member});
    
    this.initGlobalData();
    
    this.loadCommentCount(options.id)
    this.setData({'params.bbsId':options.id,bbsId:options.id})
    this.loadComments()
    this.getOptionsCount()
    this.getMineData()
  },
  initGlobalData:function(){
    let globalData=getApp().globalData;
    let statusBarHeightpx=globalData.statusBarHeightpx;
    let navHeadHeightpx=globalData.navHeadHeightpx;
    let navHeightpx=globalData.navHeightpx;
    this.setData({navHeadHeightpx:navHeadHeightpx,navHeightpx:navHeightpx,statusBarHeightpx:statusBarHeightpx});
  },
  confirmComment(e){
    let that = this
    this.setData({sending:true});
    this.setData({commentShow:!that.data.commentShow});
    let comment = that.data.comment
    if(!comment.mainBody.trim()){
      wx.showToast({
        title: '请输入内容',
        icon:'none',
        duration:1500
      })
    this.setData({commentShow:!that.data.commentShow,sending:false});
      return
    }
    API_Mall_BBsComment.add(comment).then(res=>{
      let arr = that.data.commentList
      if(that.data.commentId >0){
        // 二级及二级以下评论
        let item = that.data.commentList.find(item => item.topId == that.data.commentId);          
        item.replyComments.unshift(res.data)
        that.setData({commentId:0})
      }else{
        arr.unshift(res.data)
      }
      that.setData({
        commentList:arr,
        commentCount:that.data.commentCount + 1
      })
      this.setData({sending:false});
    })
  },
  loadComments(){
    API_Mall_BBsComment.getList(this.data.params).then(res=>{
      let data=res.data;
      let arr = this.data.commentList
      arr.push(...data.rows)
      this.setData({
        commentList:arr
      })
      
        this.setData({
          'params.finished':data.lastPage
        })
     
    })  
  },
  loadMore(e){
    let that = this
    let item = this.data.commentList[e.currentTarget.dataset.index]
    let maxId = item.replyComments[item.replyComments.length-1].id;
    let second = item.replyComments
    API_Mall_BBsComment.loadMore(maxId).then(res=>{
      second.push(...res.data)
      item.secondFinished = res.data.finished
      this.setData({
        commentList:that.data.commentList
      })
    })

  },
  loadCommentCount(e){
    API_Mall_BBsComment.getCount(e).then(res=>{
      this.setData({
        commentCount:res.data
      })
    })
  },
  loadDetail(e){
    let member=this.data.member;
    API_Mall_BBs.getDetail(e).then(res=>{
      let {data} = res
      if(data.publishId==member.id){
        this.setData({showEditBtn:true})
      }
      
      if(data.videoList.trim()){
        data.videoList= data.videoList.split(',');
      }
      if(data.imageList.trim()){
        let imageList=JSON.parse(data.imageList)
        data.imageList = imageList;
        if(imageList.length>0){
          let vwNunm=999.99;
          imageList.forEach(item=>{
            let tempVw=item.height/item.width;
           if(tempVw<vwNunm) {
            vwNunm=tempVw;
           }
          })
          this.setData({
            vwNunm:vwNunm*100
          })
        }
      
        // debugger
      }
      this.setData({
        bbs:data
      })
    })
  },
  videoloadedmetadata(e){
    // let index=e.currentTarget.dataset.index;
    let height=e.detail.height;
    let width=e.detail.width;
  //  opus[index].videoheight=height/width*46.6106+'vw';
    // let ob="opus["+index+"].videoheight";
    this.setData({
      vwNunm:height/width*100
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.loadDetail(this.data.bbsId)
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    if(this.data.params.finished){
      return
    }
    let page = this.data.params.pageNum + 1 
    this.setData({
      'params.pageNum':page
    })
    this.loadComments()
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})