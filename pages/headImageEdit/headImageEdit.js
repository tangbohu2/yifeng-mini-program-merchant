// pages/headImageEdit/headImageEdit.js
import * as API_Fitment from '../../api/fitment'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    fitment:{
      id:null,
      picUrl:'',
      videoUrl:''
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    API_Fitment.getInfo().then(res=>{
      if(res){
        this.doLoad(res)
      }
    })
  },

  doLoad(res){
    this.setData({
      fitment:res
    })
    if(res.picUrl){
      this.choiceGuide = this.selectComponent("#imgs")
      this.choiceGuide.load(res.picUrl.split(","))
    }
    if(res.videoUrl){
      this.choiceGuide1 = this.selectComponent("#video")
       this.choiceGuide1.load(res.videoUrl.split(","))
    }
  },

  //视频上传回调
  uploadVideo(e){
    var videos = e.detail.videos.join(",")
    this.setData({"fitment.videoUrl":videos})
    this.edit()
  },
  edit(){
    let fitment = this.data.fitment
    API_Fitment.edit(fitment).then(res=>{
      this.doLoad(res)
    })
  },
  goBack(){
    wx.navigateBack({
      delta: 0,
    })
  },
    //图片上传回调
    uploadImgs(e){
    var imgs = e.detail.picsList.join(",")
    this.setData({"fitment.picUrl":imgs})
    this.edit()
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})