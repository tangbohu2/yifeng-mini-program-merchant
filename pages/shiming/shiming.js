// pages/shiming/shiming.js
import * as API_Adamember from '../../api/adaMember'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    adaPayMember:{},
     // 提交状态
     showCommit:true,
     hasMember:true,//是否已实名
     showDelete:false,
  },
  changeUserName(e){
    this.setData({'adaPayMember.userName':e.detail.value})
  },
  changephone(e){
    this.setData({'adaPayMember.phone':e.detail.value})
  },
  changeidCard(e){
    this.setData({'adaPayMember.idCard':e.detail.value})
  },
  saveAtt(e){
    let that = this
    this.setData({showCommit:!that.data.showCommit})

    if(!this.data.adaPayMember.userName){
      wx.showToast({title:'请输入姓名',duration:1500,icon:'none'});
      that.setData({showCommit:!that.data.showCommit});
      return;
    }
    if(!this.data.adaPayMember.phone){
      wx.showToast({title:'请输入手机号',duration:1500,icon:'none'});
      that.setData({showCommit:!that.data.showCommit});
      return;
    }
    if(this.data.adaPayMember.phone.length!=11){
      wx.showToast({title:'请输入正确手机号',duration:1500,icon:'none'});
      that.setData({showCommit:!that.data.showCommit});
      return;
    }

    if(!this.data.adaPayMember.idCard){
      wx.showToast({title:'请输入身份证号',duration:1500,icon:'none'});
      that.setData({showCommit:!that.data.showCommit});
      return;
    }
    if(this.data.adaPayMember.idCard.length!=18){
      wx.showToast({title:'请输入正确身份证号',duration:1500,icon:'none'});
      that.setData({showCommit:!that.data.showCommit});
      return;
    }
    API_Adamember.createMember(this.data.adaPayMember).then(res=>{

        if(res.code==200){
          wx.showToast({title:"实名认证成功",duration:1500,icon:'none'});
            setTimeout(()=>{
              wx.navigateBack();
          }, 1500)
        }else{
          wx.showToast({title:res.msg,duration:1500,icon:'none'});
        }
        that.setData({showCommit:!that.data.showCommit});
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let store = wx.getStorageSync('store')
    this.setData({store:store});
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
   this.initMember();
  },
  initMember:function(){
    let that=this;
    API_Adamember.getMember().then(res=>{
        if(res.code==200){

          if(!res.data){
            that.setData({hasMember:false,adaPayMember:{}});
          }else{
            that.setData({adaPayMember:res.data});
            that.initSettleAccount();
          }
        }
    })
  },
  initSettleAccount:function(){
    API_Adamember.getSettleAccount().then(res=>{
        if(res.code==200){
          if(!res.data){
            //没有绑卡信息可以删除
            this.setData({showDelete:true})
          }else{
            this.setData({showDelete:false})
          }
        }
    })
  },
  delMember:function(){
    let that=this;
    API_Adamember.delMember().then(res=>{
      if(res.code==200){
        that.initMember();
      }else{
        wx.showToast({title:res.msg,duration:1500,icon:'none'});
      }
  })
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
