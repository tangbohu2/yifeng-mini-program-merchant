// pages/commentList/commentList.js
import * as API_Comment from '../../api/comment'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    tabindex:0,
    list:[],
    params:{
      pageSize:20,
      pageNum:1,
      type:'all'
    },
    selectComment:{
      storeContent:''
    },
    display:'none',
    finished:false,
    starList:[,,,,,],

  },
  viewImg(e){
    wx.previewImage({
      current:e.currentTarget.dataset.src,
      urls: e.currentTarget.dataset.mysrc,
    })
  },
  showview(e) { 
    this.setData({
      'selectComment.id':e.currentTarget.dataset.id
    })
    this.setData({
      display: "block"
    })
  },
  hideview() {
    this.setData({
      display: "none"
    })
  },
  reComment(e){
    this.setData({
      'selectComment.storeContent':e.detail.value
    })
  },
  commit(){
    let comment = this.data.selectComment
    if(!comment.storeContent.trim()){
      wx.showToast({
        title: '请填写回复内容',
        icon:'none'
      })
      return
    }
    API_Comment.commit(comment).then(res=>{
      if(res){
        wx.showToast({
          title: '回复成功',
        })
        this.setData({
          display: "none",
          'params.pageNum':1,
          list:[]
        })
        this.getList()
      }
    })
  },
  tabchange:function(e){
    this.hideview();
    let tabindex=e.currentTarget.dataset.index;
    this.setData({
      tabindex:tabindex,
      'params.type':e.currentTarget.dataset.type,
      'params.pageNum':1,
      list:[]
    });
    this.getList()
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getList()
  },

  getList(){
    API_Comment.getList(this.data.params).then(res=>{
      console.log(res)
      for(let item of res.data.rows){
        
       let imgs = [];
       if(item.commentImages){
        imgs=item.commentImages.split(',')
       }
        item.imgs = imgs
        let commentVideo= [];
        if( item.commentVideo){
          commentVideo=  item.commentVideo.split(',');
        }
       
        item.commentVideo=commentVideo;
        }
      let arr = this.data.list
      arr.push(...res.data.rows)
      this.setData({
        list:arr,
        finished:res.data.lastPage
      })
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    if(this.data.finished){
      return
    }
    var page = this.data.params.pageNum + 1
    this.setData({
      'params.pageNum':page
    })
    this.getList()
  },
})