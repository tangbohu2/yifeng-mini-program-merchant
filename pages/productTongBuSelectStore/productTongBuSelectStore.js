// pages/productTongBuSelectStore/productTongBuSelectStore.js
import * as API_Stroe from '../../api/store'
import * as API_Product from '../../api/product'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    storeList:[],
    selectIds:[]
  },
  initMyStoreList:function(){
    let that=this;
    API_Stroe.getMyStoreList().then(res=>{
      if(res.code==200){
        let storeId = wx.getStorageSync('storeId');//选中的
        let storeList=res.data;
        let storeListTemp=[];
        storeList.forEach(item => {
          if(item.id!=storeId){
            storeListTemp.push(item);
          }
        });
        that.setData({storeList:storeListTemp});
        // that.setData({storeList:storeList});
      }
    })
  },
  
  tongbuSub(){
    let selectIds=this.data.selectIds;
    // debugger
    if(selectIds.length==0){
      wx.showToast({
        title:'请选择将商品同步到哪些门店',
        icon:'none'
    })
      return ;
    }

    wx.showModal({
      title: '提示',
      content: '将当前门店已上架的团单同步追加到所选门店？',
      success (res) {
        if (res.confirm) {
          let param={storeIds:selectIds};
          API_Product.tongbuSub(param).then(res=>{
            if(res.code==200){
              setTimeout(function(){
                wx.navigateBack({
                  delta: 0,
                })
              },1500)
            }
              wx.showToast({
                title:res.msg,
                icon:'none'
            })
            
          })
        } else if (res.cancel) {
         
        }
      }
    })

   

  },
  selectNode(e){
    let item = this.data.storeList[e.currentTarget.dataset.index]
    if(item.checked){
      for(var i = 0; i< this.data.selectIds.length; i++){
        if(this.data.selectIds[i] == item.id){
          this.data.selectIds.splice(i,1)
          item.checked = false
        }
      }
    }else{
      this.data.selectIds.push(e.currentTarget.dataset.id)
      item.checked = true
    }
    console.info(this.data.selectIds)
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
this.initMyStoreList();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})