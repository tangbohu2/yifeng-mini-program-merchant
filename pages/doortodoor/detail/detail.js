// pages/doortodoor/detail/detail.js
import { getServeDetail } from '../../../api/fix'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    detail: {},
    imglist: [],
    id: ''
  },
  toPreView(e) {
    const index = e.currentTarget.dataset.index
    const url = this.data.imglist[index]
    wx.previewImage({
      current: url, // 当前显示图片的http链接
      urls: this.data.imglist // 需要预览的图片http链接列表
    })
  },
  assign() {
    wx.navigateTo({
      url: '/pages/doortodoor/stafflist/stafflist?order='+this.data.detail.id,
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    const id = options.id
    this.setData({
      id
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    getServeDetail(this.data.id).then(res => {
      this.setData({
        detail: res.data.data,
        imglist: res.data.data.storeDoorserverProduct.imageList.split(',')
      })
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})