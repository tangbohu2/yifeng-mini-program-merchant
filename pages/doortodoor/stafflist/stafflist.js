// pages/doortodoor/stafflist/stafflist.js
import { getStaffList,assignStaff } from '../../../api/fix'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: [],
    current: -1,
    order: ''
  },
  radioChange({detail}) {
    this.setData({
      current: detail.value
    })
  },
  submit() {
    if(this.data.current == -1) {
      wx.showToast({
        title: '请先选择员工',
        icon: 'error'
      })
      return
    }
    const data = {
      orderId: this.data.order,
      storeUserId: this.data.current
    }
    assignStaff(data).then(res => {
      if(res.code == 200) {
        wx.showToast({
          title: '指派成功',
        })
        setTimeout(() => {
          wx.navigateBack({
            delta: 1
          })
        },500)
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    const order = options.order
    this.setData({
      order
    })
    getStaffList().then(res => {
      this.setData({
        list: res.data.data
      })
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})