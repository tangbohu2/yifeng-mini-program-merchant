// pages/doortodoor/doortodoor.js
import { getServiceForStore } from '../../api/fix'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    params: {
      pageNum: 1,
      pageSize: 10,
      orderState: ""
    },
    list: [],
    isFished: false,
    index: 0
  },
  toAssign(e) {
    console.log(e)
    const id = e.currentTarget.dataset.id
    wx.navigateTo({
      url: '/pages/doortodoor/stafflist/stafflist?order='+id,
    })
  },
  getList() {
    const that = this
    getServiceForStore(this.data.params).then(res => {
      that.setData({
        list: that.data.list.concat(res.data.rows),
        "params.pageNum": ++that.data.params.pageNum,
        isFished: res.data.lastPage
      })
      console.log(this.data.list)
    })
  },
  switchTab(e) {
    const index = e.currentTarget.dataset.index
    const params = this.data.params
    if(index == 0) {
      params.orderState = ""
    } else if(index == 1) {
      params.orderState = "wait_use"
    } else if(index == 2) {
      params.orderState = "used"
    } else if(index == 3) {
      params.orderState = "wait_assgin"
    }
    // else if(index == 1) {
    //   params.orderState = "wait_pay"
    // } else if(index == 2) {
    //   params.orderState = "wait_use"
    // } else {
    //   params.orderState = "used"
    // }
    this.setData({
      index,
      params,
      "params.pageNum": 1,
      isFinished: false,
      list: []
    })
    console.log(this.data.index)
    this.getList()
  },
  lookmore(e) {
    wx.navigateTo({
      url: '/pages/doortodoor/detail/detail?id='+e.currentTarget.dataset.id,
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    this.setData({
      "params.pageNum": 1,
      isFinished: false,
      list: []
    })
    this.getList()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {
    if(this.data.isFished) {
      return
    } else {
      this.getList()
    }
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})