// pages/fenXiaoCustomer/fenXiaoCustomer.js
import * as API_With_Draw from '../../api/withDraw'
import { customInfo } from '../../api/fix'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    customerList:[],
    params:{
      pageNum:1,
      pageSize:20
    },
    finished:false,
    count:0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let globalData=getApp().globalData;
    let navHeadHeightpx=globalData.navHeadHeightpx;
    this.setData({navHeadHeightpx:navHeadHeightpx})
    this.loadData()
  },

  loadData(){
    // API_With_Draw.getCustomerList(this.data.params).then(res=>{
    //   console.log(res)
    //   let arr = this.data.customerList
    //   arr.push(...res.data.rows)
    //   this.setData({
    //     customerList:arr,
    //     finished:res.data.lastPage,
    //     count:res.data.total
    //   })
    // })
    customInfo(this.data.params).then(res=>{
      console.log(res)
      let arr = this.data.customerList
      arr.push(...res.data.rows)
      this.setData({
        customerList:arr,
        finished:res.data.lastPage,
        count:res.data.total
      })
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    if(!this.data.finished){
      return
    }
    let that = this
    let page = that.data.params.pageNum + 1
    that.setData({
      'params.pageNum':page
    })
    that.loadData()
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})