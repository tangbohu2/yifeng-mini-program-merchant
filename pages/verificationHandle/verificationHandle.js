// pages/verificationHandle/verificationHandle.js
import * as API_Order from '../../api/order'

Page({

  /**
   * 页面的初始数据
   */
  data: {
    code:'',
    commit:true,
    method: '',
    text: '核销'
  },

  codeInput(e){
    this.setData({
      code:e.detail.value
    })
  },
  verifyCode(e){
    const that = this
    if(!this.data.code.trim()){
      wx.showToast({
        title: '请输入券码',
        icon:'none'
      })
      return
    }
    this.setData({
      commit:false
    })
    var verifyCode = this.data.code
    let params = {
     verifyCode:verifyCode
    }
    const method = this.data.method
    API_Order[method](params).then(res=>{
      if(res.code == 200){
        this.setData({
          commit:true,
          code:''
        })
        wx.showToast({
          title: '核销成功',
        })
        setTimeout(()=>{
          // let loginType = wx.getStorageSync('loginType')
          // let type = loginType === 1 ? 2 : 1
          wx.navigateTo({
            url: '/pages/verificationRecord/verificationRecord?type=' + that.data.type,
          })
        }, 1500)
      }else{
        this.setData({
          commit:true,
        })
        wx.showToast({
          title: res.msg || '验证失败',
          icon:'none'
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function ({index}) {
    //index 0核销1售后
    //type 0员工端1商家端
    const flag = index
    if(!flag || flag == 0) {
      this.setData({
        method: 'scanVerify',
        text: '核销'
      })
    } else {
      //storeScanAfter
        this.setData({
          method: 'scanAfter',
          text: '售后'
        })
    }
    this.setData({
      type: index == 0 ? 1 : 2
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})