// pages/staffdoortodoor/staffdoortodoor.js
import { getStaffOrderList } from '../../api/fix'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    params: {
      pageNum: 1,
      pageSize: 10,
      orderState: ''
    },
    list: [],
    isFinished: false,
    index: 0
  },
  switchTab(e) {
    const index = e.currentTarget.dataset.index
    const params = this.data.params
    if(index == 0) {
      params.orderState = ""
    } else if(index == 1) {
      params.orderState = "wait_use"
    } else if(index == 2) {
      params.orderState = "used"
    }
    // else if(index == 1) {
    //   params.orderStatue = "wait_pay"
    // } else if(index == 2) {
    //   params.orderStatue = "wait_use"
    // } else {
    //   params.orderStatue = "used"
    // }
    this.setData({
      index,
      params,
      "params.pageNum": 1,
      isFinished: false,
      list: []
    })
    this.getList()
  },
  lookmore(e) {
    const id = e.currentTarget.dataset.id
    wx.navigateTo({
      url: '/pages/staffdoortodoor/detail/detail?id='+id,
    })
  },
  getList() {
    const that = this
    getStaffOrderList(this.data.params).then(res => {
      that.setData({
        list: that.data.list.concat(res.data.rows),
        "params.pageNum": ++that.data.params.pageNum,
        isFinished: res.data.lastPage
      })
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    const status = options.status
    if(status) {
      const params = this.data.params
      params.orderState = status
      let index = 0
      if(status == 'wait_use') {
        index = 1
      } else if(status == 'used') {
        index = 2
      }
      this.setData({
        params,
        index
      })
    }
    this.getList()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {
    if(this.data.isFinished) {
      return
    } else {
      this.getList()
    }
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})