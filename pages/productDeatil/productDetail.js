// pages/productDeatil/productDetail.js
import { getStoreDetail } from '../../api/fix'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    detail: {}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad({id,storeId}) {
    const data = {
      id,
      storeId:storeId
    }
    getStoreDetail(data).then(res => {
      this.choiceGuige = this.selectComponent("#richeditor")
      this.choiceGuige.init(res.data.infoId)
      const detail = res.data
      detail.itemDetail = JSON.parse(detail.itemDetail)
      console.log(detail.itemDetail)
      detail.peopleRight = JSON.parse(detail.peopleRight)
      this.setData({
        detail
      })
      console.log(this.data.detail)
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})