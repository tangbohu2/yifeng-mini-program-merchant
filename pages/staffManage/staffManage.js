// pages/staffManage/staffManage.js
import { getList,deleteStaff } from '../../api/fix'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: [],
  },
  lookdetal(e) {
    const id = e.currentTarget.dataset.id
    wx.navigateTo({
      url: '/pages/staffDetail/staffDetail?id='+id,
    })
  },
  addOrEditStaff(e) {
    console.log(e)
    const id = e.currentTarget.dataset.id
    if(id) {
      wx.navigateTo({
        url: '/pages/staffplus/staffplus?id='+id,
      })
    } else {
      wx.navigateTo({
        url: '/pages/staffplus/staffplus',
      })
    }
  },
  getList() {
    getList().then(res => {
      this.setData({
        list: res.rows
      })
    })
  },
  delStaff(e) {
    const that = this
    wx.showModal({
      title: '提示',
      content: '确定删除？',
      success(res) {
        if (res.confirm) {
          const id = e.currentTarget.dataset.id
          deleteStaff(id).then(res => {
            const index = that.data.list.findIndex(item => {
              return item.id == id
            })
            let list = that.data.list
            list.splice(index,1)
            that.setData({
              list
            })
            wx.showToast({
              title: '删除成功',
            })
          })
        }else if (res.cancel) {
          wx.showToast({
            title: '取消删除',
            icon: 'none'
          })
        }

      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.getList()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    this.setData({
      list: []
    })
    this.getList()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})