// pages/balanceRecord/balanceRecord.js
const util = require('../../utils/util')
import * as API_Finance from '../../api/finance'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    dateend:util.getDate(),
   params:{
     recodeDate:util.getDateNY(),
     pageSize:20,
     pageNum:1,
    } ,
    finished:false,
    list:[]
  },
  bindDateChange: function(e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      'params.recodeDate': e.detail.value,
      list:[],
      'params.pageNum':1
    })
    this.listBalanceRecord()
  },
  listBalanceRecord:function(){

  API_Finance.listBalanceRecord(this.data.params).then(res=>{
    let arr = this.data.list
    arr.push(...res.rows)
    this.setData({
      list:arr,
      finished:res.lastPage
    })
  })
  
},
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let globalData=getApp().globalData;
    let windowHeight=globalData.windowHeight;
    let navHeadHeightpx=globalData.navHeadHeightpx;
    this.setData({navHeadHeightpx:navHeadHeightpx,windowHeight:windowHeight});
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
this.listBalanceRecord();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */


  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  onReachBottom: function () {
    if(this.data.finished){
      return
    }
    var page = this.data.params.pageNum + 1
    this.setData({
      'params.pageNum':page
    })
    this.listBalanceRecord()
  },

})