// pages/productList/productList.js
import * as API_Product from '../../api/product'
import * as API_Stroe from '../../api/store'
import { upnew,downnew } from '../../api/fix'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    tabindex:'all',
    productList:[],
    //请求参数
    params: {
      pageNum: 1,
      pageSize: 20,
      // 所选分类id
      state:'all'
    },
    finished:false,
    storeList:[]
  },
  //进入商品详情页
  toDetail(e) {
    const id = e.currentTarget.dataset.id
    const storeId = e.currentTarget.dataset.storeid
    // if(storeId == 0) {
    //   wx.showToast({
    //     title: '项目未上架过',
    //     icon: 'error'
    //   })
    // } else {
    //   wx.navigateTo({
    //     url: '/pages/productDeatil/productDetail?id='+id+'&storeId='+storeId,
    //   })
    // }
    wx.navigateTo({
      url: '/pages/productDeatil/productDetail?id='+id+'&storeId='+storeId,
    })
  },
  //上架项目
  doUpNew(e) {
    const product = e.currentTarget.dataset.product
    const id = e.currentTarget.dataset.id
    const productList = this.data.productList
    const index = productList.findIndex(item => {
      return item.id == id
    })
    upnew(id).then(res => {
      wx.showToast({
        title: '上架成功',
        icon: 'none'
      })
      //主动改变状态，稳妥点还是接口获取状态
      productList[index].state = 'Y'
      productList[index].storeProductJointId = res.storeProductJointId
      this.setData({
        productList
      })
    })
  },
  //下架项目
  doDownNew(e) {
    const id = e.currentTarget.dataset.id
    const productList = this.data.productList
    const index = productList.findIndex(item => {
      return item.id == id
    })
    downnew(id).then(res => {
      wx.showToast({
        title: '下架成功',
        icon: 'none'
      })
      //主动改变状态，稳妥点还是接口获取状态
      productList[index].state = 'N'
      this.setData({
        productList
      })
    })
  },
  toSelectStore(){
    wx.navigateTo({
      url: '/pages/productTongBuSelectStore/productTongBuSelectStore',
    })
   },
  tabchange:function(e){
    let state=e.currentTarget.dataset.state;
    this.setData({
      tabindex:state,
      'params.state':state
    });
    this.flush()
    
  },
  //新增团单
  toProdectEdit(){
    wx.navigateTo({
      url: '../productEdit/productEdit?type=add',
    })
  },
  productset(e) {
    const id = e.currentTarget.dataset.id
    const product = e.currentTarget.dataset.product
    const storeid = e.currentTarget.dataset.storeid
    if(!id) {
      return
    }
    wx.navigateTo({
      url: '/pages/productset/productset?id='+id+'&productid='+product+'&storeid='+storeid,
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let that = this
    that.getList(),
    that.initMyStoreList();
  },
  initMyStoreList:function(){
    let that=this;
    API_Stroe.getMyStoreList().then(res=>{
      if(res.code==200){
        that.setData({storeList:res.data});
        // that.checCanSee();
      }
    })
  },
  flush(){
    this.setData({
      'params.pageNum':1,
      productList:[]
    })
    this.getList()
  },
  shelvesUp(e){
    API_Product.shelvesUp(e.currentTarget.dataset.id).then(res=>{
      if(res.success){
        wx.showToast({
          title: '上架成功',
        })
        this.flush()
      }else{
        wx.showToast({
          title: res.msg,
          icon:'none'
        })
      }
    })
  },
  shelvesDown(e){
    API_Product.shelvesDown(e.currentTarget.dataset.id).then(res=>{
      if(res.success){
        wx.showToast({
          title: '下架成功',
        })
        this.flush()
      }else{
        wx.showToast({
          title: res.msg,
          icon:'none'
        })
      }
    })
  },
  goAddProduct() {
    wx.navigateTo({
      url: '../productEdit/productEdit?type=1',
    })
  },
  goEdit(e){
    wx.navigateTo({
      url: '../productEdit/productEdit?id='+e.currentTarget.dataset.id+'&type=2',
    })
  },
  toCopy(e){
    wx.navigateTo({
      url: '../productEdit/productEdit?copy=copy&id='+e.currentTarget.dataset.id,
    })
  },
  getList(){
    let that = this
    API_Product.getList(that.data.params).then(res =>{
      if(res.code == 200){
        let list = that.data.productList
        const rows = res.rows
        rows.forEach(item => {
          item.peopleRight = JSON.parse(item.peopleRight)
        })
        list.push(...rows)
        that.setData({
          finished:res.lastPage,
          productList:list
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    let that = this
    if(that.data.finished){
      return
    }
    var pageNum = that.data.params.pageNum + 1
    that.setData({
      'params.pageNum':pageNum
    })
    that.getList()
  },

   
})