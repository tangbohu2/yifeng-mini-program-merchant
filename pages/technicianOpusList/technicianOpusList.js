// pages/technicianOpusList/technicianOpusList.js
import * as API_Opus from '../../api/technicianOpus'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    tabindex:0,
    opusList:[],
    techId:null,
    type:'all',
    jishiedit:''
  },
  tabchange:function(e){
    let tabindex=e.currentTarget.dataset.index;
    this.setData({
      tabindex:tabindex,
      type:e.currentTarget.dataset.type,
      opusList:[]
    });
    this.getList()
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({techId:options.techId,jishiedit:options.jishiedit})
    this.getList()
  },
  getList(){
    if(this.data.jishiedit=='Y'){
      API_Opus.getTechnicianOpusList(this.data.type).then(res=>{
        this.setData({
            opusList:res
        })
      })
    }else{
      API_Opus.getList(this.data.techId,this.data.type).then(res=>{
        this.setData({
            opusList:res
        })
      })
    }
   
  },
  /**新增 */
  add(){
    let id = this.data.techId
   let paramStr="";
  if(id){
    const type = this.data.tabindex
    paramStr='?techId='+id+'&type='+type;
  }
    wx.navigateTo({
      url: '../technicianOpusEdit/technicianOpusEdit'+paramStr,
    })
  },
  edit(e){
    wx.navigateTo({
      url: '../technicianOpusEdit/technicianOpusEdit?id='+e.currentTarget.dataset.id,
    })
  },
  del(e){
    let that = this
    wx.showModal({
      title:'是否删除该作品？',
      success(res){
        if(res.confirm){
          API_Opus.del(e.currentTarget.dataset.id).then(res=>{
            that.getList()
          })
        }
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

   
})