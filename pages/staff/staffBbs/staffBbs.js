// pages/staff/staffBbs/staffBbs.js
function param(pageNum,pageSize,classify,load,finished,minId,searchValue){
  this.pageNum = pageNum
  this.pageSize = pageSize
  this.classify = classify
  this.load = load
  this.finished = finished
  this.minId = minId
  this.searchValue = searchValue
}
import * as API_Dict from '../../../api/dict'
import * as API_Mall_BBs from '../../../api/mall/bbs'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    tab_index:0,
    paramsArr:[{}],
    headshow:0,
    opus:[],
    bbsArr:[],
    classifyList:[],
    minId:[],
    searchValue:''
  },
  searchInput(e){
    this.setData({
      searchValue:e.detail.value
    })
  },
  search(e){
    let val = this.data.searchValue
    console.log(val)
    if(val.trim()){
      wx.navigateTo({
        url: '../bbsSearch/bbsSearch?searchValue='+val,
      })  
    }
    
  },
  scroll :function(e){
    let deltaX=e.detail.deltaX;
    let scrollWidth=e.detail.scrollWidth;
    let scrollLeft=e.detail.scrollLeft+80;
    let clientWidth=getApp().globalData.clientWidth;
    let rightX=scrollWidth-clientWidth;
    let opacity = 1
    if(rightX<scrollLeft){
      opacity = (100-(scrollLeft- rightX)*3)/100
    }
    this.setData({headshow:opacity})
  },
  goDetail(e){
    wx.navigateTo({
      url: '/pages/mall/bbsDetail/bbsDetail?id='+e.currentTarget.dataset.id,
    })
  },
  goBBsAdd(){
    wx.navigateTo({
      url: '/pages/mall/bbsAdd/bbsAdd',
    })
  },
  toBBsMy:function(){
    wx.navigateTo({
      url: '/pages/mall/bbsMy/bbsMy',
    })
  },
  videoloadedmetadata(e){
    let index=e.currentTarget.dataset.index;
    let height=e.detail.height;
    let width=e.detail.width;
  //  opus[index].videoheight=height/width*46.6106+'vw';
    let ob="opus["+index+"].videoheight";
    this.setData({
      [ob]:height/width*46.6106+'vw'
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.initGlobalData();
    this.loadClassify()

  },
  loadClassify(){
    API_Dict.getDictData('bbs_classify').then(res=>{
      let arr = []
      let itemArr = []
      let classifyArr = []
      let minIdArr = []
      arr.push(new param(
        1,
        20,
        '',
        true, 
        false,
        0
      ))
      classifyArr[0] = {dictValue:'',dictLabel:'全部',select:'N'}
      itemArr.push(new Array())
      minIdArr.push(0)
      for(let item of res){
        item.select = 'N'
        arr.push(new param(
          1,
          20,
          item.dictValue,
          true, 
          false,
          0
        ))
      itemArr.push(new Array())
      minIdArr.push(0)
      classifyArr.push(item)
      }
      this.setData({
        classifyList:classifyArr,
        paramsArr:arr,
        bbsArr:itemArr,
        minId:minIdArr
      })
      this.loadData()
    })
  },
  loadData(){
    let that = this
    let param = that.data.paramsArr[that.data.tab_index]
    let minId = that.data.minId[that.data.tab_index]
    API_Mall_BBs.getList(param).then(res=>{
      let arr = that.data.bbsArr[that.data.tab_index]
      for(let item of res.data.rows){
        if(item.imageList){
          item.imageList = JSON.parse(item.imageList)
        }
        if(item.videoList){
         item.videoList= item.videoList.split(',');
        }
      }
      if(res.data.rows.length>0){
        minId = res.data.rows[res.data.rows.length - 1].id
      }
      arr.push(...res.data.rows)
      let param = that.data.paramsArr[that.data.tab_index]
      param.load = false
      param.finished = res.data.lastPage
      that.setData({
        bbsArr:that.data.bbsArr,
        paramsArr:that.data.paramsArr,
        minId:that.data.minId
      })
    })
  },
  initGlobalData:function(){
    let globalData=getApp().globalData;
    let statusBarHeightpx=globalData.statusBarHeightpx;
    let navHeadHeightpx=globalData.navHeadHeightpx;
    let navHeightpx=globalData.navHeightpx;
    this.setData({navHeadHeightpx:navHeadHeightpx,navHeightpx:navHeightpx,statusBarHeightpx:statusBarHeightpx});
  },
  switchTab(e){
    let index = e.currentTarget.dataset.tab_index
  
    let param = this.data.paramsArr[index]
    this.setData({
      tab_index:index
    })
    if(param.load){
      this.loadData()
    }
    
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    let bbsdelId=wx.getStorageSync('bbsdelId');
    if(bbsdelId){
      let bbsArr=this.data.bbsArr;
      bbsArr.forEach(item=>{
        let index=0;
        let delIndex=0;
        item.forEach(item1=>{
          if(item1.id==bbsdelId){
            delIndex=index;
          }
          index++;
        })
        item.splice(delIndex,1)
      })
      this.setData({bbsArr:bbsArr})
      wx.removeStorageSync('bbsdelId')
    }
   
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    let that = this
    that.data.bbsArr[that.data.tab_index] = []
    let param = that.data.paramsArr[that.data.tab_index]
    param.load = true
    param.finished = false
    param.pageNum = 1
    param.minId = 0
    this.setData({
      bbsArr:that.data.bbsArr,
      paramsArr :that.data.paramsArr
    })
    this.loadData()
    wx.stopPullDownRefresh();

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    let that = this
    let param = that.data.paramsArr[that.data.tab_index]
    if(param.finished){
      return
    }
    param.pageNum = param.pageNum + 1
    that.setData({
      paramsArr:that.data.paramsArr
    })
    this.loadData()
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})