// pages/bankCard/bankCard.js
import * as API_Adamember from '../../../api/adaMember'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    adapaySettleaccount: {},
    // 提交状态
    showCommit: true,
    hasMember: false, //是否已实名
    isRealname: false, //是否已实名
    isDebitCard: false, //是否已提交结算卡
    type: -1, // 0：实名信息，1：结算卡信息
    sendData: {}
  },
  changephone(e) {
    this.setData({
      'adapaySettleaccount.phone': e.detail.value
    })
  },
  changebankCard(e) {
    this.setData({
      'adapaySettleaccount.bankCard': e.detail.value
    })
  },
  delete() {
    let title = this.data.type === 0 ? '实名信息' : '结算卡'
    wx.showModal({
      title: '提示',
      content: `确定删除${title}？`,
      success: (res) => {
        if (res.confirm) {
          if (this.data.type === 0) {
            API_Adamember.delClerkMember().then(() => {
              setTimeout(() => {
                wx.navigateBack()
              }, 1000)
            })
          }
          if (this.data.type === 1) {
            API_Adamember.delClerkSettleAccount().then(() => {
              this.setData({
                isDebitCard: false,
                'sendData.phone': '',
                'sendData.bankCard': ''
              })
            })
          }
        }
      }
    })
  },
  saveAtt(e) {
    let that = this
    this.setData({
      showCommit: !that.data.showCommit
    })
    if (!this.data.adapaySettleaccount.bankCard) {
      wx.showToast({
        title: '请输入银行卡号',
        duration: 1500,
        icon: 'none'
      });
      that.setData({
        showCommit: !that.data.showCommit
      });
      return;
    }
    if (!this.data.adapaySettleaccount.phone) {
      wx.showToast({
        title: '请输入银行卡预留手机号',
        duration: 1500,
        icon: 'none'
      });
      that.setData({
        showCommit: !that.data.showCommit
      });
      return;
    }
    if (this.data.adapaySettleaccount.phone.length != 11) {
      wx.showToast({
        title: '请输入正确银行卡预留手机号',
        duration: 1500,
        icon: 'none'
      });
      that.setData({
        showCommit: !that.data.showCommit
      });
      return;
    }
    API_Adamember.createSettleAccount(this.data.adapaySettleaccount).then(res => {
      if (res.code == 200) {
        wx.showToast({
          title: "结算卡绑定成功",
          duration: 1500,
          icon: 'none'
        });
        setTimeout(() => {
          wx.navigateBack();
        }, 1500)
      } else {
        wx.showToast({
          title: res.msg,
          duration: 1500,
          icon: 'none'
        });
      }
      that.setData({
        showCommit: !that.data.showCommit
      });
    })
  },
  isValidPhoneNumber(phoneNumber) {
    return /^1\d{10}$/.test(phoneNumber);
},
  submit () {
      if(this.data.type == 0) {
        if(!this.data.sendData.userName) {
            wx.showToast({
              title: '请输入姓名',
              icon: 'none'
            })
            return
        }
        if(!this.data.sendData.idCard) {
          wx.showToast({
              title: '请输入身份证号',
              icon: 'none'
            })
            return
        }
        if(!this.data.sendData.phone) {
          wx.showToast({
              title: '请输入手机号',
              icon: 'none'
            })
            return
        }
        if(!this.isValidPhoneNumber(this.data.sendData.phone)) {
            wx.showToast({
              title: '请输入正确的手机号',
              icon: 'none'
            })
            return
        }
      }
    if (this.data.type === 0) {
      API_Adamember.createClerkMember(this.data.sendData).then(res => {
        this.getInfo()
      })
    }
    if (this.data.type === 1) {
      if(!this.data.isRealname) {
        wx.showModal({
            title: '提示',
            content: '前往完善用户信息',
            success (res) {
                if (res.confirm) {
                    console.log('用户点击确定')
                    wx.navigateTo({
                        url: '/pages/staff/bankCard/bankCard?type=0',
                    })
                } else if (res.cancel) {
                    console.log('用户点击取消')
                }
            }
        })
        return
      }
      const data = JSON.parse(JSON.stringify(this.data.sendData))
      delete data.userName
      delete data.idCard
      API_Adamember.createClerkSettleAccount(data).then(res => {
        this.getInfo()
      })
    }
  },
  input (e) {
    let val = e.detail.value
    let type = e.currentTarget.dataset.type
    this.data.sendData[type] = val
  },
  getClerkMember () {
    API_Adamember.getClerkSettleAccount().then(({ code, data, msg }) => {
      this.setData({
        isDebitCard: true,
        'sendData.userName': data.userName,
        'sendData.phone': data.phone,
        'sendData.idCard': data.idCard,
        'sendData.bankCard': data.bankCard
      })
    })
  },
  toWriteRealName() {
    if(!this.data.isRealname && this.data.type != 0) {
        wx.showModal({
            title: '提示',
            content: '前往完善用户信息',
            success (res) {
                if (res.confirm) {
                    console.log('用户点击确定')
                    wx.navigateTo({
                    url: '/pages/staff/bankCard/bankCard?type=0',
                    })
                } else if (res.cancel) {
                    console.log('用户点击取消')
                }
            }
        })
    }
  },
  getInfo() {
    API_Adamember.getClerkMember()
        .then(({ code, data, msg }) => {
            // console.log(123)
            this.setData({
                isRealname: true,
                'sendData.userName': data.userName,
                'sendData.phone': data.phone,
                'sendData.idCard': data.idCard
            })
            if (this.data.type === 1) {
                this.getClerkMember()
            }
        })
        .catch(({code,msg}) => {
            if(code == 500) {

            }
        })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      type: parseInt(options.type)
    })
    let store = wx.getStorageSync('store')
    this.setData({
      store: store
    })
    this.getInfo()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getInfo()
    // let that = this;
    // API_Adamember.getMember().then(res => {
    //   if (res.code == 200) {
    //     that.setData({
    //       adaPayMember: res.data
    //     })
    //     if (!res.data) {
    //       wx.showToast({
    //         title: "请先实名后再绑卡",
    //         duration: 1500,
    //         icon: 'none'
    //       });
    //       setTimeout(() => {
    //         wx.navigateBack();
    //       }, 1500)
    //     }
    //   }
    // })
    // API_Adamember.getSettleAccount().then(res => {
    //   if (res.code == 200) {
    //     if (!res.data) {
    //       that.setData({
    //         hasMember: false
    //       });
    //     } else {
    //       that.setData({
    //         adapaySettleaccount: res.data
    //       });
    //     }
    //   }
    // })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})