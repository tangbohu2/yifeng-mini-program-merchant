// pages/staff/commission/record/record.js
import { listClerkDrawcashRecordWithStuff } from '../../../../api/adaMember'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    pageNum: 1,
    pageSize: 20,
    total: 20,
    list: []
  },
  getList() {
    const data = {
      pageNum: this.data.pageNum,
      pageSize: this.data.pageSize
    }
    listClerkDrawcashRecordWithStuff(data).then(res => {
      const list = this.data.list
      this.setData({
        total: res.total,
        list: list.concat(res.rows),
        pageNum: ++this.data.pageNum
      })
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    this.setData({
      list: [],
      pageNum: 1,
      pageSize: 20,
      total: 20,
    })
    this.getList()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {
    if(this.data.list.length >= this.data.total) {
      return
    } else {
      this.getList()
    }
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})