// pages/staff/commission/commission.js
import { projectCommissionInfo, distributionCommissionInfo, customInfo,distributionCommissionInfoWithStuff } from '../../../api/fix'
import * as API_Adamember from '../../../api/adaMember'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    type: 0,
    sendData: {
      pageNum: 1,
      pageSize: 10
    },
    pages:0,
    list: [],
    commissionInfo: {
      commission: 0, // 项目佣金
      proPrice: 0, // 自身收入
      bletPrice: 0, // 老带新收入
      pushPrice: 0 // 推广收入
    },
    dongCashPrice: "",
    pageTitle: {
      0: '项目佣金',
      1: '分销佣金',
      2: '我的客户'
    }
  },
  toCashList() {
    wx.navigateTo({
      url: './record/record',
    })
  },
  withdrawal () {
    API_Adamember.applyStaffDrawcash().then(res => {
      this.setData({
        list: []
      }, () => {
        this.setData({
          "sendData.pageNum": 1
        })
        // this.data.sendData.pageNum = 1
        this.getList()
      })
    })
  },
  handerList (data) {
    let list = []
    let old_time = ''
    let list_len = -1
    let num = 0
    data.map(item => {
      if (this.data.type === 2) {
        item.name = '分享小程序获得新人'
        item.money = 1
      } else {
        if (item.busType === 'pro' && item.name === '售后手工费'){
          item.name = `售后订单（订单号${item.techRemark.slice(5, item.techRemark.length)}）`
        } else if (item.busType === 'pro' && item.name !== '售后手工费'){
          item.name = `核销订单（订单号${item.techRemark.slice(5, item.techRemark.length)}）`
        }
      }
      let new_time = item.createTime.slice(0, 10)
      if (!old_time || old_time !== new_time) {
        if (list_len > -1) {
          list[list_len].num = num
        }
        old_time = new_time
        list_len = list_len + 1
        num = 0
        num = num + item.money
        list.push({
          title: old_time,
          children: [
            {
              name: item.name,
              money: item.money,
              createTime: new_time
            }
          ]
        })
      } else {
        num = num + item.money
        list[list_len].children.push({
          name: item.name,
          money: item.money,
          createTime: new_time
        })
      }
    })
    if (list.length > 0) {
      list[list_len].num = num
    }
    return list
  },
  // 查看项目佣金明细
  getProjectCommissionInfo () {
    projectCommissionInfo(this.data.sendData).then(({ data }) => {
      let commissionInfo = {}
      commissionInfo.commission = data.commission
      commissionInfo.proPrice = data.proPrice
      commissionInfo.bletPrice = data.bletPrice
      commissionInfo.pushPrice = data.pushPrice
      let list = data.clerkBalanceRecordListAll.rows
      list = this.handerList(list)
      list = [...this.data.list, ...list]
      list.forEach(item => {
        item.num = (item.num).toFixed(2) * 1
      })
      this.data.pages = data.clerkBalanceRecordListAll.pages
      this.setData({
        list: list,
        commissionInfo: commissionInfo
      })
    })
  },
  // 查看分销佣金明细
  getDistributionCommissionInfo () {
    distributionCommissionInfo(this.data.sendData).then(({ data }) => {
      // data =  {

      //   "msg": "成功",
    
      //   "pushPrice": 0,
    
      //   "bletPrice": 0,
    
      //   "success": true,
    
      //   "clerkBalanceRecordListAll": {
    
      //     "total": 17,
    
      //     "rows": [{
    
      //       "searchValue": null,
    
      //       "createType": null,
    
      //       "createBy": null,
    
      //       "createTime": "2024-08-26 10:52:26",
    
      //       "updateBy": null,
    
      //       "updateType": null,
    
      //       "updateTime": null,
    
      //       "delState": "N",
    
      //       "delType": null,
    
      //       "delUser": null,
    
      //       "delTime": null,
    
      //       "remark": null,
    
      //       "params": {},
    
      //       "id": 496,
    
      //       "storeId": 497,
    
      //       "storeUserId": 1014,
    
      //       "name": "24082601",
    
      //       "handleType": "add",
    
      //       "money": 10.00,
    
      //       "busType": "pro",
    
      //       "techRemark": "订单id:24082609360839397792",
    
      //       "productId": 643,
    
      //       "verifyCodeTime": null,
    
      //       "busTypeNo": null,
    
      //       "storeUserName": null,
    
      //       "storeUserPhone": null,
    
      //       "storeUserUserType": null
    
      //     }, {
    
      //       "searchValue": null,
    
      //       "createType": null,
    
      //       "createBy": null,
    
      //       "createTime": "2024-08-26 09:44:19",
    
      //       "updateBy": null,
    
      //       "updateType": null,
    
      //       "updateTime": null,
    
      //       "delState": "N",
    
      //       "delType": null,
    
      //       "delUser": null,
    
      //       "delTime": null,
    
      //       "remark": null,
    
      //       "params": {},
    
      //       "id": 471,
    
      //       "storeId": 497,
    
      //       "storeUserId": 1014,
    
      //       "name": "24082601",
    
      //       "handleType": "add",
    
      //       "money": 10.00,
    
      //       "busType": "pro",
    
      //       "techRemark": "订单id:24082609360839397792",
    
      //       "productId": 643,
    
      //       "verifyCodeTime": null,
    
      //       "busTypeNo": null,
    
      //       "storeUserName": null,
    
      //       "storeUserPhone": null,
    
      //       "storeUserUserType": null
    
      //     }, {
    
      //       "searchValue": null,
    
      //       "createType": null,
    
      //       "createBy": null,
    
      //       "createTime": "2024-08-26 09:44:00",
    
      //       "updateBy": null,
    
      //       "updateType": null,
    
      //       "updateTime": null,
    
      //       "delState": "N",
    
      //       "delType": null,
    
      //       "delUser": null,
    
      //       "delTime": null,
    
      //       "remark": null,
    
      //       "params": {},
    
      //       "id": 470,
    
      //       "storeId": 497,
    
      //       "storeUserId": 1014,
    
      //       "name": "24082601",
    
      //       "handleType": "add",
    
      //       "money": 10.00,
    
      //       "busType": "pro",
    
      //       "techRemark": "订单id:24082609360839397792",
    
      //       "productId": 643,
    
      //       "verifyCodeTime": null,
    
      //       "busTypeNo": null,
    
      //       "storeUserName": null,
    
      //       "storeUserPhone": null,
    
      //       "storeUserUserType": null
    
      //     }, {
    
      //       "searchValue": null,
    
      //       "createType": null,
    
      //       "createBy": null,
    
      //       "createTime": "2024-08-26 09:43:20",
    
      //       "updateBy": null,
    
      //       "updateType": null,
    
      //       "updateTime": null,
    
      //       "delState": "N",
    
      //       "delType": null,
    
      //       "delUser": null,
    
      //       "delTime": null,
    
      //       "remark": null,
    
      //       "params": {},
    
      //       "id": 469,
    
      //       "storeId": 497,
    
      //       "storeUserId": 1014,
    
      //       "name": "24082601",
    
      //       "handleType": "add",
    
      //       "money": 10.00,
    
      //       "busType": "pro",
    
      //       "techRemark": "订单id:24082609360839397792",
    
      //       "productId": 643,
    
      //       "verifyCodeTime": null,
    
      //       "busTypeNo": null,
    
      //       "storeUserName": null,
    
      //       "storeUserPhone": null,
    
      //       "storeUserUserType": null
    
      //     }, {
    
      //       "searchValue": null,
    
      //       "createType": null,
    
      //       "createBy": null,
    
      //       "createTime": "2024-08-26 09:14:52",
    
      //       "updateBy": null,
    
      //       "updateType": null,
    
      //       "updateTime": null,
    
      //       "delState": "N",
    
      //       "delType": null,
    
      //       "delUser": null,
    
      //       "delTime": null,
    
      //       "remark": null,
    
      //       "params": {},
    
      //       "id": 455,
    
      //       "storeId": 497,
    
      //       "storeUserId": 1014,
    
      //       "name": "24082601",
    
      //       "handleType": "add",
    
      //       "money": 149.20,
    
      //       "busType": "store",
    
      //       "techRemark": "订单id:24082609132411997794",
    
      //       "productId": 643,
    
      //       "verifyCodeTime": null,
    
      //       "busTypeNo": null,
    
      //       "storeUserName": null,
    
      //       "storeUserPhone": null,
    
      //       "storeUserUserType": null
    
      //     }, {
    
      //       "searchValue": null,
    
      //       "createType": null,
    
      //       "createBy": null,
    
      //       "createTime": "2024-08-26 09:14:52",
    
      //       "updateBy": null,
    
      //       "updateType": null,
    
      //       "updateTime": null,
    
      //       "delState": "N",
    
      //       "delType": null,
    
      //       "delUser": null,
    
      //       "delTime": null,
    
      //       "remark": null,
    
      //       "params": {},
    
      //       "id": 453,
    
      //       "storeId": 497,
    
      //       "storeUserId": 1014,
    
      //       "name": "24082601",
    
      //       "handleType": "add",
    
      //       "money": 10.00,
    
      //       "busType": "pro",
    
      //       "techRemark": "订单id:24082609132411997794",
    
      //       "productId": 643,
    
      //       "verifyCodeTime": null,
    
      //       "busTypeNo": null,
    
      //       "storeUserName": null,
    
      //       "storeUserPhone": null,
    
      //       "storeUserUserType": null
    
      //     }, {
    
      //       "searchValue": null,
    
      //       "createType": null,
    
      //       "createBy": null,
    
      //       "createTime": "2024-08-22 14:06:20",
    
      //       "updateBy": null,
    
      //       "updateType": null,
    
      //       "updateTime": null,
    
      //       "delState": "N",
    
      //       "delType": null,
    
      //       "delUser": null,
    
      //       "delTime": null,
    
      //       "remark": null,
    
      //       "params": {},
    
      //       "id": 448,
    
      //       "storeId": 497,
    
      //       "storeUserId": 1014,
    
      //       "name": "qc新店铺项目",
    
      //       "handleType": "add",
    
      //       "money": 10.00,
    
      //       "busType": "pro",
    
      //       "techRemark": "订单id:24082016523120897791",
    
      //       "productId": 642,
    
      //       "verifyCodeTime": null,
    
      //       "busTypeNo": null,
    
      //       "storeUserName": null,
    
      //       "storeUserPhone": null,
    
      //       "storeUserUserType": null
    
      //     }, {
    
      //       "searchValue": null,
    
      //       "createType": null,
    
      //       "createBy": null,
    
      //       "createTime": "2024-08-22 14:04:44",
    
      //       "updateBy": null,
    
      //       "updateType": null,
    
      //       "updateTime": null,
    
      //       "delState": "N",
    
      //       "delType": null,
    
      //       "delUser": null,
    
      //       "delTime": null,
    
      //       "remark": null,
    
      //       "params": {},
    
      //       "id": 447,
    
      //       "storeId": 497,
    
      //       "storeUserId": 1014,
    
      //       "name": "qc新店铺项目",
    
      //       "handleType": "add",
    
      //       "money": -19.99,
    
      //       "busType": "store",
    
      //       "techRemark": "订单id:24082016523120897791",
    
      //       "productId": 642,
    
      //       "verifyCodeTime": null,
    
      //       "busTypeNo": null,
    
      //       "storeUserName": null,
    
      //       "storeUserPhone": null,
    
      //       "storeUserUserType": null
    
      //     }, {
    
      //       "searchValue": null,
    
      //       "createType": null,
    
      //       "createBy": null,
    
      //       "createTime": "2024-08-22 14:04:44",
    
      //       "updateBy": null,
    
      //       "updateType": null,
    
      //       "updateTime": null,
    
      //       "delState": "N",
    
      //       "delType": null,
    
      //       "delUser": null,
    
      //       "delTime": null,
    
      //       "remark": null,
    
      //       "params": {},
    
      //       "id": 445,
    
      //       "storeId": 497,
    
      //       "storeUserId": 1014,
    
      //       "name": "qc新店铺项目",
    
      //       "handleType": "add",
    
      //       "money": 10.00,
    
      //       "busType": "pro",
    
      //       "techRemark": "订单id:24082016523120897791",
    
      //       "productId": 642,
    
      //       "verifyCodeTime": null,
    
      //       "busTypeNo": null,
    
      //       "storeUserName": null,
    
      //       "storeUserPhone": null,
    
      //       "storeUserUserType": null
    
      //     }, {
    
      //       "searchValue": null,
    
      //       "createType": null,
    
      //       "createBy": null,
    
      //       "createTime": "2024-08-20 17:45:10",
    
      //       "updateBy": null,
    
      //       "updateType": null,
    
      //       "updateTime": null,
    
      //       "delState": "N",
    
      //       "delType": null,
    
      //       "delUser": null,
    
      //       "delTime": null,
    
      //       "remark": null,
    
      //       "params": {},
    
      //       "id": 398,
    
      //       "storeId": 497,
    
      //       "storeUserId": 1014,
    
      //       "name": "qc新店铺项目",
    
      //       "handleType": "add",
    
      //       "money": 10.00,
    
      //       "busType": "pro",
    
      //       "techRemark": "订单id:24082017444438181472",
    
      //       "productId": 642,
    
      //       "verifyCodeTime": null,
    
      //       "busTypeNo": null,
    
      //       "storeUserName": null,
    
      //       "storeUserPhone": null,
    
      //       "storeUserUserType": null
    
      //     }],
    
      //     "code": 200,
    
      //     "msg": "查询成功",
    
      //     "pages": 2,
    
      //     "lastPage": false
    
      //   },
    
      //   "commission": 150.00,
    
      //   "proPrice": 150.00
    
      // }
      let list = data.clerkBalanceRecordListAll.rows
      list = this.handerList(list)
      list = [...this.data.list, ...list]
      // console.log(list)
      list.forEach(item => {
        item.num = (item.num).toFixed(2) * 1
      })
      this.setData({
        'commissionInfo.commission': data.divPrice,
        list: list,
        pages: data.clerkBalanceRecordListAll.pages,
        dongCashPrice: data.dongCashPrice
      })
    })
  },
  // 查看我的客户明细
  getCustomInfo () {
    customInfo(this.data.sendData).then(({ data }) => {
      let list = data.rows
      list = this.handerList(list)
      list = [...this.data.list, ...list]
      // list.forEach(item => {
      //   item.num = (item.num).toFixed(2) * 1
      // })
      this.data.pages = data.pages
      this.setData({
        'commissionInfo.commission': data.total,
        list: list
      })
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  // options.type
  // 0：查看项目佣金明细，1：查看分销佣金明细，2：查看我的客户明细
  onLoad(options) {
    let type = parseInt(options.type)
    let self = this
    this.getList = (function (self, type) {
      if (type === 0) {
        return self.getProjectCommissionInfo
      }
      if (type === 1) {
        return self.getDistributionCommissionInfo
      }
      if (type === 2) {
        return self.getCustomInfo
      }
    })(self, type)
    
    this.setData({
      type: type
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    this.setData({
      list: []
    })
    this.getList()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {
    if (this.data.pages <= this.data.sendData.pageNum) {
      return
    }
    this.data.sendData.pageNum = this.data.sendData.pageNum + 1
    this.getList()
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})