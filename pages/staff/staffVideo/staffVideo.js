// pages/staffVideo/staffVideo.js
import * as socketUtil from'../../../api/socketUtil'
import * as API_Teching from '../../../api/teching'
Page({

  /**
   * 页面的初始数据
   */
  data: {

    params:{
      pageNum:1,
      pageSize:20
    },
    finished:false,
    list:[],
    store:{}
  },
  videoloadedmetadata(e){
    let index=e.currentTarget.dataset.index;
 
    let height=e.detail.height;
    let width=e.detail.width;
    
    let ob="list["+index+"].videoheight";
    this.setData({
      [ob]:height/width*46.61+'vw'
    })
  },
  RECEIVE_MSG_INFORM:function(json){
    //收到新消息
    let reqParam={storeId:this.data.store.id} ;
    socketUtil.sendData("MY_MSG_UNREAD_COUNT",reqParam);
  },
  MY_MSG_UNREAD_COUNT:function(json){
   let unreadMsgCount= json.info.unreadMsgCount;
   if(unreadMsgCount==0){
    wx.removeTabBarBadge({
      index: 1
     })
   }else{
    wx.setTabBarBadge({
      index: 1,
      text: unreadMsgCount+''
     })
   }
 },
 goDetail(e){
  wx.navigateTo({
    url: '/pages/teachingDetail/teachingDetail?id='+e.currentTarget.dataset.id,
  })
 },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let store = wx.getStorageSync('store')
    this.setData({store:store})
    this.getList()
  },
  getList(){
    API_Teching.getList(this.data.params).then(res=>{
      let arr = this.data.list
      arr.push(...res.rows)
      this.setData({
        list:arr,
        finished:res.lastPage
      })
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    getApp().watch(this.MY_MSG_UNREAD_COUNT,'MY_MSG_UNREAD_COUNT')//注册监听
    getApp().watch(this.RECEIVE_MSG_INFORM,'RECEIVE_MSG_INFORM')//注册监听
    let reqParam={storeId:this.data.store.id} ;
    socketUtil.sendData("MY_MSG_UNREAD_COUNT",reqParam);
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    if(this.data.finished){
      return
    }
    var page = this.data.params.pageNum + 1
    this.setData({
      'params.pageNum':page
    })
    this.getList()
  },

   
})