// pages/staff/retail/retail.js
import { staffDistribution } from '../../../api/fix'
import * as API_Adamember from '../../../api/adaMember'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    income: {},
    hasSettleAccount: false
  },
  previewCode () {
    if(!this.data.hasSettleAccount) {
      wx.showToast({
        title: '请绑定结算卡',
        icon: 'none'
      })
      return
    }
    let store = wx.getStorageSync('store')
    let code = store.member.belowImageUrl
    if (!code) {
      API_Adamember.refreshStoreBelow().then(({ data }) => {
        store.member.belowImageUrl = data.belowImageUrl
        wx.setStorageSync('store', store)
        wx.previewImage({
          urls: [data.belowImageUrl]
        })
      })
    } else {
      wx.previewImage({
        urls: [code]
      })
    }
  },
  withdrawal () {
    API_Adamember.applyStaffDrawcash()
  },
  tocustom() {
    wx.navigateTo({
      url: '/pages/staff/commission/commission?type=2',
    })
  },
  todetail() {
    wx.navigateTo({
      url: '/pages/staff/commission/commission?type=1',
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    staffDistribution().then(({ data }) => {
      this.setData({
        income: data
      })
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
      const that = this
    API_Adamember.getStaffSettleAccount().then(res=>{
      if(res.code==200){
        if(!res.data){
          that.setData({hasSettleAccount:false});
        }else{
          that.setData({hasSettleAccount:true});
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})