// pages/staff/staffMy/staffMy.js
import * as API_SYSConfig from "../../../api/sysConfig";
import * as API_Member from "../../../api/member";
import * as API_Stroe from '../../../api/store'
const service = require('../../../utils/service')
// pages/UCenter/index/index.js
import * as socketUtil from '../../../api/socketUtil'
import * as API_Upload from '../../../utils/fileUpload'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    service_phone: '',
    store: {},
    showStoreManager: false,
  },
  onChooseAvatar (e) {
    const { avatarUrl } = e.detail
    API_Upload.upload(avatarUrl, res => {
      const { url } = JSON.parse(res.data)
      const data = {
        headImg: url,
        id: this.data.store.id
      }
      API_Member.updateMember(data).then(res => {
        if (res.code === 200) {
          let store = wx.getStorageSync('store')
          store.member.headImg = url
          wx.setStorageSync('member', store.member)
          wx.setStorageSync('store', store)
          this.setData({
            'store.headImg': url
          })
        }
      })
    }, () => {
      wx.showToast({
        title:'上传失败',
        icon:'none'
      })
    })
  },
  exit() {
    wx.removeStorageSync('token')
    wx.removeStorageSync('member')
    wx.removeStorageSync('openid')
    wx.removeStorageSync('store')
    wx.removeStorageSync('storeId')
    wx.removeStorageSync('loginType')
    getApp().globalData.openId = null
    wx.showToast({
      title: '退出成功',
      icon: 'none',
      success: () => {
        setTimeout(() => {
          wx.reLaunch({
            url: '/pages/middlePage/middlePage',
          })
        }, 1000)
      }
    })
  },
  RECEIVE_MSG_INFORM: function (json) {
    //收到新消息
    let reqParam = {
      storeId: this.data.store.id
    };
    socketUtil.sendData("MY_MSG_UNREAD_COUNT", reqParam);
  },
  MY_MSG_UNREAD_COUNT: function (json) {
    let unreadMsgCount = json.info.unreadMsgCount;
    if (unreadMsgCount == 0) {
      wx.removeTabBarBadge({
        index: 1
      })
    } else {
      wx.setTabBarBadge({
        index: 1,
        text: unreadMsgCount + ''
      })
    }
  },
  protocolTemplate(e) {
    wx.navigateTo({
      url: '../../protocol/protocol?code=' + e.currentTarget.dataset.code,
    })
  },
  toShiMing() {
    // wx.navigateTo({
    //   url: '../../shiming/shiming',
    // })
    wx.navigateTo({
      url: '../bankCard/bankCard?type=0',
    })
  },
  toBankCard() {
    wx.navigateTo({
      url: '../bankCard/bankCard?type=1',
    })
  },
  goOpinion() {
    wx.navigateTo({
      url: '/pages/UCenter/opinion/opinion',
    })
  },
  //  goStoreUser(){
  //   wx.navigateTo({
  //     url: '/pages/storeUser/storeUser',
  //   })
  // },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    API_SYSConfig.getValueByKey('service_phone').then(res => {
      this.setData({
        service_phone: res
      })
    })
    let statusBarHeight = getApp().globalData.statusBarHeight;
    this.setData({
      statusBarHeight: statusBarHeight
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.setData({
      store: wx.getStorageSync('store').member
    })
    // getApp().watch(this.MY_MSG_UNREAD_COUNT,'MY_MSG_UNREAD_COUNT')//注册监听
    // getApp().watch(this.RECEIVE_MSG_INFORM,'RECEIVE_MSG_INFORM')//注册监听
    // let reqParam = {
    //   storeId: this.data.store.id
    // };
    // socketUtil.sendData("MY_MSG_UNREAD_COUNT",reqParam);

    // this.flushInfo();
    // this.flushLogin();
    // this.initShowView();
  },
  /**
   * 初始化哪些需要展示
   */
  initShowView() {
    let that = this;
    API_Stroe.getRoleTypes().then(res => {
      let showStoreManager = false;
      if (res.code == 200) {
        let list = res.data;
        if (list.indexOf('store_manager') > -1 || list.indexOf('store_yunYing') > -1) {
          showStoreManager = true;

        }
      }
      that.setData({
        showStoreManager: showStoreManager
      })
    })
  },
  flushInfo() {
    const member = wx.getStorageSync('member')
    this.setData({
      userInfo: member
    })
  },
  // getPhone:function(e){
  //   let that=this;
  //   if(!e.detail.iv){
  //   //认为用户拒绝了或者关闭了授权 
  //   return;
  //   }
  //       const params = {encryptedData:e.detail.encryptedData,iv:e.detail.iv,code:this.data.wxlogincode}
  //       API_Member.flushPhone(params).then(res =>{
  //         if(res.code==200){
  //           let data=res.data;
  //           wx.setStorageSync('token',data.token)
  //           wx.setStorageSync('member',data.member)

  //           that.flushInfo();
  //         }else{
  //           wx.showToast({
  //             title:res.msg,
  //             icon:'none'
  //           })  
  //         }
  //         that.flushLogin();
  //       })
  // },
  flushLogin: function () {
    //因为这个code只能用一次，使用一次后需要刷新下
    let that = this;
    wx.login({
      timeout: 8000,
      success(res) {
        that.setData({
          wxlogincode: res.code
        });

      }
    })
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  callPhone() {
    wx.makePhoneCall({
      phoneNumber: this.data.service_phone + ''
    })
  }

})