// pages/storeApplyResult/storeApplyResult.js
import * as API_Store from '../../api/store'
import * as API_Member from '../../api/member'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    storeApply:{

    },
    back:false,
    showFollowSer:'N'
  },
  initShowFollowSer:function(){
    let that=this;
    API_Member.showFollowSer().then(res =>{
      if(res.code==200){
        that.setData({showFollowSer:res.data.showFlag});
      }
    })
  },
  showBigImg(e){
    if(this.data.timestamp){
      this.data.timestamp=Date.parse(new Date()); 
    }
    let imagesrc="https://wx.yiqisa.cn/mp/qrcode.jpg?v="+ this.data.timestamp;
    wx.previewImage({
      current: imagesrc, // 当前显示图片的http链接
      urls: [imagesrc] // 需要预览的图片http链接列表
    })
   },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if(options.back){
      this.setData({
        back:true
      })
    }
  },
  edit(){
    wx.navigateTo({
      url: '/pages/storeApply/storeApply',
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    API_Store.getApply().then(res => {
      this.setData({storeApply:res})
      if(res.applyState!='audit_IN'&&res.applyState!='audit_REFUSE'){
        wx.redirectTo({
          url: '../middlePage/middlePage',
        })
      }
    })
    this.initShowFollowSer();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

   
})