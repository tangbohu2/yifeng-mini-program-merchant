// pages/orderList/orderList.js
const util = require('../../utils/util')
import * as API_Order from '../../api/order'
import { getSmsTmp,sendSms,getOrderSortList } from '../../api/fix'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    //样式控制器
    stateClass:'',
    tabindex:0,
    datestart:'2021-11-11',//取出当前门店最早的核销日期，没有核销订单则取当日
    dateend:'',
    param:{
      pageNum: 1,
      pageSize: 20,
      // productId:'',
      classifyId: -1,
      orderState:'',
      createTime:util.getDate(),
    },
    
    orderList:[],
    paramArrayIndex:0,
    paramArray: [],
    finished:false,
    model: false,
    smsList: [],
    smsindex: 0,
    smsform: {
      mobiles: '', //手机号
      orderId: '', //订单id
      templateId: '' //短信模板
    },
    defaultSmsform: { //提交完成后初始化为默认数据
      mobiles: '', //手机号
      orderId: '', //订单id
      templateId: '' //短信模板
    }
  },  
  handleCancel() {
    this.setData({
      model: false,
      smsform: {...this.data.defaultSmsform}
    })
  },
  getSmsList: function() {
    getSmsTmp().then(res => {
      this.setData({
        smsList: res.rows
      })
      //设置默认模版id
      const smsform = this.data.smsform
      let defaultSmsform = this.data.defaultSmsform
      if(this.data.smsList.length != 0) {
        const tmpId = this.data.smsList[0].id
        smsform.templateId = tmpId
        defaultSmsform.templateId = tmpId
        this.setData({
          smsform,
          defaultSmsform
        })
      }
    })
  },
  afterTip(e) {
    const ph = e.currentTarget.dataset.ph
    const id = e.currentTarget.dataset.id
    const smsform = this.data.smsform
    smsform.mobiles = ph
    smsform.orderId = id
    this.setData({
      model: true,
      smsform
    })
  },
  handleTip: function(e) {
    const index = e.currentTarget.dataset.index
    const id = e.currentTarget.dataset.id
    const smsform = this.data.smsform
    smsform.templateId = id
    this.setData({
      smsindex: index,
      smsform
    })
  },
  handleSend: function() {
    if(this.data.smsList.length == 0) {
      wx.showToast({
        title: '请设置短信模版',
        icon: 'error'
      })
      return
    }
    console.log(this.data.smsform)
    if(!this.data.smsform.mobiles || this.data.smsform.mobiles == '') {
      wx.showToast({
        title: '手机号不存在',
        icon: 'error'
      })
      return
    }
    sendSms(this.data.smsform).then(() =>{
      wx.showToast({
        title: '发送成功',
      })
    }).finally(() => {
      this.setData({
        model:false,
        smsform: {...this.data.defaultSmsform}
      })
    })
  },
  paramArrayChange:function(e){
    let paramArrayIndex =e.detail.value;
    this.setData({
      paramArrayIndex:paramArrayIndex,
      orderList:[],
      'param.classifyId':this.data.paramArray[paramArrayIndex].id,
      'param.pageNum':1
    });
    this.getList()
  },
  bindDateChange: function(e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      'param.createTime': e.detail.value,
      orderList:[],
      'param.pageNum':1
    })
    this.getList()
  },
  tabchange:function(e){
    let tabindex=e.currentTarget.dataset.index;
    let state=e.currentTarget.dataset.state;
    this.setData({
      tabindex:tabindex,
      orderList:[],
      'param.orderState':state,
      'param.pageNum':1
    });
    this.getList()
  },
  aggreeFund:function(e){
    wx.showModal({
      title:'同意退款',
      content:'同意后款项将直接退回到用户账户中，不可撤销，确定继续？',
      success (res) {
        if (res.confirm) {
          
        }
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let that = this
    // 分类是商品名字

    // API_Order.getClassifyList().then(res =>{
    //   let all = {
    //     id:'',
    //     productName:'全部分类'
    //   }
    //   //如果存在分类才会接下来的动作
    //   if(res.rows){
    //     res.rows.unshift(all)
    //     that.setData({
    //       paramArray:res.rows,
    //       'param.productId':res.rows[0].id
    //     })
    //     that.getList()
    //   }else{
    //     let arr =[all]
    //     that.setData({
    //       paramArray:arr
    //     })
    //   }
    // })
    that.getList()
    this.getSmsList()
    this.getOrderSortList()
  },
  getList(){
    let that = this
    const param = that.data.param
    if(!param.orderState || param.orderState == '') {
      delete param.orderState
    }
    API_Order.getList(param).then(res =>{
      console.log(res)
      if(res.code == 200){
        var list = that.data.orderList
        for(var i = 0; i < res.rows.length; i++){
          let item = res.rows[i]
          switch (item.orderState){
            case 'wait_use':
              item.classStyle = ''
              item.classComment = '待核销'
              break
            case 'used':
              item.classStyle = 'hasverify'
              item.classComment = '已核销'
              break
            case 'refund_success':
              item.classStyle = 'refund'
              item.classComment = '退款'
              break
          }
          list.push(item)
        }
        that.setData({
          finished:res.lastPage,
          orderList:list
        })
        //设置默认提醒上传数据
        const smsform = that.data.smsform
        let defaultSmsform = that.data.defaultSmsform
        if(that.data.orderList.length != 0) {
          const data = that.data.orderList[0]
          smsform.mobiles = data.memberPhone
          smsform.orderId = data.id
          defaultSmsform = {...smsform}
          that.setData({
            smsform,
            defaultSmsform
          })
        } 
      }
    })
  },
  getOrderSortList() {
    getOrderSortList().then(res => {
      console.log(res)
      let paramArray = [
        {
          id: -1,
          productName:'全部分类'
        }
      ]
      res.data.forEach(item => {
        paramArray.push({
          id: item.id,
          productName: item.classifyName
        })
      })
      this.setData({
        paramArray
      })
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    let dateend=util.getDate();
    this.setData({
      dateend:dateend,
      'param.createTime':dateend
    });
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    let that = this
    if(that.data.isFinished){
      return
    }
    that.setData({
      'params.pageNum':that.data.params.pageNum + 1
    })
    that.getList()
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  
})