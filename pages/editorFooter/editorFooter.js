// pages/editorFooter/editorFooter.js
import * as API_RichEditor from '../../api/richEditor'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    footerData:[],
    type:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({type:options.type})
    this.getList()
  },

  getList(){
    API_RichEditor.getByType(this.data.type).then(res => {
      this.setData({footerData:res})
    })
  },
  selectFooter:function(e){
    wx.showModal({
      title:'追加',
      cancelText: '暂不追加',
      content:'是否追加此模块',
      success (res) {
        if (res.confirm) {
          var page = getCurrentPages()
          var prevPage = page[page.length-2]
          wx.navigateBack({
            success:function (params) {
                prevPage.doHandleEditorAppend(e.currentTarget.dataset.id)
            },
            delta: 0,
          })
        }
      }
    })
   },
   add(){
    wx.navigateTo({
      url: '../editEditorFooter/editEditorFooter?type=footer',
    })
   },
   edit(e){
    var id = e.currentTarget.dataset.id
    wx.navigateTo({
      url: '../editEditorFooter/editEditorFooter?id='+id,
    })
   },
   
   del(e){
     let that = this
      wx.showModal({
        title:'删除',
        content:'是否删除该数据',
        success (res) {
          if (res.confirm) {
            API_RichEditor.del(e.currentTarget.dataset.id).then( res =>{
              that.getList()
              wx.showToast({
                title: '删除成功',
                icon:'success'
              })
            })
          }
        }
      })
    }, 
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },


})