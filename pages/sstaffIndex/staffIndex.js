// pages/sstaffIndex/staffIndex.js
import { staffIndex,getStaffOrderList } from '../../api/fix'
import * as API_Order from '../../api/order'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userName: null,
    array: ['核销', '售后']
  },
  toServicelist(e) {
    const status = e.currentTarget.dataset.status
    wx.navigateTo({
      url: '/pages/staffdoortodoor/staffdoortodoor?status='+status,
    })
  },
  goDistribution () {
    wx.navigateTo({
      url: '/pages/staff/retail/retail',
    })
  },
  goVerificationRecord(){
    wx.navigateTo({
      url: '/pages/verificationRecord/verificationRecord?type=1',
    })
  },
  scanCodeChange (e) {
    let val = parseInt(e.detail.value)
    this.scanCode(val)
  },
  scanCode(type){
    this.setData({scanreturn:true})
    wx.scanCode({
      onlyFromCamera: false,
      scanType:['barCode','datamatrix','pdf417','qrCode'],
      success(res){
        var verifyCode = res.result
        let params = {
         verifyCode:verifyCode
        }
        let scanFun = null
        if (type === 0) {
          scanFun =  API_Order.scanVerify(params)
        }
        if (type === 1) {
          scanFun =  API_Order.scanAfter(params)
        }
        scanFun.then(res=>{
          if(res.code == 200){
            wx.showToast({
              title: '核销成功',
            })
            setTimeout(()=>{
              let loginType = wx.getStorageSync('loginType')
              let type = loginType === 1 ? 2 : 1
              wx.navigateTo({
                url: '/pages/verificationRecord/verificationRecord?type=' + type,
              })
            }, 1500)
          }else{
            wx.showToast({
              title: res.msg || '验证失败',
              icon:'none',
              duration:2000
            })
          }
        })
      },
      fail(res){
         console.log(res)
      }
    })
  },
  codeHandleChange (e) {
    let val = parseInt(e.detail.value)
    wx.navigateTo({
      url: '/pages/verificationHandle/verificationHandle?index=' + val,
    })
  },
  lookdoordetail() {
    wx.navigateTo({
      url: '/pages/staffdoortodoor/staffdoortodoor',
    })
  },
  lookdetail(e) {
    // 0：查看项目佣金明细，1：查看分销佣金明细
    let type = e.currentTarget.dataset.type
    wx.navigateTo({
      url: '/pages/staff/commission/commission?type=' + type,
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    let store = wx.getStorageSync('store') || {}
    this.setData({
      userName: store.member.userName
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    staffIndex().then(({ data }) => {
      getApp().globalData.balance = data.balance
      this.setData({
        staffData: data
      })
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})