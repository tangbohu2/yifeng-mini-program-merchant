//index.js
//获取应用实例
// import * as API_Category from '../../api/category'
import * as API_Store from '../../api/store'

//第二种引入方式
const QQMapWX = require('../../utils/qqmap-wx-jssdk.min')
const config = require('../../utils/config.js')
const service = require('../../utils/service')
import * as socketUtil from'../../api/socketUtil'
var qqMapSdk;
Page({
  data: {
    showAD:false,
    indicatorDots: false,
    // 分类
    categoryList: [],
    locationAddr:'定位中...',
    latitude:'',
    longitude:'',
    params: {
      //经度
      longitude:'',
      //纬度
      latitude:''
    },
    advertisingList:[],
    appendImgStr:''
  },
  uploadEvent:function(e){
    this.setData({appendImgStr:e.detail.picsList})
   },
  changeIndicatorDots() {
    this.setData({
      indicatorDots: !this.data.indicatorDots
    })
  },

 

  registFun(){
    const member = {nickName:'cc',username:'啊吧啊吧'}
      service.registBind(JSON.stringify(member)).then((res) => {
        //doSomething..
      })
  },
  
  // sendMsg:function(){
  //   let reqParam={content:'我发了消息',msgType: 'text',storeId:9,receiveId:27} ;
  //   socketUtil.sendData("SEND_MSG",reqParam);
  // },
  async onShow() {
    getApp().watch(this.MY_MSG_UNREAD_COUNT,'MY_MSG_UNREAD_COUNT')//注册监听 获取我的未读消息数量
    getApp().watch(this.RECEIVE_MSG_INFORM,'RECEIVE_MSG_INFORM')//注册监听 收到新消息
  },
  RECEIVE_MSG_INFORM:function(json){
    //收到新消息
    
    let reqParam={} ;
    socketUtil.sendData("MY_MSG_UNREAD_COUNT",reqParam);
    console.log('收到新消息了');
  },
  MY_MSG_UNREAD_COUNT:function(json){
    
   let unreadMsgCount= json.info.unreadMsgCount;
   console.log('我有'+unreadMsgCount+'条未读消息');
  //  this.setData({unreadMsgCount:unreadMsgCount});
},
   //虚处理选中文本底部追加
   doHandleEditorAppend(e){
    this.choiceGuige.appendAffter(e)
  },
  //获取保存过的按钮信息
  saveAtt(e){
    this.choiceGuige.save()
  },
  getAtt(e){
    console.log(e.detail.data)
  },
  showPage(){
    wx.navigateTo({
      url: '../editorView/editorView?attId=4',
    })
  },
  async onLoad() {
    this.choiceGuige = this.selectComponent("#richEditor")
    this.choiceGuige2 = this.selectComponent("#view")
    // this.choiceGuige2.init(16)
    // this.choiceGuige.init('',2)

    wx.getSystemInfo({
      success: (result) => {
      },
    })
    this.setData({showLocationMySelf:getApp().globalData.showLocationMySelf});


    wx.showLoading({title: '加载中...',mask:true})
    let that = this

    wx.getSetting({
      withSubscriptions: true,
      success(res){
        if(res.authSetting["scope.userLocation"]  == undefined || !res.authSetting["scope.userLocation"] ){
           //无法获取位置处理
        }
      }
    })
    
    await that.initLocation()
    //加载轮播图数据
    wx.hideLoading()
    // this.animationInit();

    // return
    //查询此人是否有店铺:是否需要跳转申请页面
    // API_Store.getApply().then(res => {
    //   if (!res){
    //       wx.navigateTo({
    //         url: '/pages/storeApply/storeApply',
    //       })
    //   }
    // })
  },
  async initLocation(){
    
    let that = this
    wx.getLocation({
      type: 'gcj02',
      // //开启高精准度
      // isHighAccuracy:true,
      // //最多五秒等待
      // highAccuracyExpireTime:5000,
      success: function(res) {
          //纬度
      var latitude = res.latitude
      getApp().globalData.latitude = latitude
      //经度
      var longitude = res.longitude
      getApp().globalData.longitude = longitude
      that.reverseLocation(latitude,longitude)
      },
      fail(res){
        that.localFail();
      }
    })
  },
  reverseLocation(latitude,longitude){
    let that = this
    qqMapSdk = new QQMapWX({
      key: config.QQMapKey
    });
    qqMapSdk.reverseGeocoder({
      // location:{latitude:latitude,longitude:longitude},
      location:{latitude:39.0851,longitude:117.19937},

      complete(res){
        if(!res.status && res.status==0){
          console.log(res)
         that.localSuccess();
          const arr = [];
          const {address_component} = res.result
          //门牌号,街道,区,市
          arr.push(address_component.street_number?address_component.street_number:'')
          arr.push(address_component.street?address_component.street:'')
          arr.push(address_component.district?address_component.district:'')
          arr.push(address_component.city)
          //市级以下可能为空,删掉元素
          for(let i=arr.length-1; i>=0; i--){
            if(arr[i] == ""){
                arr.splice(i,1);
            }
          }
          //
          that.setData({
            locationAddr:arr[0],
            latitude:latitude,
            longitude:longitude
          })
        }else{
          that.localFail();
        }
      }
    })
  },
  localSuccess:function(){
    this.setData({showLocationMySelf:false})
    getApp().globalData.showLocationMySelf=false;
  },
  localFail:function(){
    this.setData({locationAddr:'定位失败',showLocationMySelf:true})
    getApp().globalData.showLocationMySelf=true;
  },
  changeLocation(){
    let that = this
    wx.chooseLocation({
      success: (res) => {
        that.localSuccess();
        if(res.name == ""){
          that.initLocation()
        }else{
          this.setData({
            locationAddr:res.name,
            latitude:res.latitude,
            longitude:res.longitude
          })
          that.reverseLocation(res.latitude,res.longitude)
           //纬度
        var latitude = res.latitude
        getApp().globalData.latitude = latitude
        //经度
        var longitude = res.longitude
        getApp().globalData.longitude = longitude
        }
      },
      complete(res){
        console.log(res)
      }
    })
  },

  onReachBottom: function () {

  },



  

})
