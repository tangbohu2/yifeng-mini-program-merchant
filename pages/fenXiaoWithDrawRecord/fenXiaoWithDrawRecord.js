// pages/fenXiaoWithDrawRecord/fenXiaoWithDrawRecord.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    withDrawList:[
      {name:"商城消费100元",time:"2022-07-06 13:19:04",type:"in",nums:"+10"},
      {name:"商城消费100元退款",time:"2022-07-06 13:19:04",type:"out",nums:"-10"},
      {name:"商城消费100元",time:"2022-07-06 13:19:04",type:"in",nums:"+10"},
      {name:"商城消费100元",time:"2022-07-06 13:19:04",type:"in",nums:"+10"},
      {name:"商城消费100元退款",time:"2022-07-06 13:19:04",type:"out",nums:"-10"},
      {name:"商城消费100元",time:"2022-07-06 13:19:04",type:"in",nums:"+10"},
      {name:"商城消费100元",time:"2022-07-06 13:19:04",type:"in",nums:"+10"},
      {name:"商城消费100元退款",time:"2022-07-06 13:19:04",type:"out",nums:"-10"},
      {name:"商城消费100元",time:"2022-07-06 13:19:04",type:"in",nums:"+10"},
      
    ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let globalData=getApp().globalData;
    let navHeadHeightpx=globalData.navHeadHeightpx;
    this.setData({navHeadHeightpx:navHeadHeightpx})
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})