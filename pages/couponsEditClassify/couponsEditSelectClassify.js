// pages/couponsEditSelect/couponsEditSelect.js
import * as API_Product from '../../api/product'
import * as API_Classify from '../../api/storeClassify'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    selectClassifyList:[],
    selectedList:[],
  },

  choice(e){
    let item = this.data.selectClassifyList[e.currentTarget.dataset.index]
    if(item.checked){
      for(var i = 0; i< this.data.selectedList.length; i++){
        if(this.data.selectedList[i].id == item.id){
          this.data.selectedList.splice(i,1)
          item.checked = false
        }
      }
    }else{
      this.data.selectedList.push(item)
      item.checked = true
    }
  },
  select(){
    let that = this
    var pages = getCurrentPages();
    var prevPage = pages[pages.length - 2];
    if (prevPage) {
      prevPage.setData({ selectClassifyList: that.data.selectedList })
      wx.navigateBack({
        //返回上一级
        delta: 1,
      })
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let params = {
      state:'Y'
    }
    API_Classify.getClassifySecond().then(res=>{
      this.setData({
        selectClassifyList:res
      })
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
