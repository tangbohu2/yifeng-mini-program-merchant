// pages/staffplus/staffplus.js
import { addStaff,editStaff,getStaffDetail,getList } from '../../api/fix'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    form: {
      userName: '',
      phone: '',
      password: '',
      staffMasterId: '',
      commissionRate: ''
    },
    teacher: '',
    index: 0,
    addOrEditUrl: 1, //1是增加，2是修改
    title: '新增员工',
    btnTitle: '确认新增',
    detail: {},
    list:[],
    teacherList: []
  },
  bindPickerChange: function(e) {
    console.log(this.data.list)
    console.log(e)
    if(this.data.list.length == 0) {
      return
    } else {
      const form = this.data.form
      form.staffMasterId = this.data.list[e.detail.value].id
      const teacher = this.data.list[e.detail.value].userName
      // form.teacher = this.data.array[e.detail.value]
      this.setData({
        index: e.detail.value,
        form,
        teacher
      })
    }
  },
  inputname(e) {
    const form = this.data.form
    form.userName = e.detail.value
    this.setData({
      form
    })
  },
  inputphone(e) {
    const form = this.data.form
    form.phone = e.detail.value
    this.setData({
      form
    })
  },
  inputpwd(e) {
    const form = this.data.form
    form.password = e.detail.value
    this.setData({
      form
    })
  },
  inputrate(e) {
    const form = this.data.form
    form.commissionRate = e.detail.value
    this.setData({
      form
    })
  },
  confirmAdd(){
    const form = this.data.form
    for(let i in form) {
      if(!form[i] || form[i] == '') {
        delete form[i]
      }
    }
    if(form.id) {
      editStaff(form).then(res => {
        wx.showToast({
          title: res.msg,
          icon: 'none'
        })
      })
    } else {
      addStaff(form).then(res => {
        wx.showToast({
          title: res.msg,
          icon: 'none'
        })
      })
    }
  },
  getDetail() {
    const id = this.data.form.id
    if(id) {
      getStaffDetail(id).then(res => {
        const staffMasterId = res.data.staffMasterId
        let tObj = {
          userName: null
        }
        if(this.data.list.length != 0) {
          tObj = this.data.list.find(item => {
            return item.id == staffMasterId
          })
        }
        this.setData({
          form: {...res.data,id},
          teacher: tObj ? tObj.userName ? tObj.userName : '' : ''
        })
        console.log(this.data.form)
      })
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad({id}) {
    getList().then(res => {
      let list = res.rows
      // const index = list.findIndex(item => {
      //   return item.id == id
      // })
      // if(index != -1) {
      //   list = list.splice(index,1)
      //   // delete list[index]
      // }
      list.forEach((item,index) => {
        if(item.id == id) {
          list.splice(index,1)
        }
      })
      if(list.length === 0) {
        this.setData({
          teacherList: [],
          list: []
        })
      }else {
        const newList = list.map(item => {
          return item.userName
        })
        this.setData({
          teacherList: newList,
          list
        })
      }
    }).then(() => {
      if(id) {
        const form = Object.assign({},this.data.form,{id: id})
        this.setData({
          addOrEditUrl: 2,
          btnTitle: '确认修改',
          title: '编辑员工',
          form
        })
        this.getDetail()
      } else {
        this.setData({
          addOrEditUrl: 1,
          title: '新增员工',
          btnTitle: '确认新增'
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})