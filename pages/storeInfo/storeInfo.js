// pages/storeInfo/storeInfo.js
import * as API_StoreApply from '../../api/store'
import * as API_Classify from '../../api/storeClassify'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    store:{},
    name:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let that = this
    let store = wx.getStorageSync('store')
    // this.setData({store:store})
    // let store = JSON.parse(options.store)
    store.storeShowImage = store.storeShowImage ? store.storeShowImage.split(",") : []
    that.setData({store:store})
    this.choiceGuige = this.selectComponent("#serviceItem");
    let e = {
      key:'jy_service_item',
      title:'服务保障',
      selected:store.serviceItem
    }
    this.choiceGuige.loadDic(e)
    API_Classify.getName(store.storeClassify).then(res=>{
      this.setData({
        name:res
      })
    })
  },

  toEdit(){
    API_StoreApply.getApply().then(res =>{
      if(res.applyState == 'audit_PASS'){
        wx.navigateTo({
          url: '../storeApply/storeApply?back=true',
        })
      }else{
        wx.navigateTo({
          url: '../storeApplyResult/storeApplyResult?back=true',
        })
      }
    })
    
  
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

   
})