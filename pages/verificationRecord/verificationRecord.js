// pages/verificationRecord/verificationRecord.js
const util = require('../../utils/util')
import * as API_Order from '../../api/order'
import { getListNewChange } from '../../api/fix'

Page({

  /**
   * 页面的初始数据
   */
  data: {
    finished:false,
    orderList:[],
    datestart:'2021-11-11',//取出当前门店最早的核销日期，没有核销订单则取当日
    dateend:'',
    param:{
      pageNum: 1,
      pageSize: 20,
      // productId:0,
      // orderState:'used',
      // verifyCodeTime:util.getDate(),
      verifyCodeTime:'全部',
      // orderByColumn: 'verifyCodeTime'
    },
    paramArrayIndex:0,
    paramArray: [],
    type: null,
    total: 0
  },
  paramArrayChange:function(e){
    let paramArrayIndex =e.detail.value;
    this.setData({
      paramArrayIndex:paramArrayIndex,
      orderList:[],
      'param.productId':this.data.paramArray[paramArrayIndex].id,
      'param.pageNum':1
    });
    this.getList()
  },
  bindDateChange: function(e) {
    this.setData({
      'param.verifyCodeTime': e.detail.value,
      orderList:[],
      'param.pageNum':1,
      // dateend:e.detail.value
    })
    this.getList()
    
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options.type)
    let that = this
    this.data.type = parseInt(options.type) || 0
    this.setData({
      type: parseInt(options.type) || 0
    })
    // console.log(this.data.type)
    // 分类是商品名字

    // API_Order.getClassifyList().then(res =>{
    //   //如果存在分类才会接下来的动作
    //   let all = {
    //     id:'',
    //     productName:'全部分类'
    //   }
    //   if(res.rows){
    //     res.rows.unshift(all)
    //     that.setData({
    //       paramArray:res.rows,
    //       'param.productId':res.rows[0].id
    //     })
    //     that.getList()
    //   }else{
    //     let arr =[all]
    //     that.setData({
    //       paramArray:arr
    //     })
    //   }
    // })
    
  },
  getList(){
    let that = this

    // getListNew(that.data.param).then(res =>{

    let getList = null
    let datas = {...that.data.param}
    if(datas.verifyCodeTime == '全部') {
      delete datas.verifyCodeTime
    }
    if (this.data.type === 0) {
      getList = API_Order.getList(datas)
    }
    if (this.data.type === 1) {
      const loginType = wx.getStorageSync('loginType')

      if(loginType == 1) {
        datas.verifyType = 'verify'
        console.log(datas)
      }
      
      getList = API_Order.getVerifyList(datas)
    }
    //商家端售后
    if(this.data.type === 2) {
      const loginType = wx.getStorageSync('loginType')
      if(loginType == 1) {
        datas.verifyType = 'aftersale'
      }
      getList = API_Order.getVerifyList(datas)
      // getList = getListNewChange(datas)
    }
    getList.then(res =>{
      if(res.code == 200){
        var list = that.data.orderList
        list.push(...res.rows)
        that.setData({
          finished:res.lastPage,
          orderList:list
        })
        if(that.data.type == 1) {
          that.setData({
            total: res.total
          })
          if(that.data.orderList.length >= that.data.total) {
            that.setData({
              isFinished: true
            })
          }
        }
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    let dateend=util.getDate();
    this.setData({dateend:dateend});
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.setData({
      orderList: [],
      isFinished: false,
      "param.pageNum": 1
    })
    this.getList()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    let that = this
    if(that.data.isFinished){
      return
    }
    that.setData({
      'param.pageNum':that.data.param.pageNum + 1
    })
    that.getList()
  },

   
})