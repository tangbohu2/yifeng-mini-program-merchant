// pages/technicianAdd/technicianAdd.js
import * as API_Dict from '../../api/dict'
import * as API_Upload from '../../utils/fileUpload'
import * as API_Technician from '../../api/technician'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    technicianClassifyArray:[],
    technicianClassifyIndex:0,
    technicianPositionalArray:[],
    technicianPositionalIndex:0,
    workYearsArray:[],
    workYearsIndex:0,
    submitState:true,
    technician:{
      techImage:'',
      techBacImage:'',
      techType:null,
      techName:'',
      techPhone:'',
      techPositional:'',
      workYears:'',
      goodatAreas:''
    },
    title:'',
    subtitle:'提交',
    editEnable:false
  },
  //技师分类
  technicianClassifyChange(e){
    let index =e.detail.value;
    this.setData({
      technicianClassifyIndex:index,
      'technician.techType':this.data.technicianClassifyArray[index].dictValue
    })
  },
  //技师职称
  technicianPositionalChange(e){
    let index =e.detail.value;
    this.setData({
      technicianPositionalIndex:index,
      'technician.techPositional':this.data.technicianPositionalArray[index].dictLabel,
    })
  },
  //
  workYearsChange(e){
    let index =e.detail.value;
    this.setData({
      workYearsIndex:index,
      'technician.workYears':this.data.workYearsArray[index].dictLabel,
    })
  },
  uploadImg: function (e) {
    var that = this;
    let type = e.currentTarget.dataset.img
    wx.showActionSheet({
     itemList: ['相册', '相机'],
     itemColor: "#40ba67",
     success: function (res) {
      if (!res.cancel) {
       if (res.tapIndex == 0) {
        that.chooseWxImageShop('album',type)
       } else if (res.tapIndex == 1) {
        that.chooseWxImageShop('camera',type)
       }
      }
     }
    })
   },
   chooseWxImageShop: function (type,img) {
    var that = this;
    wx.chooseImage({
      count: 1,
      sizeType: ['compressed'],
      sourceType: [type],
      success: function (res) {
        //门店形象照
        API_Upload.upload(res.tempFilePaths[0],res=>{
          const {url} = JSON.parse(res.data)
          if(img == 'img'){
            that.setData({'technician.techImage':url})
          }else{
            that.setData({'technician.techBacImage':url})
          }
      },res=>{
          wx.showToast({
              title:'上传失败',
              icon:'none'
          })
      })
      }
    })
  },
  // goodAt(e){
  //   let data = JSON.stringify(e.detail.data)
  //   this.setData({'technician.goodatAreas':data})
  // },
  changeName(e){
    this.setData({'technician.techName':e.detail.value})
  },
  changePhone(e){
    this.setData({'technician.techPhone':e.detail.value})
  },
   changePositional(e){
    this.setData({'technician.techPositional':e.detail.value})
  },
  changeGoodAt(e){
    this.setData({
      'technician.goodatAreas':e.detail.value
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
    if(options.jishiedit){
      this.setData({title:'修改信息',subtitle:'提交修改'})
      API_Technician.getOneInfo().then(res=>{
        if(res.code&&res.code!=200){
          wx.showToast({title: res.msg, icon:'none', duration:1500 })
          return
        }
        this.setData({
          technician:res
        })
        API_Dict.getDictData('work_years').then(ref =>{
          for(var i = 0; i<ref.length;i++){
            if(res.workYears == ref[i].dictLabel){
              this.setData({
                workYearsArray:ref,
                workYearsIndex:i
              })
              break
            }
          }
        })
      })
    }else  if(options.id){
      this.setData({title:'修改技师信息',subtitle:'提交修改',editEnable:true})
      API_Technician.getOne(options.id).then(res=>{
        this.setData({
          technician:res
        })
        // this.choiceGuige = this
        API_Dict.getDictData('work_years').then(ref =>{
          for(var i = 0; i<ref.length;i++){
            if(res.workYears == ref[i].dictLabel){
              this.setData({
                workYearsArray:ref,
                workYearsIndex:i
              })
              break
            }
          }
        })
      })
    }
    
    else{
      this.setData({title:'添加技师',subtitle:'提交',editEnable:true})
      // API_Dict.getDictData('tech_type').then(res =>{
      //   this.setData({
      //     technicianClassifyArray:res,
      //     'technician.techType':res[0].dictValue,
      //     'technician.techName':res[0].dictLabel,
      //   })
      // })
      // API_Dict.getDictData('tech_positional').then(res =>{
      //   this.setData({
      //     technicianPositionalArray:res,
      //     'technician.techPositional':res[0].dictLabel,
      //   })
      // })
      API_Dict.getDictData('work_years').then(res =>{
        this.setData({
          workYearsArray:res,
          'technician.workYears':res[0].dictLabel,
        })
      })
  
      // this.choiceGuige = this.selectComponent("#gooodAt")
      // let g = {
      //   key:'good_at',
      //   title:'擅长领域'
      // }
      // this.choiceGuige.loadDic(g)
    }
    
  },

  save(){
    let that = this
      const technician = this.data.technician
      that.setData({submitState:!that.data.submitState})
      if(!technician.techImage.trim()){
        that.setData({submitState:!that.data.submitState})
        wx.showToast({title: '请上传技师头像', icon:'none', duration:1500 })
        return
      }else if(!technician.techName.trim()){
        that.setData({submitState:!that.data.submitState})
        wx.showToast({title: '请填写技师名称', icon:'none', duration:1500 })
        return
      }else if(!technician.techPhone.trim()){
        that.setData({submitState:!that.data.submitState})
        wx.showToast({title: '请填写技师电话', icon:'none', duration:1500 })
        return
      }else if(!technician.techPositional.trim()){
        that.setData({submitState:!that.data.submitState})
        wx.showToast({title: '请填写技师职称', icon:'none', duration:1500 })
        return
      }else if(!technician.goodatAreas.trim()){
        that.setData({submitState:!that.data.submitState})
        wx.showToast({title: '请填写擅长领域', icon:'none', duration:1500 })
        return
      }else{
        if(technician.id){
          API_Technician.update(technician).then(res=>{
            
            if(res.code&&res.code!=200){
              that.setData({submitState:!that.data.submitState})
              wx.showToast({title: res.msg, icon:'none', duration:1500 })
              return;
            }
            var pages = getCurrentPages();
            var beforePage = pages[pages.length - 2];
            wx.navigateBack({
              delta: 0,
              success(e){
                beforePage.flush()
              }
            })
          })
        }else{
          API_Technician.create(technician).then(res =>{
            debugger
            if(res.id){
              var pages = getCurrentPages();
              var beforePage = pages[pages.length - 2];
              wx.navigateBack({
                delta: 0,
                success(e){
                  beforePage.flush()
                }
              })
            }else{
              that.setData({submitState:!that.data.submitState})
              wx.showToast({title: res.msg, icon:'none', duration:1500 })
            }
          })
        }
      }

  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

   
})