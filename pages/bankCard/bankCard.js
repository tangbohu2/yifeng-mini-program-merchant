// pages/bankCard/bankCard.js
import * as API_Adamember from '../../api/adaMember'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    adapaySettleaccount:{},
      // 提交状态
      showCommit:true,
      hasMember:true//是否已实名
  },
  changephone(e){
    this.setData({'adapaySettleaccount.phone':e.detail.value})
  },
  changebankCard(e){
    this.setData({'adapaySettleaccount.bankCard':e.detail.value})
  },
  delete(){
    let that=this;
    wx.showModal({
      title: '提示',
      content: '确定删除此结算卡？',
      success (res) {
        if (res.confirm) {
          API_Adamember.delSettleAccount().then(res=>{
            if(res.code==200){
              wx.showToast({title:"操作成功",duration:1500,icon:'none'});
              that.setData({adapaySettleaccount:{},hasMember:false});
            }else{
              wx.showToast({title:res.msg,duration:1500,icon:'none'});
            }
        })
        } else if (res.cancel) {
         
        }
      }
    })
   
 
  },
  saveAtt(e){
    if(!this.data.adaPayMember.userName || !this.data.adaPayMember.idCard) {
        wx.showModal({
            title: '提示',
            content: '前往完善用户信息',
            success (res) {
                if (res.confirm) {
                    // console.log('用户点击确定')
                    wx.navigateTo({
                        url: '/pages/shiming/shiming',
                    })
                } else if (res.cancel) {
                    console.log('用户点击取消')
                }
            }
        })
        return
    }
    let that = this
    this.setData({showCommit:!that.data.showCommit})
    if(!this.data.adapaySettleaccount.bankCard){
      wx.showToast({title:'请输入银行卡号',duration:1500,icon:'none'});
      that.setData({showCommit:!that.data.showCommit});
      return;
    }
    if(!this.data.adapaySettleaccount.phone){
      wx.showToast({title:'请输入银行卡预留手机号',duration:1500,icon:'none'});
      that.setData({showCommit:!that.data.showCommit});
      return;
    }
    if(this.data.adapaySettleaccount.phone.length!=11){
      wx.showToast({title:'请输入正确银行卡预留手机号',duration:1500,icon:'none'});
      that.setData({showCommit:!that.data.showCommit});
      return;
    }
   
    
    API_Adamember.createSettleAccount(this.data.adapaySettleaccount).then(res=>{
        if(res.code==200){
          wx.showToast({title:"结算卡绑定成功",duration:1500,icon:'none'});
            setTimeout(()=>{
              wx.navigateBack();
          }, 1500)
        }else{
          wx.showToast({title:res.msg,duration:1500,icon:'none'});
        }
        that.setData({showCommit:!that.data.showCommit});
    })
  },
  //去填写实名
  toWriteRealName() {
    if(!this.data.adaPayMember.userName || !this.data.adaPayMember.idCard) {
        wx.showModal({
            title: '提示',
            content: '前往完善用户信息',
            success (res) {
                if (res.confirm) {
                    // console.log('用户点击确定')
                    wx.navigateTo({
                        url: '/pages/shiming/shiming',
                    })
                } else if (res.cancel) {
                    console.log('用户点击取消')
                }
            }
        })
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let store = wx.getStorageSync('store')
    this.setData({store:store});
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    let that=this;
    API_Adamember.getMember().then(res=>{
        if(res.code==200){
          that.setData({adaPayMember:res.data || {}});
          
        //   if(!res.data){
        //     // wx.showToast({title:"请先实名后再绑卡",duration:1500,icon:'none'});
        //     // setTimeout(()=>{
        //     //   wx.navigateBack();
        //     // }, 1500)
        //   }
        API_Adamember.getSettleAccount().then(res=>{
            if(res.code==200){
              if(!res.data){
                that.setData({hasMember:false});
              }else{
                that.setData({
                    adapaySettleaccount:res.data,
                    adaPayMember: {
                      userName: res.data.userName,
                      idCard: res.data.idCard
                    }
                  });
                  }
              }
          })
        }
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})