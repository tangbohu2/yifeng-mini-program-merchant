// pages/fenXiaoCommission/fenXiaoCommission.js
import * as API_Mall_With_Draw from '../../api/withDraw'
import { distributionCommissionInfo } from '../../api/fix'
import * as API_Finance from '../../api/finance'
import * as API_Adamember from '../../api/adaMember'

Page({

  /**
   * 页面的初始数据
   */
  data: {
    count:0,
    commissionList:[],
    params:{
      pageNum:1,
      pageSize:10,
    },
    finished:false,
    commission: 0,
    dongCashPrice: ""
  },
  // withdrawal () {
  //   API_Adamember.applyStaffDrawcash().then(res => {
  //     this.setData({
  //       list: []
  //     }, () => {
  //       this.setData({
  //         "sendData.pageNum": 1
  //       })
  //       // this.data.sendData.pageNum = 1
  //       this.getList()
  //     })
  //   })
  // },
  toreordlist() {
    console.log(123)
    wx.navigateTo({
      url: './record/record',
    })
  },
  applyDrawcash:function(){
    let that = this
    wx.showModal({
      cancelColor: '#666',
      title:'提现',
      content:'确定提现吗？',
      success (res) {
        if (res.confirm) {
          API_Adamember.applyStaffDrawcash().then(res=>{
            if(res.code==200){
              wx.showToast({title:res.msg,duration:2500,icon:'none',success(e){
                setTimeout(function () {
                  //要延时执行的代码
                }, 2000) //延迟时间
              }});
              this.setData({
                "params.pageNum": 1,
                commissionList: [],
                finished: false
              })
              this.loadData()
            }else{
              wx.showToast({title:res.msg,duration:2500,icon:'none',success(e){
                setTimeout(function () {
                  //要延时执行的代码
                }, 2000) //延迟时间
              }});
            }
        })
        } else if (res.cancel) {
        }
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let globalData=getApp().globalData;
    let navHeadHeightpx=globalData.navHeadHeightpx;
    this.setData({navHeadHeightpx:navHeadHeightpx})
    this.setData({
      count:options.count
    })
    this.loadData()
  },
  loadData(){
    // API_Mall_With_Draw.getCommissionList().then(res=>{
    //   let arr =this.data.commissionList
    //   arr.push(...res.data.rows)
    //   this.setData({
    //     commissionList:arr,
    //     finished:res.data.lastPage
    //   })
    // })
    distributionCommissionInfo(this.data.params).then(res=>{
      let arr =this.data.commissionList
      arr.push(...res.data.clerkBalanceRecordListAll.rows)
      this.setData({
        commissionList:arr,
        finished:res.data.lastPage,
        commission: res.data.divPrice,
        dongCashPrice: res.data.dongCashPrice
      })
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    if(this.data.finished){
      return
    }
    let page = this.data.params.pageNum + 1
    this.setData({
      'params.pageNum':page
    })
    this.loadData()
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})