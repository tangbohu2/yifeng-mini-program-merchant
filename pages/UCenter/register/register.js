// pages/UCenter/register/register.js
import * as API_Member from '../../../api/member'
Page({

  /**
   * 页面的初始数据
   */
  data: {
      code:''
  }, 

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let that = this
      wx.login({
        success(res){
          that.setData({code:res.code})
        }
      })
  },

  getPhone (e) {
      if(e.detail.iv && e.detail.encryptedData){
        const params = {encryptedData:e.detail.encryptedData,iv:e.detail.iv,code:this.data.code}
        API_Member.register(params).then(res =>{
          if(res.success || res.exist ){
            wx.setStorageSync('member', res.member)
            wx.setStorageSync('token', res.token)
            wx.setStorageSync('pass','Y')
            wx.navigateBack({
              delta: 1
            })
          }
        })
      }else{
        wx.switchTab({
          url: '/pages/index/index',
        })
      }
    // API_Member.register(ed.detail.encryptedData)
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },


})
