// pages/UCenter/opinion/opinion.js
import * as API_Opinion from '../../../api/opinion'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    //视频列表
    opinion:{
      opinionContent:'',
      opinionVideo:'',
      opinionImages:'',
    },
    saveState:true
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },
  //视频上传回调
  uploadVideo(e){
    var videos = e.detail.videos.join(",")
    this.setData({"opinion.opinionVideo":videos})
  },

   //图片上传回调
   uploadImgs(e){
    var imgs = e.detail.picsList.join(",")
    this.setData({"opinion.opinionImages":imgs})
  },
  //输入文字内容
  changeText(e){
    this.setData({"opinion.opinionContent": e.detail.value})
  },
  //保存
  save(){
    let that = this
    that.setData({
      saveState:false
    })
    let opinion = that.data.opinion
    if(!opinion.opinionContent.trim()){
      wx.showToast({
        title: '请填写反馈内容',
        duration:1500,
        icon:'none'
      })
      that.setData({
        saveState:true
      })
      return
    }
    API_Opinion.create(opinion).then(res =>{
      if(res){
        wx.showToast({
          title: '提交成功',
          duration:1500,
          icon:'none',
          success(res) {
            setTimeout(function () {
              wx.navigateBack({
                delta: 0,
              })
            },1500)
          }
        })
      }else{
        wx.showToast({
          title: '提交失败,请重试',
          duration:1500,
          icon:'none'
        })
        that.setData({
          saveState:true
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

   
})