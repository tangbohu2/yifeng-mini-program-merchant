// pages/headImageList/headImageList.js
import * as API_Fitment from '../../api/fitment'
import * as API_File from '../../utils/fileUpload'

Page({

  /**
   * 页面的初始数据
   */
  data: {
    showDel:false,
    headshow:0,
    tab_index:0,
    list:[],
    params:{
      pageNum:1,
      pageSize:20,
      type:''
    },
    finished:false
  },
  autoplayVideo:function(e){
    let indexAll = e.currentTarget.dataset.indexAll;
    let list=this.data.list;
    if(item.videoUrl){
      list[indexAll].autoplay=true;
      this.setData({list:list});
    }
   
    // debugger
  },
  switchTab:function(e){

    let current=e.currentTarget.dataset.tab_index;
    if(this.data.tab_index == current){
      return
    }
    this.setData({tab_index:current});
    if(current ==0 ){
      this.setData({
        'params.pageNum':1,
        'params.type':'image',
        list:[],
        finished:false
      })
    }else{
      this.setData({
        'params.pageNum':1,
        'params.type':'video',
        list:[],
        finished:false
      })
    }
    this.getList()

  },
  uploadMedia(){
    let that = this
    let index = that.data.tab_index
    if(index == 0){
      that.uploadImg()
    }else{
      that.uploadVideo()
    }
  },
  uploadVideo(){
    let that = this
    wx.chooseMedia({
      count: 1,
      mediaType: ['video'],
      sourceType: ['album', 'camera'],
      success: function (res) {
        API_File.upload(res.tempFiles[0].tempFilePath,res=>{
          const {url} = JSON.parse(res.data)
          that.commitMedia(null,'video',url)
        },res=>{
            wx.showToast({
                title:'上传失败',
                icon:'none'
            })
        })
      }
  })
  },
  uploadImg(){
    let that = this
    var pics = [];
      wx.chooseImage({
      count: 9, // 最多可以选择的图片张数a
      sizeType: ['compressed'], // original 原图，compressed 压缩图，默认二者都有
      // sourceType: ['album', 'camera'], // album 从相册选图，camera 使用相机，默认二者都有\
      sourceType:['album','camera'],
      success: function(res) {
        var imgs = res.tempFilePaths;
        for (var i = 0; i < imgs.length; i++) {
          pics.push(imgs[i])
        }
        that.uploadimg({
          path: pics //这里是选取的图片的地址数组
        });
      },
    })
  },
  getList(){
    let that = this
    let index = that.data.tab_index
    if(index == 0){
      that.setData({
        'params.type':'image'
      })
    }else{
      that.setData({
        'params.type':'video'
      })
    }
    API_Fitment.getList(that.data.params).then(res=>{
      var arr = that.data.list
      if(index == 0){
        for(let item of res.rows){
          item.src = item.picUrl
        }
      }else{
        for(let item of res.rows){
          item.src = item.videoCover
        }
      }
      arr.push(...res.rows)
      this.setData({
        list:arr,
        finished:res.lastPage
      })
    })
  },
  commitMedia(picUrl,type,videoUrl){
    let fitment = {
      picUrl:picUrl,
      mediaType:type,
      videoUrl:videoUrl
    }
    if(type == 'image'){
      API_Fitment.addImage(fitment).then(res=>{
        this.setData({
          list:[]
        })
        this.getList()
      })
    }else{
      this.setData({
        list:[]
      })
      API_Fitment.addVideo(fitment).then(res=>{
        this.getList()
      })
    }
  },
  uploadimg: function(data) {
    wx.showLoading({
      title: '上传中...',
      mask: true,
    })
    var that = this,
      i = data.i ? data.i : 0,
      success = data.success ? data.success : 0,
      fail = data.fail ? data.fail : 0;
      API_File.upload(data.path[i],res=>{
        wx.hideLoading();
        success++;
        const {url} = JSON.parse(res.data)
        that.commitMedia(url,'image',null)
        i++;
        if (i == data.path.length) { //当图片传完时，传递后台
        } else { //若图片还没有传完，则继续调用函数
          data.i = i;
          data.success = success;
          data.fail = fail;
          that.uploadimg(data);
        }
    },res=>{
        wx.showToast({
            title:'上传失败',
            icon:'none'
        })
    })
  },
  showDel:function(){
  this.setData({showDel:!this.data.showDel});
  },
  del:function(e){
    let that = this
    wx.showModal({
    title:'删除',
    content:'是否删除此项？',
    success (res) {
      if (res.confirm) {
        const  id  = e.currentTarget.dataset.id
        const  index = e.currentTarget.dataset.index
        API_Fitment.del(id).then(res=>{
          that.data.list.splice(index,1)
          that.setData({
            list:that.data.list
          })
        })

      }
    }
  })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.getList();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {
    if(this.data.finished){
      return
    }
    let page = this.data.params.pageNum + 1
    this.setData({
      'params.pageNum':page
    })
    this.getList()
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})
