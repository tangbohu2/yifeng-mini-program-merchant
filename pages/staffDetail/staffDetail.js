// pages/staffDetail/staffDetail.js
import { getStaffInfo } from '../../api/fix'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    id: null,
    clerkBalanceRecordListAll: {},
    staffOne: {}
  },
  forMatDate(date) {
    const datetime = new Date(date)
    const year = datetime.getFullYear()
    const month = (datetime.getMonth() + 1) > 9 ? datetime.getMonth() + 1 : ( '0' + (datetime.getMonth() + 1) )
    const day = datetime.getDate() > 9 ? datetime.getDate() : ('0' + datetime.getDate())
    return year + '-' + month + '-' + day
  },
  getDetail() {
    if(this.data.id) {
      const that = this
      this.setData({
        clerkBalanceRecordListAll: [],
      })
      getStaffInfo({id:this.data.id}).then(res => {
        const list = that.data.clerkBalanceRecordListAll
        const clerkBalanceRecordListAll =  res.data.clerkBalanceRecordListAll
        clerkBalanceRecordListAll.forEach(item => {
          const date = this.forMatDate(item.createTime)
          if(list.hasOwnProperty(date)) {
            list[date].push(item)
          } else {
            list[date] = []
            list[date].push(item)
          }
        })

        const result = []
        for(let i in list) {
          const value = list[i] || []
          let price = 0
          if(value.length > 0) {
            value.forEach(item => {
              if(item.busType === 'pro') {
                price += item.money
              }
            })
          }
          result.push({
            date: i,
            price,
            value
          })
        }
        this.setData({
          clerkBalanceRecordListAll: result,
          staffOne: res.data.staffOne
        })
      })
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad({id}) {
    this.setData({
      id
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    this.getDetail()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})