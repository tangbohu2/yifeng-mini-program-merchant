// pages/appointmentEdit/appointmentEdit.js
import * as API_Appointment from '../../api/appointment'
const util = require('../../utils/util')
Page({

  /**
   * 页面的初始数据
   */
  data: {
  // closeState:true,
  appointment:{},
    showBackMask:false,
    showWeeksSelect:false,
    showClosesSelect:false,
    hasSeveralWeeksY:false,
    timeFramesIndex:[0,0],
    timeFramesArray:[],
    timeList:[],
    closesStart:util.getDate(),
    closesEnd:util.getDate(),
    showCommit:true,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    API_Appointment.appointmentSetInfo().then(res=>{
      // debugger
      let jso=res.data;
      let timeList=jso.timeList;
      let timeFramesArray=[];
      timeFramesArray.push(timeList);
      timeFramesArray.push(timeList);
      this.setData({appointment:jso.appointmentVO,timeFramesArray:timeFramesArray,timeList:timeList});
      const teachid = options.id
      const appointment = this.data.appointment
      appointment.technicianId = teachid
      this.setData({
        appointment
      })
      this.inithasSeveralWeeksY();
    })
  },
  toSave:function(){

    this.setData({showCommit:false});
    let appointment=this.data.appointment;
  
    if(!this.data.hasSeveralWeeksY){
        wx.showToast({
            title:'请选择每周可预约时间',
            icon:'none'
        })
      this.setData({showCommit:true});
      return;
    }
    if(appointment.timeFrames.length==0){
        wx.showToast({
            title:'请添加可预约时段',
            icon:'none'
        })
      this.setData({showCommit:true});
      return;
      }
      // debugger
    API_Appointment.appointmentEdit(appointment).then(res=>{
      // debugger
      // this.setData({showCommit:true});
      
      wx.showToast({title:res.msg,duration:1500,icon:'none'});
      setTimeout(()=>{
        wx.navigateBack();
    }, 1500)
    })
  },
  /**
   * 开启或关闭预约
   */
  openClose:function(){
    let appointmentState=this.data.appointment.appointmentState;
    if(appointmentState=='Y'){
      appointmentState='N';
    }else{
      appointmentState='Y';
    }
    this.setData({'appointment.appointmentState':appointmentState});
    
  },
  /**
   * 选择每周可预约时间
   */
  severalWeeksSelect:function(e){
    // debugger
    let index=e.currentTarget.dataset.index;
    let severalWeeksd=this.data.appointment.severalWeeks;
    let checked=severalWeeksd[index].checked;
    if(checked=='Y'){
      checked='N';
    }else{
      checked='Y';
    }
    severalWeeksd[index].checked=checked;
    this.setData({'appointment.severalWeeks':severalWeeksd});
    this.inithasSeveralWeeksY();
  },
  inithasSeveralWeeksY:function(){
    let severalWeeksd=this.data.appointment.severalWeeks;
    let hasSeveralWeeksY=false;
    severalWeeksd.forEach(item => {
      if(item.checked=='Y'){
        hasSeveralWeeksY=true;
      }
    });
    this.setData({hasSeveralWeeksY:hasSeveralWeeksY});
  },
  timeFramesArrayChange:function(e){
    // debugger
    let values=e.detail.value;
    let index1=values[0];//开始时间
    let index2=values[1];//结束时间
    // if(index1>index2){
    //   wx.showToast({
    //       title:'结束时间需大于等于开始时间',
    //       icon:'none'
    //   })
    //   return;
    // }
    let timeList=this.data.timeList;
    let start=timeList[index1];
    let end=timeList[index2];
    let timeFrames=this.data.appointment.timeFrames;
    let jso={startTime:start,endTime:end};
    timeFrames.push(jso);
    this.setData({'appointment.timeFrames':timeFrames});
  },
  closeSeveralWeeksSelect:function(){
    this.setData({showBackMask:false,showWeeksSelect:false});
  },
  openSeveralWeeksSelect:function(){
    this.setData({showBackMask:true,showWeeksSelect:true});
  },
  closeClosesSelect:function(){
    //确定不可预约日期
    let closesDates=this.data.appointment.closesDates;
    let closesDate={startDate:this.data.closesStart,endDate:this.data.closesEnd};
    closesDates.push(closesDate);
    this.setData({showBackMask:false,showClosesSelect:false,'appointment.closesDates':closesDates});
  },
  openClosesSelect:function(){
    this.setData({showBackMask:true,showClosesSelect:true});
  },
  bindDateStartChange:function(e){
    this.setData({closesStart: e.detail.value,closesEnd: e.detail.value})
  },
  bindDateEndChange:function(e){
    this.setData({closesEnd: e.detail.value})
  },
  /**
   * 删除
   */
  deleteTimeFrames:function(e){
    let index=e.currentTarget.dataset.index;
    let that=this;
    wx.showModal({
      title:'删除',
      content:'是否删除此时段',
      success (res) {
        if (res.confirm) {
          // debugger
         let timeFrames=that.data.appointment.timeFrames;
          timeFrames.splice(index,1)
          that.setData({
            'appointment.timeFrames':timeFrames
          })
        }
      }
    })
  },
  
  deleteClosesDates:function(e){
    let index=e.currentTarget.dataset.index;
    let that=this;
    wx.showModal({
      title:'删除',
      content:'是否删除此日期',
      success (res) {
        if (res.confirm) {
          // debugger
         let closesDates=that.data.appointment.closesDates;
         closesDates.splice(index,1)
          that.setData({
            'appointment.closesDates':closesDates
          })
        }
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})