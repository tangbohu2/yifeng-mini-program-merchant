// pages/productset/productset.js
import { configSet,getConfig } from '../../api/fix'
import FormData from '../../utils/formdata'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    array: Array.from({ length: 100 }, (_, index) => index),
    index: 0, //次数
    times: '',
    product: '', //查询用id
    productid: '', //修改用id
    storeid: '', //门店id
    startDate: '',//售后开始时限
    endDate: '',//售后结束时限
    staffMoney: '',//项目员工佣金
    isAfterSales: 'N',//是否有售后
    asMoney: '', //售后手工费
    items: [
      {value: 'Y', name: '是'},
      {value: 'N', name: '否'}
    ]
  },
  inputStaffMoney(e) {
    const value = e.detail.value
    this.setData({
      staffMoney: value
    })
  }, 
  inputAsMoney(e) {
    const value = e.detail.value
    this.setData({
      asMoney: value
    })
  },
  radioChange(e) {
    const value = e.detail.value
    this.setData({
      isAfterSales: value
    })
  },
  bindPickerChange: function(e) {
    this.setData({
      index: e.detail.value
    })
  },
  bindStartDateChange: function(e) {
    this.setData({
      startDate: e.detail.value
    })
  },
  bindEndDateChange: function(e) {
    this.setData({
      endDate: e.detail.value
    })
  },
  saveConfig() {
    let data = {
      id: Number(this.data.product),
        staffMoney: Number(this.data.staffMoney),
        isAfterSales: this.data.isAfterSales,
        asNum: this.data.index,
        asStartTime: this.data.startDate,
        asEndTime: this.data.endDate,
    }
    if(this.data.isAfterSales == 'Y') {
      data.asMoney = Number(this.data.asMoney)
    } else {
      data.asMoney = 0
    }
    configSet(data).then(res => {
      wx.showToast({
        title: res.msg,
      })
    })
  },
  showConfig() {

    // const form = new FormData()
    // form.append('storeId',this.data.storeid)
    // form.append('productId',this.data.productid)
    // const data = form.getData()
    // wx.request({
    //   url: 'http://172.168.10.98:8080/store/storeProduct/showProductConfig',
    //   method: 'post',
    //   header: {
    //     'content-type': data.contentType
    //   },
    //   data: data.buffer,
    //   success: (res) => {
    //     console.log(res)
    //   }
    // })
    getConfig(this.data.product).then(res => {
      console.log(new Date(res.asStartTime))
      const startTime = new Date(res.asStartTime)
      let asStartTime = ''
      if(res.asStartTime) {
        asStartTime = startTime.getFullYear()+'-'+ ((parseInt(startTime.getMonth())+1) > 9 ? (parseInt(startTime.getMonth())+1) : ('0'+ (parseInt(startTime.getMonth())+1))) +'-'+ (startTime.getDate() > 9 ? startTime.getDate() : ('0'+startTime.getDate()))
      }
      
      const endTime = new Date(res.asEndTime)
      let asEndTime = ''
      if(res.asEndTime) {
        asEndTime = endTime.getFullYear()+'-'+ ((parseInt(endTime.getMonth())+1) > 9 ? (parseInt(endTime.getMonth())+1) : ('0'+ (parseInt(endTime.getMonth())+1))) +'-'+ (endTime.getDate() > 9 ? endTime.getDate() : ('0'+endTime.getDate()))
      }
      
      this.setData({
        staffMoney: res.staffMoney,
        isAfterSales: res.isAfterSales,
        index: res.asNum,
        startDate: asStartTime,
        endDate: asEndTime,
        asMoney: res.asMoney
      })
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad({id,productid,storeid}) {
    console.log(storeid)
    //id: 查询用，productid修改用
    this.setData({
      productid: id || '',
      product: productid,
      storeid
    })
    this.showConfig()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})