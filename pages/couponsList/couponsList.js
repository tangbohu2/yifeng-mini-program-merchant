// pages/couponsList/couponsList.js
import * as API_Coupons from '../../api/coupons'
Page({
  /**
   * 页面的初始数据
   */
  data: {
    couponList:[],
    headshow:0,
    tab_index:0,
    paramsArray:[{},{}],
    listArray:[[{useState:'Y'}],[{useState:'N'}]],
    params:{
      pageNum:1,pageSize:20,state:'Y'
    },
    finished:false
  },
  switchTab:function(e){
    let current=e.currentTarget.dataset.tab_index;
    //此处setData会触发tabIndexChange，所以不用主动调用tabIndexChangeHandle，避免多次执行
    this.setData({tab_index:current,scrollhandle:false});
    this.setData({
      'params.pageNum':1,
      'params.state':e.currentTarget.dataset.state,
      couponList:[]
    })
    this.getList()
    
  },
  tabIndexChange:function(e){
    console.info('tabIndexChange')
  let current=e.detail.current;
  this.tabIndexChangeHandle(current);
  this.setData({scrollhandle:true})
  },
  tabIndexChangeHandle:function(current){
    
    console.info('tabIndexChangeHandle')
    this.setData({tab_index:current});
    console.info('页签变化为'+current)
    if(this.data.scrollhandle){
      //手动滑动才需要滑动到对应位置
      wx.createSelectorQuery()
      .select('.select_tab_view')
      .node()
      .exec((res) => {
        const scrollView = res[0].node;
        scrollView.scrollIntoView(".act")
      })
    }
    let item = this.data.paramsArray[current]
    if(item.load){
      this.getList()
    }
  },
  getList(){
    API_Coupons.getList(this.data.params).then(res=>{
      let arr = this.data.couponList
      arr.push(...res.rows)
      this.setData({
        finished:res.lastPage,
        couponList:arr
      })
      console.log(this.data.couponList)
    })
  },
  nextSwiper:function(){
    let len=3;
    let current=this.data.tab_index;
    let next=current+1;
    if(next>len-1){
      next=0;
    }
    this.tabIndexChangeHandle(next);
  },
  showRightRecal:function(){
    //手动滑动才需要滑动到对应位置
    let that=this;
    wx.createSelectorQuery()
    .selectAll('.item')
    .boundingClientRect(function (rect) {
      let len= rect.length;
      if(len>0){
        //有元素
        let lastItem= rect[len-1];
        let right= Number(lastItem.right);
        if(right>getApp().globalData.clientWidth){
          that.setData({headshow:1});
        }
      }
    }).exec();
  },
  scroll :function(e){
    
    let deltaX=e.detail.deltaX;
    let scrollWidth=e.detail.scrollWidth;
    let scrollLeft=e.detail.scrollLeft+80;
    let clientWidth=getApp().globalData.clientWidth;
    let rightX=scrollWidth-clientWidth;
    let opacity = 1
    if(rightX<scrollLeft){
      console.info('需要隐藏')
      
      opacity = (100-(scrollLeft- rightX)*3)/100
      console.info("opacity="+opacity)
    }
    this.setData({headshow:opacity})
  
  },
  add(){
    wx.navigateTo({
      url: '/pages/couponsEdit/couponsEdit',
    })
  },
  flush(){
    this.setData({
      'params.pageNum':1,
      'params.state':'N',
      couponList:[]
    })
    this.getList()
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let globalData=getApp().globalData;
    let windowHeight=globalData.windowHeight;
    let navHeadHeightpx=globalData.navHeadHeightpx;
    this.setData({navHeadHeightpx:navHeadHeightpx,windowHeight:windowHeight});
    this.getList()
  },

  down(e){
    API_Coupons.down(e.currentTarget.dataset.id).then(res=>{
      wx.showToast({
        title: '停用成功',
      })
      this.setData({
        'params.pageNum':1,
        couponList:[]
      })
      this.getList()
    })
  },
  up(e){
    // API_Coupons.getQuanDetail(e.currentTarget.dataset.id)
    API_Coupons.up(e.currentTarget.dataset.id).then(res=>{
      if(!res.success) {
        wx.navigateTo({
          url: '/pages/couponsEdit/couponsEdit?id='+e.currentTarget.dataset.id,
        })
      } else if(res.success) {
        wx.showToast({
          title: '启用成功',
        })
        this.setData({
          'params.pageNum':1,
          couponList:[]
        })
        this.getList()
      }
    })
  },

  flush(){
    this.setData({
      'params.pageNum':1,
      couponList:[]
    })
    this.getList()
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    if(this.data.finished){
      return
    }
    var page = this.data.params.pageNum + 1
    this.setData({
      'params.pageNum':page
    })
    this.getList()
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})