// pages/vipPayment/vipPayment.js
import * as API_VipPayment from '../../api/vipPayment'
import * as API_Stroe from '../../api/store'
const util = require('../../utils/util')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    payTypeList:[],
    paymentRecordList:[],
    choicePayIndex:0,
    store:{}
  },
  loadMember(){
    let that=this;
    API_Stroe.getStroe().then(res =>{
      
      if(res){
        wx.setStorageSync('store',res)
        that.setData({store:res})
      }
    })
  },
  getPaymentConfigList(){
    API_VipPayment.listPaymentConfig().then(res=>{
      if(res.code == 200){
        
       this.setData({payTypeList:res.data.rows});
      }
    })
  },
  getlistAdapayRecordB(){
    API_VipPayment.listAdapayRecordB().then(res=>{
      if(res.code == 200){
       this.setData({paymentRecordList:res.data.rows});
      }
    })
  },

  
  choicePay:function(e){
  let index =e.currentTarget.dataset.index;
  this.setData({choicePayIndex:index})
  },
  // toBack:function(){
  //   wx.navigateBack({
  //     delta: 0,
  //   })
  // },
  toPay:util.btnDis(function(){
    let that =this;
    let choicePayIndex=this.data.choicePayIndex;
    let payMent=this.data.payTypeList[choicePayIndex];
    const params = {
      payId:payMent.id
     }
    API_VipPayment.pay(params).then(res=>{
      if(res.code==200){

        let payment=res.data;

      if(payment.status=="succeeded"){
        let expend=payment.expend;
        let pay_info=JSON.parse(expend.pay_info);

        wx.requestPayment(
          {
              'timeStamp':pay_info.timeStamp,
              'nonceStr': pay_info.nonceStr,
              'package': pay_info.package,
              'signType': pay_info.signType,
              'paySign': pay_info.paySign,
              'success':function(res){
                wx.showToast({
                  title: '支付成功',
                })
                

              },
              'fail':function(res){

              },
              'complete':function(res){
                that.getlistAdapayRecordB();
                that.loadMember();
              }
          })
      }


      }else{
        wx.showToast({
          title: res.msg
        })
      }

    })

    
  },3000),
  
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
   
    let globalData=getApp().globalData;
    let statusBarHeight=globalData.systemInfo.statusBarHeight;
    this.setData({statusBarHeight:statusBarHeight})
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getPaymentConfigList();
    this.getlistAdapayRecordB();
    this.loadMember();
  },
 

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})