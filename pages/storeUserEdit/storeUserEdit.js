// pages/storeUserEdit/storeUserEdit.js
import * as API_StoreUserAccount from '../../api/storeUserAccount'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    
    storeUser:{},
     // 提交状态
     showCommit:true
  },
  changeUserName(e){
    this.setData({'storeUser.userName':e.detail.value})
  },
  changePhone(e){
    this.setData({'storeUser.phone':e.detail.value})
  },
  saveAtt(e){
    let that = this
    that.setData({showCommit:!that.data.showCommit})
    
    API_StoreUserAccount.editAcount(this.data.storeUser).then(res =>{
      if(res.code==200){
        wx.navigateBack();
      }else{
        wx.showToast({
            title:res.msg,
            icon:'none'
        })
      that.setData({showCommit:!that.data.showCommit})
      }
   
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let that=this;
    if(options.uuid){
     

      let param={uuid:options.uuid};
      API_StoreUserAccount.getAcount(param).then(res =>{
        that.setData({'storeUser':res.data})
        that.setData({'storeUser.uuid':options.uuid})
        
        
     
      })
    }

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})