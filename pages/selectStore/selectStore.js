// pages/selectStore/selectStore.js
import * as API_Stroe from '../../api/store'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    storeList:[]
  },
  initMyStoreList:function(){
    let that=this;
    API_Stroe.getMyStoreList().then(res=>{
      if(res.code==200){
        that.setData({storeList:res.data});
      }
    })
  },
  selectStore:function(e){
    let id=e.currentTarget.dataset.id;
    let param=  {storeId:id};
    API_Stroe.getStroe(param).then(res =>{
      if(res){
        //有门店，先进门店
        
        wx.setStorageSync('store',res)
        wx.setStorageSync('storeId',res.id)
        wx.navigateBack();
      }else{
       
        wx.reLaunch({
          url: '/pages/middlePage/middlePage',
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
this.initMyStoreList();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})