// pages/middlePage/middlePage.js
import * as API_Stroe from '../../api/store'
import * as API_Member from '../../api/member'
import * as API_Config from '../../api/sysConfig'
import * as API_Upload from '../../utils/fileUpload'
import {
  staffLoginByUserPwd
} from '../../api/fix'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    default_avatar: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAYAAABw4pVUAAAACXBIWXMAAAsTAAALEwEAmpwYAAAW0UlEQVR4nO2dd3Rc1Z2Av/dmpBlpVC3JVnEvso2LXABjLzWHlgVSSUJCYifZBZLAJpCcUBJ293GWJEvCAoHgAIHNSgkBktBCgBBI6AZjG1su2JZlq0ujPhpNn3nv7R8jCY3mtRkVyybfOT7H8+a+e6/ub277lXsFpjFuSZoNrAJWAouA+UAFUATMABxjXgkDfUAv0AY0AkeB/cC+UklqnYp6p4OqqgAIx7keCbglaTFwMXA2cCZQNsFFdABvAW8AfymVpPoJzj9tpo1A3JK0CrgS+CyweIqLrweeBB4tlaR9U1x2AsdVIG5JygO+DFwNVB2POmhQCzwE/LZUkrxTXfhxEYhbkuYA3wGuAvKmsuwU8AK/An5eKkktU1XolArELUmlwM3ANYAznTw8kRA9wQDeSJiQHCOqyEQVJSldhiiSIdpw2uzkZTooycomPzOtIkPAg8B/l0qSO50MUmFKBOKWJCdwA/ADIMfqewOREAf7e6gb6KVp0ENn0E9YjqVdjyybnVnZOczJyWd5QTGVBcXkZ45doOniA34M3F0qSaG0K2HCpAvELUnnER+TLU3UPaEA73S2sLOrnfbA4GRVa4QKVy6nz5zNhpmzKXJmWXmlHri6VJJenYz6TJpA3JLkAu4iPmEboqgqO7rbeK29kfqBvomuiiUEYElBEeeVL2BdcRmiYNokDwHfLZUk/0TWY1IE4pak9cBjwBKjdIqqsq2zhecaD9MXDk5kFcZFSZaLS+dWcsas2WaCOQJ8sVSSdk1U2RMuELckfR3YSvLuOYED/d08Ub+fjgkclgRBoLSsjIKCQmKxGKFQkFAoRDAQIBgMIstySvlVuPK4YvFKlhUUGyULA98qlaT/HU/dh5kwgbglyQbcDfybUTpfNMLj9fvZ3jXx2ovSsjIqKmbrfh8MBhn0ehnwDjDo9Y788WZsKp3DFxatJNueYZTsPuCGUklKTepjmBCBuCUpG3gcuMwo3cH+bh45tJuByOQsUiqXLiU319q2RpZl+vv66Onpxu83nwYKHVn86/J1VOYXGSV7DriiVJICliqhwbgF4pakfOAZ4FzdQoAXm4/wTMMhVKz9KtNhzpy5zJw1K+X3fD4fne4OPB6PYTpRELh84SlcMHuRUbLXgE+VStJAyhVhnAIZEsZfgdP10sRUherDe3i3c/IVrHa7nQULF5GXl97m3+/309rSgs9nPK+dVTaPLy9ZbTThvwdcmI5Q0hbI0DD1PAY9I6LIbN3/Hgf6u1PNflzYbDacTidOZxZZ2Vm4XDm4XC4E86UsAL29vbS2NBOL6W9Cq4pK+caKU7ELol6S14BLUh2+0hLI0AT+NAZzRlSRuXffdg55elLJetIQRZHc3FwKCmdQWFiIzWYzTB+NRmlsbMA7oP8jtyCU54BPpzLRDwtEN0cd7sJAGIqq8tDBXdNGGACKojAwMEBTYwN7a/fQ2NhAKKi/98nIyGDJkkrKKyp009T2uvnVwV1Gs+JlxNsqZSz3ELckfQ0wXHP/7sg+Xm1vSKceU05hYSEVs+fgcOhvmzz9/TQ0HEPRUGICXDRnMZcvPMWomK+XStKvrdQnpSHLLUlrgXcw2PS95W6m+vAeK9lNGwRBoLS0jLLyct15xufzUX+kTndzefXy9Zw2U7c3hYGNpZK026wuloesId3U4xgIo90/yKNH9pplNe1QVZWOjnYOfvABQZ1hLCcnhyWVSxFF7aaqrttDV1B3P+MAHh9qQ0tYmUP+B6jU+1JWFR4+tIuYTrc+EQgGAxw6+AH9fdoKTpfLxaLFizV7UViWefjg+yj6u/9K4m1oCUOBuCXpXOJGJV1ebK6nxTflFs8JR1EUjh07itvdofl9Xl4+c+fO0/yuYbCfv7UdM8r+mqG2NEVXIG5JchA3ZerSGwrwQnOdlXJOGNpaW2lr1d7MFpeUUFxSovnds42HzVRDvxpqU0OMesj1mBiXnmo4qGlGPdFxuzvodGtbbefMmYszK9mgFZZjPN1wyCjbxcTb1BBNgQzZwH9o9GKr38uOrjaz/E9YWltb6O/vT3ouiiILFizQnE/e6WwxmuABfjjUtrro9ZCbgVyjF19oPjKJ6sLpQWPDMUKh5NVXdraLkpkzk54rqsqLzUeMsswl3ra6JAnELUllmEzkfeEgu7rbjZKcFCiKQsOxY5r2k/LyCux2e9LzdztbGYyGjbK9ZqiNNdHqId/FxFXn3c5Wo2XeSUUgENBcedlsNsrKypOex1SF7Z2GQ7mTeBtrkiAQtyTlEndiM2Sbe8r8x6YFHe3tRMLJv3q9Fde2TtP2uWqorZMY20O+AuQb5dQw2E9n0GdW4EmFqqq0tSf/6vXULS2+Adr9hraVfOJtncRYgfyLWeVqezrNkpyU9Pf1EQ4l7jN6evS12nt6TZ0dNdt6RCBDXujrzHI5PDB9VOtTiaqqHD58iO6uLrzeAdpaW2lpbtJNf8hjapxbN9TmCYxeJnzeLIewHOOYN3lt/lEhGo3SbCCE0dQP9BNTFSMjFsTbPCEMYnTqy80KOeb1fGRWV1aw2Wy680hUkWka9JhlkdTmIoBbkhYCy8zenkjnthMZp9PJ8lNOYc3adaxZu05z+QvQETBd/CwbavsRhnvIx61U5KO2utJj0aLFZGfHTRyiKFJeUUFhYWFSOre5QGBM2w8L5Gwrb1os4KTG4XBoKhfz8wuSnln8ASe0vQggR6PnWHlzOjlGHy/0TLlaz/vD5p6aY9tedEtSuX/AM8uKv6s/GjFNc7ITi8XoHbP/UBSF7u6upLQ+k/ZSVRX/gGeWW5JGJiE7sDocCOB0hcjU6IoJlfnHCguApqZGAsEAeXn5xKJROjvdhELJvSFk4HAHEA2FCAcCAKuBdgB7NBQ6U5FlwgG/qUBCsWhaf4DLlU0wGNJ1pznRUFWVrs5OujqNtRaKiYEiHPCjyDLRUOhM4C8AYiwSWQMQ8vvBpAek2z9WLFvG5z71iTTfPnkJDXnfD8sAQJTl2CIARZYJB9P2pjdk6eJFXHbxhZy5ccOk5H8iMtw7AIZlACDK0diI6SvonZyNX0VZGYIgcPWWr3DWxjMmpYwTjdFtPVoGoqLII05c4WAA2WQiSoey0njshs1m45qvbeZLl38Gm47j2UcBORZLGI1Gy0BUZDkhXitg4PWdLrk5HzruCYLAJRddwE/+81aqVq6Y8LJOBMa28WgZ2BVZTvipBge9uAoKEE3c9lNBy6G5oryMG79zHccam3j97W3s2rOXfpNIppMBRZYJDnrHPhuRgX3shlBVVfweD7lFhjF1lsnONl5KL5w/j4Xz5/G1K79IZ3c3be0dDPr8ur62xxNBEKh5/PfjysPv8SQ5TYz+bFc19gaT0UusMKukhFk6durpQCAYHJdAtHoHwGgZaM6sqqri03E8/gfp4+vrMw3J1l3qBH2DRC0ox/Sw2WxUlJexdPFUn0k2eYiiyLLKJZSVzrIctzhMNBwiaBJUCokm3CS8PT0UlVdACoUXFhTwmcsuYeNpp5KVldaxSNMWp8PBv38/7lLlHfTx5rZ3+NOLL+Ezi3dXVbwGDhGjsQuiKKuKojlZxCIRfB4PORrGFy1WrVjOt6++ynQi16K1rZ2G5mYGfT5c2S4WzJvL3Nn6cX7jwef3c6iunp6+Xuw2OyXFRSxfWklmhuGJDQnk5eZwyUUXsGnDadx53y9pbG7WL8/jIRbR1/wKojiiu7cLIKugO3v7Pf04srLIcBr/2hfOn8d3r/1mSn8UwM7de/jDs8/R2pbsmjqrpIRPXvJxzt50RspDhBbdPT08/uQz7NxTmxT6nJmZyTmbNvLZT15Kbo7lo70oLCjglhu+za23/4Tu3t6k76OhEH6PsWOIACMCEQXRZhptM9DdZaipFQSBq7Z8JUkYsqeXmI6HvKIoPPKbR7l764OawgDo7O7mof+r4a77HyBi8AuzQu3+A9x82494d+cuzTj0SCTCy6+9zs3S7dQ36ASuqioxdwvKYOLGLifHxVevvELrBQY07CRjGS0DURAEUwciORZjoEs/49UrTkkcXmSZ8N7thPe+R7T+oOY7j/3xaf7+xlumlQV4v3Yv9/zyIUtptWhsbuau+x/QtFmMxTMwwI9+djcd7mTVuirLROr2E9q9jfDBPQna8TWrVlJRlhhpoCqqJVWUIAgjE4wdgSZgudlLEQNN8FgVSLTpCLLHeNn8zo6dI/93Op1cdvGFbDr9NEqKixj0+dmzbx9PPfcC3UOTYe3+A/gDAVzZ2WZVTWLH+3sSesWpa6v45wvOZ+GC+aCqHGts4oWX/8bO3XsAiESj7N67b0QHp4Xc3UEsrxB7xYdhblWrVtLWkcbxjAKNw/+1q4r6HvHDi9OmpDhxVy/3mXfTysUL2b7zfWYUFvKD732HslGHx+Tl5nD2po1sWL+O+x56mN1791NRVkq2iQFNv6y4dlsQBDZf8Xku/Ni5Cd8vXbKYpUsW8/pb23jkN4+iqCoL52vHE45G7utKEMjMYsPztXQZkgEAdjkWtTZuGGWYtNkxn4C/+fWv8k8bTqdy8SLdSdThcPC9675F3dGjzCmvSHtir1q5gttuuRGbzcaCeXN1051z5iaWLV1CLBqjotzCodpj6qOo6VlER8vAzhhXxnTo7klcXdiKZqGYuAxlZGSwfo35GcqCIEzI5nLxwgWW0qWiurEVJQ5pY9shBQ4M/0esqq5xC6I4Lp37+3sTZZoxbzG2GckhXycT9tI52MsST7Hbs3d/yvkIouipqq4ZWWbaAQRBeFOFS9Ot3AeHDtPQ1PzhcCCKOFauRxkcQNGI0TtREWw2MpdVIbpyEV2J8TY7d++hw8TpQTNPQXh79GcRQJHl58dTUVVVefDXNYTGRBmJufnYSwyDTk8sBAH7zPIkYXgGvFQ/lp4WWJHlF0Z/HlYuvpxWbqNoaWvjp/f8Au+gufukMOhBbKxDbG2Ir+WjEcTmesSmIwhT3aNUFbGtEbGxDmEo1ELo74nXr8M8dK+ru4cf33UPfRoh1BZ5cfQHEaCquuaoIAhH081xmMP19dz4H7fxwsuvMODVVwAIvV0I/kGEgT6EcDD+/8EBBJ8XwZP2xJgWQjiE4OmN16EvvkcWe9xDn7tAx3W0p7ePJ//0Z2657Xba2rWP4zAtWxCOVlXXJKgFRrS9qqo+BtxqlIFdEEy9Fwd9Ph79/ZP87g9PMaOwkLzcXG6/1TA0ewxT7R2pUZ7O39jv8XDnfVsZ8A5OiLlZVdUnxj4bbQ8xHQSdonULoqqq9Pb10dDURCSansfjdGPQ56exucWyMFzm7fXo2AcjAqmqrtmHIBgetDUzw/KNAgn4fCdHGENvX2rzRJFRewnC7qrqmg/GPk60GKrqA0YFzHdYPocrgY7OMaqU7KGduc2Oas8Eh3Nk16tmWVd9TwRqhgOGT652xvVk6nD9Mh0wyn/MneKydoFRe6mqprZ0rMXwMeBOdM45WZ2dz98HzPVUY2l3u1mxbOnIZ2VmORTPAkEEQUC125GXr4kP51PtQGezIVeuAlWBoSFGmb0AyubG6zJKPdLSbv04EQFYla0b8u8Ffqv1RcJfX1VdM0j8QH1NijMcLM9K/bDi+qMah3uJtkRdkCBOvTBGyhZGhDGCzZakq/rgkPWzwVZl51Ogf2b81qrqGs1xXKsF7iZ+eKMmFxeWYk9RyXfg0OGU0k9Henr7RkwBZmQKIhcW6G6Iw8C9el8mCaSquqaT+N1LmhTZM/nEDO2oUz36PQM0No/jfBRFQQ0Fx/3PLNzCiO07rV8V8qmiCqPecX9VdY3uxkXP6+S/gK+ic5PaOlchflnmJY91Y8xb725n/tw5ltOPRgn6Ce0at5WArE3nIxhfPaHLW+9ut5TussJyVuvPHYPAT4ze1xy0q6preoAfGb14Vl4xXyieY3lvsm37DsMz1Y0QHBPgTmSzpS2M+mMNNLcan56XJdq4smQuG3JnGCX78VDb6mI0i94LGM5iq7Lzub5sCRtyZpBhfIQEA14vr7/9jmEaPQR7BmKO4SFFptiMb8sx5JnnX9D9LlMQ2ZhbxA3llWYLnoNYOH7ccHau3bL5LOL3xpoSVhTqQoM0hwP0xyKENbxUCotmcP1/3GR6IL4WsbYmIkeT9lGWcaxYl2RQskJbUwsP3HlvglXUIYrMsGcyz+FiSVYOmSY/xiHOq6queU3vS8tHjddu2bwV+KaVEq1QeN4ZFJy5PvUXVYXgjjfik3OKiHmFONekF7nV/vAfCHekvvcaw9aq6pprjRKkcjvC94EJW7d63tpJbCCN0DlBxLF8TfJ+wey1jEwcS1enXh7g3bFvIoRxiHgbWsJUIFXVNX7gc8SvIh03ajRG11N/RSsMwgwxtwDnqlMRLHpHCs4sHKtPR8hK3XUo0tlD3ytvmyc0JgR8qaq6xnI0raXBr6q6Zh8mt7ClQrjVTf8r29J6V8yfgXP9WdjL5sZ391rYbGTMWYRz3ZlJ1j0rKMEQXX98CTU2rovXAK6uqq4xvRlhNCltuWu3bL4T+F5KVTKg6KKzyDs9veEEQJVjKP09KEE/RKOQkYnoysVWMCPloW0kz1iMjt88S7h13PcR/7yquuZ6y+UOzSGG4Qga3ET8tH/Da/Ks0vvSm4iOTHKqTI/q0kSw2bEVl+p7iqeIGo3R+cTzEyGMp0nzh5uy51ntls3ZwJ+B89IpUIsZ528if+PaicouLeRAiM7H/ky4fdyHfL4BXFRVXZPSnDuua/OGhPIqBtfmpUpO1TKKP34OQkaqnXb8hNs66XrypfRWf4lsJy6MtK/NS0vfPbRquBCLm0YrhNxdeA7VIU+luVdVCfkG8RxtMD0oxgKvAh9LRxijGVcUTO2WzU7gd8Cn081DzHORvXEVGfM/1CBnZmWT5cpFmET7SDQSJjjoRZE/1K+FDzYQfO8AaijlWJRnSXF5O5YJu5y4dstmG3APcF1KL4oizrVLyVq7FGzJDS8IAplZ2TiyXBManh0Nhwj5fcg6R02p4SjB9w4Q/sDwxpzR3AXcWFVdc/wvJx5N7ZbNW4AHMDnIH8BWkIvr/NOxFVlTGNozM8l0ZGHPdKQsHFVVkaMRouEwkXDQ8oY02tKJ/9WdqEFdW10IuK6quuaRlCpkUE+Y4Avua7dsXkfcLq97iVjmotlkn7sOQeOqByuINhs2ewY2ux1RtMWHNUFAQEBVFVRVRVUUZDmGEosRi0XTNkwpgRD+v+0g1p4UZHYE+OzQhnlCmBSBwMgK7KdAkjIt69TlONebBmtNLxSVwJu7CR9qHH6yFbhJzyaeLpMmkGFqt2w+H/gFsBQg+6w1OE5ZaPzSNCbwdm13eP/RL1RV17w6GfmPa9lrharqmleIH+54S/am1dETVRhqNKZG6lueF52ZCydLGKOZtB4ymrr776i0FeY9YZ8zc40wxQfapIsqy8RauvbI/d4rK6+9KX3LmNXyJnvI0qJu6x1rbQW5D9rLS04VMjOmtGyrqLGYGmvtek/uH/xG5bU37Zmyco+HQIapu/+OWaIr615bScGltsK81I0Vk4Dc7w3I3f1PKf7QjZXX3pRefME4OK4CGU3dL3/2SVue62ZbUf5aMc+Vnjd3mihefzjW49mh+AJ3V37j+09NZdljmTYCGU3d1p+eLWY7rxNzsjaIBbkVYrZzQiccNRhW5H5vq+ILvK0Ewr+u/NaN444cmyimpUDGUrf1jtOEjIyLhEz7esHpqBQyM0qEzIwc0ZnpwCYKCfOQqqJGYyqKqirBcFiNRAbVSKxHDUeOquHoO2ok+nzltTdN2zvGhwXy/58OWKzOBHN0AAAAAElFTkSuQmCC',
    member: {},
    // showBtn:false,
    // enableAccountLogin:'N'
    showBtn: true,
    enableAccountLogin: 'Y',
    staffshow: false,
    phone: '',
    password: ''
  },
  staffLogin() {
    this.setData({
      staffshow: !this.data.staffshow
    })
  },
  spacefn() {},
  inputPhone(e) {
    this.setData({
      phone: e.detail.value
    })
  },
  inputPwd(e) {
    this.setData({
      password: e.detail.value
    })
  },
  staffloginSubmit() {
    console.log('login')
    // this.setHeadImg(1)
    // return
    const data = {
      phone: this.data.phone,
      password: this.data.password,
      userType: 2
    }
    staffLoginByUserPwd(data).then(({
      data,
      code,
      msg
    }) => {
      // console.log(data)
      if (code === 200) {
        wx.setStorageSync('token', data.token)
        wx.setStorageSync('member', data.member)
        wx.setStorageSync('store', data)
        wx.setStorageSync('storeId', data.id)
        wx.setStorageSync('loginType', 2)
        if (!data.member.headImg) {
          this.setHeadImg(data.member.id)
        }
        this.setData({
          store: data
        })
        wx.navigateTo({
          url: '/pages/sstaffIndex/staffIndex',
        })
      } else {
        wx.showToast({
          title: msg,
          icon: 'none'
        })
      }
      
    })
  },
  setHeadImg (id) {
    const base64 = this.data.default_avatar
    const time = new Date().getTime();
    const imgPath = wx.env.USER_DATA_PATH + "/poster" + time + "share" + ".png";
    //如果图片字符串不含要清空的前缀,可以不执行下行代码.
    const imageData = base64.replace(/^data:image\/\w+;base64,/, "");
    const fs = wx.getFileSystemManager();
    fs.writeFileSync(imgPath, imageData, "base64");
    fs.close()
    API_Upload.upload(imgPath, res => {
      let requestData = JSON.parse(res.data)
      if (requestData.code !== 200) {
        return
      }
      const { url } = requestData
      const data = {
        headImg: url,
        id: id
      }
      API_Member.updateMember(data).then(res => {
        if (res.code === 200) {
          let store = wx.getStorageSync('store')
          store.member.headImg = url
          wx.setStorageSync('member', store.member)
          wx.setStorageSync('store', store)
        }
      })
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // wx.removeStorageSync('store');
    // wx.removeStorageSync('storeId');
    wx.showLoading({
      title: '加载中',
    })
    const member = wx.getStorageSync('member')
    const token = wx.getStorageSync('token')
    const openId = getApp().globalData.openId || wx.getStorageSync('openid')
    const loginType = wx.getStorageSync('loginType')
    let url = loginType === 1 ? '/pages/home/home' : '/pages/sstaffIndex/staffIndex'
    if (member && token && openId) {
      wx.hideLoading()
      if (loginType === 1) {
        wx.switchTab({
          url: url,
        })
      }
      if (loginType === 2) {
        wx.redirectTo({
          url: url,
        })
      }
    } else {
      wx.hideLoading()
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

    // this.getStoreInfo();
    this.flushMember();
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.flushLogin();
    // let that = this
    // API_Config.getValueByKey("enableAccountLogin").then(res => {
    //   if ('Y' == res) {
    //     //允许员工登录
    //     that.setData({
    //       enableAccountLogin: res
    //     })
    //   }
    // })
  },
  flushMember: function () {
    const member = wx.getStorageSync('member');
    this.setData({
      member: member
    });
  },
  getStoreInfo: function () {
    let that = this
    API_Stroe.getStroe().then(res => {
      console.log(res);
      if (res) {
        //有门店，先进门店
        if (res.code === 200) {
          wx.setStorageSync('store', res.data)
          wx.setStorageSync('storeId', res.data.id)

          wx.setStorageSync('loginType', 1)
          that.setData({
            store: res.data
          })
          wx.switchTab({
            url: '../home/home',
          })
        }
        if (res.code === 500) {
          wx.showToast({
            title: res.msg || '获取门店信息失败',
            icon: 'none'
          })
        }
      } else {
        //没找到门店，看还有没有申请中的门店
        that.toApply();

      }
    })
  },
  toApply: function () {
    let that = this;
    API_Stroe.getApply().then(res => {
      if (res) {
        //有在申请中的，
        wx.redirectTo({
          url: '../storeApplyResult/storeApplyResult',
        })
      } else {
        //申请中的也没有，显示按钮
        that.setData({
          showBtn: true
        });
        //完全没有
        // wx.navigateTo({
        //   url: '../storeApply/storeApply',
        // })
      }
    })
  },
  toApplyUrl: function () {
    wx.navigateTo({
      url: '../storeApply/storeApply',
    })
  },
  flushLogin: function () {
    //因为这个code只能用一次，使用一次后需要刷新下
    let that = this;
    wx.login({
      timeout: 8000,
      success(res) {
        that.setData({
          wxlogincode: res.code
        });

      }
    })
  },
  getPhone: function (e) {
    let that = this;
    if (!e.detail.iv) {
      //认为用户拒绝了或者关闭了授权 
      wx.showToast({
        title: '为了获取您的员工信息，请同意授权手机号',
        icon: 'none'
      })
      return;
    }
    const params = {
      encryptedData: e.detail.encryptedData,
      iv: e.detail.iv,
      code: this.data.wxlogincode
    }
    API_Member.flushPhone(params).then(res => {
      console.log(res);
      if (res.code == 200) {
        let data = res.data;
        wx.setStorageSync('token', data.token)
        wx.setStorageSync('member', data.member)
        that.getStoreInfo()
        that.flushMember();
      } else {
        wx.showToast({
          title: res.msg,
          icon: 'none'
        })
      }
      that.flushLogin();
    })
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },


})